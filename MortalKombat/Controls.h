#pragma once
#include "2dgraphics.h"

class control_map {
	 int UP_KEY;
	 int DOWN_KEY;
	 int LEFT_KEY;
	 int RIGHT_KEY;
	 int ENTER_KEY;
	 int EXIT_KEY;

public:
	control_map() {}

	control_map(std::string playername){
		ALLEGRO_CONFIG* cfg = al_load_config_file("../resources/config.cfg");
		
		this->UP_KEY = atoi(al_get_config_value(cfg, (playername + " Controls").c_str(), "Up"));
		this->DOWN_KEY= atoi(al_get_config_value(cfg, (playername + " Controls").c_str(), "Down"));
		this->LEFT_KEY = atoi(al_get_config_value(cfg, (playername + " Controls").c_str(), "Left"));
		this->RIGHT_KEY = atoi(al_get_config_value(cfg, (playername + " Controls").c_str(), "Right"));
		this->EXIT_KEY = atoi(al_get_config_value(cfg, (playername + " Controls").c_str(), "Exit"));
		this->ENTER_KEY = atoi(al_get_config_value(cfg, (playername + " Controls").c_str(), "Start"));
	
	}
	const int get_UP_key() {
		const int UP = this->UP_KEY;
			return UP;
	}
	const int get_DOWN_key() {
		const int DOWN = this->DOWN_KEY;
		return DOWN;
	}
	const int get_LEFT_key() {
		const int LEFT = this->LEFT_KEY;
		return LEFT;
	}
	const int get_RIGHT_key() {
		const int RIGHT = this->RIGHT_KEY;
		return RIGHT;
		
	}
	const int get_EXIT_key() {
		const int EXIT = this->EXIT_KEY;
		return EXIT;
	}
	const int get_ENTER_key() {
		const int ENTER = this->ENTER_KEY;
		return ENTER;
	}

	void set_keys(std::string playername);
};


std::map<std::string, int> set_player_controls(std::string playername);