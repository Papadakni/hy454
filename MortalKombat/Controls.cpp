#include "Controls.h"

std::map<std::string, int> set_player_controls(std::string playername) {
	ALLEGRO_CONFIG* cfg = al_load_config_file("../resources/config.cfg");
	std::map<std::string, int> Player_Controls;
	Player_Controls["Up"] =  atoi(al_get_config_value(cfg, (playername + " Controls").c_str(), "Up"));
	Player_Controls["Down"] = atoi(al_get_config_value(cfg, (playername + " Controls").c_str(), "Down"));
	Player_Controls["Left"] = atoi(al_get_config_value(cfg, (playername + " Controls").c_str(), "Left"));
	Player_Controls["Right"] = atoi(al_get_config_value(cfg, (playername + " Controls").c_str(), "Right"));
	Player_Controls["Exit"] = atoi(al_get_config_value(cfg, (playername + " Controls").c_str(), "Exit"));
	Player_Controls["Start"] = atoi(al_get_config_value(cfg, (playername + " Controls").c_str(), "Start"));
	//... to be expanded

	return Player_Controls;


}