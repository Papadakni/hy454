#pragma once
#include "Allegro_Initialiser.h"
#include "2dgraphics.h"
#include "Fighter.h"
#include "Defines.h"
#include "Logic.h"

namespace Menu {
	enum selector_move {
		UP,
		DOWN,
		LEFT,
		RIGHT
	};

	enum character_selector {
		JohnnyCage, 
		Kano, 
		Hidden, 
		SubZero, 
		SonyaBlade, 
		None,
		Raiden, 
		LiuKang, 
		Scorpion, 
		None2
	};

	enum stage_selector {
		Palace_Gates,
		Courtyard,
		The_Pit,
		Throne_Room,
		Warrior_Shrine,
		Goro_Lair
	};

	enum option_selector {
		Debug,
		Music,
		Controls
	};

	enum control_selector {
		Block_1, Block_2,
		High_Kick_1, High_Kick_2,
		Low_Kick_1, Low_Kick_2,
		High_Punch_1, High_Punch_2,
		Low_Punch_1, Low_Punch_2,
	};


	void playIntro(game_state& gamestate);
	void mainScreen(game_state & gamestate, ALLEGRO_EVENT_QUEUE * event_queue);
	void optionChange(enum selector_move action, option_selector &hover);
	void optionsScreen(game_state & gamestate, ALLEGRO_EVENT_QUEUE * event_queue, ALLEGRO_DISPLAY * disp);
	void controlChange(enum selector_move action, control_selector &hover);
	void checkKeyValidityAndConfirm(int*(&keys)[10], int nkey, int pos);
	void changeKey(enum control_selector, int*(&keys)[10], ALLEGRO_EVENT_QUEUE * event_queue);
	void controlsScreen(game_state & gamestate, ALLEGRO_EVENT_QUEUE * event_queue);
	void characterSelectScreen(game_state & gamestate, ALLEGRO_EVENT_QUEUE * event_queue, mk::Fighter & player1, mk::Fighter & player2,ALLEGRO_DISPLAY * disp);
	void changeSelection(enum selector_move action, enum character_selector & player_selector, engine2d::rectangle& outline_box);
	void changeSelectionstage(enum selector_move action, enum stage_selector & stager_selector,  engine2d::rectangle& outline_box);
	void playIdleAnimations(mk::Fighter & player1, mk::Fighter & player2, character_selector & p1_selector, character_selector & p2_selector, ALLEGRO_BITMAP * screen, 
		engine2d::rectangle & p1_rect, engine2d::rectangle & p2_rect, ALLEGRO_FONT * font_name);
	void StageSelectScreen(game_state & gamestate, ALLEGRO_EVENT_QUEUE * event_queue, engine2d::background & arena, ALLEGRO_DISPLAY * disp);
}