#include "Logic.h"





enum keycode_code {
	Exit,
	Start,
	Up,
	Down,
	Left,
	Right,
	High_Punch,
	Low_Punch,
	Block,
	High_Kick,
	Low_Kick,
	UNKNOWN
};

void logic::collision_check_players(mk::Fighter& player_1, mk::Fighter& player_2) {
	//std::cout << "My collision state is : player_1 :" << player_1.get_collision() << " player_2 :" << player_2.get_collision() << "\n";
	if (player_1.get_stance() == mk::RIGHT) {

		if (player_1.tickAnim.sprite.get_x_pos() + player_1.tickAnim.sprite.frameWidth >= player_2.tickAnim.sprite.get_x_pos() - 40) {
			player_1.set_collision(true);
			player_2.set_collision(true);
			return;
		}
	}
	else {
		if (player_1.tickAnim.sprite.get_x_pos() - player_1.tickAnim.sprite.frameWidth <= player_2.tickAnim.sprite.get_x_pos() + 40) {
			player_2.set_collision(true);
			player_1.set_collision(true);
			return;
		}

	}
	player_1.set_collision(false);
	player_2.set_collision(false);
}

void logic::collision_check_players_wider(mk::Fighter& player_1, mk::Fighter& player_2) {
	//std::cout << "My collision state is : player_1 :" << player_1.get_collision() << " player_2 :" << player_2.get_collision() << "\n";
	if (player_1.get_stance() == mk::RIGHT) {

		if (player_1.tickAnim.sprite.get_x_pos() + player_1.tickAnim.sprite.frameWidth >= player_2.tickAnim.sprite.get_x_pos() - 70) {
			player_1.set_wider_collision(true);
			player_2.set_wider_collision(true);
			return;
		}
	}
	else {
		if (player_1.tickAnim.sprite.get_x_pos() - player_1.tickAnim.sprite.frameWidth <= player_2.tickAnim.sprite.get_x_pos() + 70) {
			player_2.set_wider_collision(true);
			player_1.set_wider_collision(true);
			return;
		}

	}
	player_1.set_wider_collision(false);
	player_2.set_wider_collision(false);
}


void  logic::start_round_manager(int *current_round, engine2d::background arena, int *starting_now, ALLEGRO_FONT* font3, ALLEGRO_FONT* font4, int width, int time_passed, ALLEGRO_SAMPLE *bgm, ALLEGRO_SAMPLE_ID &id) {


	if (time_passed < 120) {
		al_draw_text(font4, al_map_rgb(204, 0, 0), width / 2, 110, ALLEGRO_ALIGN_CENTER, "ROUND ");
		al_draw_text(font3, al_map_rgb(255, 255, 20), width / 2, 110, ALLEGRO_ALIGN_CENTER, "ROUND");
		al_draw_text(font4, al_map_rgb(255, 255, 0), width / 2, 150, ALLEGRO_ALIGN_CENTER, std::to_string(*current_round).c_str());
	}
	else if (time_passed == 120) {

		*current_round = *current_round + 1;
		bgm = al_load_sample(("../resources/sounds/announcer/Fight.wav"));
		al_play_sample(bgm, 0.4, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);

	}
	else if (time_passed < 180) {

		font4 = al_load_ttf_font("../resources/fonts/mk1.ttf", 54, 0);
		font3 = al_load_ttf_font("../resources/fonts/mk1.ttf", 56, 0);
		if (time_passed % 5 == 0) {
			al_draw_text(font4, al_map_rgb(0, 0, 0), width / 2, 110, ALLEGRO_ALIGN_CENTER, "FIGHT ");
			al_draw_text(font3, al_map_rgb(255, 255, 20), width / 2, 110, ALLEGRO_ALIGN_CENTER, "FIGHT");
		}
		else {
			al_draw_text(font4, al_map_rgb(0, 0, 0), width / 2, 110, ALLEGRO_ALIGN_CENTER, "FIGHT ");
			al_draw_text(font3, al_map_rgb(204, 0, 0), width / 2, 110, ALLEGRO_ALIGN_CENTER, "FIGHT");
		}
		al_destroy_font(font4);
		al_destroy_font(font3);

	}
	else if (time_passed == 230) {

		*starting_now = 0;
		ALLEGRO_CONFIG* cfg = al_load_config_file("../resources/config.cfg");
		if (atoi(al_get_config_value(cfg, "Music", "Music")) == 1) {
			bgm = al_load_sample((("../resources/sounds/arenas/" + arena.get_name() + ".wav").c_str()));
			al_play_sample(bgm, 0.4, 0.0, 1.0, ALLEGRO_PLAYMODE_LOOP, &id);
		} 
		else 
		{
			bgm = al_load_sample("../resources/sounds/arenas/Silence.wav");
			al_play_sample(bgm, 0.4, 0.0, 1.0, ALLEGRO_PLAYMODE_LOOP, &id);

		}
		al_destroy_config(cfg);
		
	}


}

bool logic::win_checker(mk::Fighter &player_1, mk::Fighter &player_2) {
	if (player_2.get_health() <= 0) {
		return true;
	}
	else if (player_1.get_health() <= 0) {
		return true;
	}

	return false;

};

void logic::end_round_manager(mk::Fighter &player_1, mk::Fighter &player_2, int *current_round, int *starting_now, ALLEGRO_FONT* font3, ALLEGRO_FONT* font4, int *time_passed, ALLEGRO_SAMPLE *bgm) {

	if (player_1.get_health() > player_2.get_health()) {
		// real first if
	   /*if (player_1.is_victorious() == true) {
		  player_1.set_player_state(mk::IDLE);
		  //do finish him logic here
	  }*/
		if (*time_passed == 2) {
			player_1.add_win();
		}
		if (player_1.is_victorious() == true) {
			//std::cout << "MPAINW EDW OEO";
			if (*time_passed == 2) {
				player_1.set_player_state(mk::IDLE);
				if (player_2.get_health() >= 0) {
					if (player_2.get_character_name() == "Sonya") {
						bgm = al_load_sample(("../resources/sounds/announcer/Finish_her.wav"));
						al_play_sample(bgm, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
						bgm = al_load_sample(("../resources/sounds/announcer/Finish_her.wav"));
						al_play_sample(bgm, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
					}
					else {
						bgm = al_load_sample(("../resources/sounds/announcer/Finish_him.wav"));
						al_play_sample(bgm, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
						bgm = al_load_sample(("../resources/sounds/announcer/Finish_him.wav"));
						al_play_sample(bgm, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
					}

					player_2.set_player_state(mk::DAZE); //should be changed to dazed or shame
				}
			}

			if (*time_passed < 120) {
				if (player_2.get_character_name() == "Sonya") {
					if (*time_passed % 5 == 0) {
						al_draw_text(font4, al_map_rgb(0, 0, 0), 800 / 2, 160, ALLEGRO_ALIGN_CENTER, "FINISH HER");
						al_draw_text(font3, al_map_rgb(255, 255, 255), 800 / 2, 160, ALLEGRO_ALIGN_CENTER, "FINISH HER");
					}
					else {
						al_draw_text(font4, al_map_rgb(0, 0, 0), 800 / 2, 160, ALLEGRO_ALIGN_CENTER, "FINISH HER");
						al_draw_text(font3, al_map_rgb(255, 0, 0), 800 / 2, 160, ALLEGRO_ALIGN_CENTER, "FINISH HER");
					}
				}
				else {
					if (*time_passed % 5 == 0) {
						al_draw_text(font4, al_map_rgb(0, 0, 0), 800 / 2, 160, ALLEGRO_ALIGN_CENTER, "FINISH HIM");
						al_draw_text(font3, al_map_rgb(255, 255, 255), 800 / 2, 160, ALLEGRO_ALIGN_CENTER, "FINISH HIM");
					}
					else {
						al_draw_text(font4, al_map_rgb(0, 0, 0), 800 / 2, 160, ALLEGRO_ALIGN_CENTER, "FINISH HIM");
						al_draw_text(font3, al_map_rgb(255, 0, 0), 800 / 2, 160, ALLEGRO_ALIGN_CENTER, "FINISH HIM");
					}

				}
			}
			if (player_2.get_state() == mk::DEAD || *time_passed == 500) {
				player_2.set_player_state(mk::DEAD);
				player_1.add_win();
				*time_passed = 0;
			}

		}
		else {
			if (*time_passed == 2) {
				player_1.set_player_state(mk::WIN);
				//player_1.add_win();
				if (player_2.get_health() > 0) {
					player_2.set_player_state(mk::DEAD); //should be changed to dazed or shame
				}

			}
			else if (*time_passed == 65) {

				//asto etsi den kserw ti paizei mhn alla3ei
				bgm = al_load_sample(("../resources/sounds/announcer/" + player_1.get_character_name() + "_wins.wav").c_str());
				al_play_sample(bgm, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
				bgm = al_load_sample(("../resources/sounds/announcer/" + player_1.get_character_name() + "_wins.wav").c_str());
				al_play_sample(bgm, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
			}
			else if (*time_passed < 109) {

				if (*time_passed % 10 == 0) {
					al_draw_text(font4, al_map_rgb(0, 0, 0), 800 / 2, 110, ALLEGRO_ALIGN_CENTER, (player_1.get_character_name() + " WINS").c_str());
					al_draw_text(font3, al_map_rgb(255, 255, 255), 800 / 2, 110, ALLEGRO_ALIGN_CENTER, (player_1.get_character_name() + " WINS").c_str());
				}
				else {
					al_draw_text(font4, al_map_rgb(0, 0, 0), 800 / 2, 110, ALLEGRO_ALIGN_CENTER, (player_1.get_character_name() + " WINS").c_str());
					al_draw_text(font3, al_map_rgb(0, 0, 255), 800 / 2, 110, ALLEGRO_ALIGN_CENTER, (player_1.get_character_name() + " WINS").c_str());
				}

			}
			else if (*time_passed < 251) {

				al_draw_text(font4, al_map_rgb(0, 0, 0), 800 / 2, 110, ALLEGRO_ALIGN_CENTER, (player_1.get_character_name() + " WINS").c_str());
				al_draw_text(font3, al_map_rgb(255, 255, 255), 800 / 2, 110, ALLEGRO_ALIGN_CENTER, (player_1.get_character_name() + " WINS").c_str());

				//wait
			}
			else if ((*time_passed == 251) && player_1.get_health() == 100) {
				bgm = al_load_sample("../resources/sounds/announcer/Flawless_Victory.wav");
				al_play_sample(bgm, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
			}
			else if (*time_passed < 400 && player_1.get_health() == 100) {


				al_draw_text(font4, al_map_rgb(0, 0, 0), 800 / 2, 110, ALLEGRO_ALIGN_CENTER, (player_1.get_character_name() + " WINS").c_str());
				al_draw_text(font3, al_map_rgb(255, 255, 255), 800 / 2, 110, ALLEGRO_ALIGN_CENTER, (player_1.get_character_name() + " WINS").c_str());

				if (*time_passed % 5 == 0) {
					al_draw_text(font4, al_map_rgb(0, 0, 0), 800 / 2, 160, ALLEGRO_ALIGN_CENTER, "FLAWLESS VICTORY");
					al_draw_text(font3, al_map_rgb(255, 255, 255), 800 / 2, 160, ALLEGRO_ALIGN_CENTER, "FLAWLESS VICTORY");
				}
				else {
					al_draw_text(font4, al_map_rgb(0, 0, 0), 800 / 2, 160, ALLEGRO_ALIGN_CENTER, "FLAWLESS VICTORY");
					al_draw_text(font3, al_map_rgb(255, 0, 0), 800 / 2, 160, ALLEGRO_ALIGN_CENTER, "FLAWLESS VICTORY");
				}



			}
			else {
				std::cout << "\n MPAINW EDW RE MPAGASA??";
				player_1.reset_player_health();
				player_2.reset_player_health();
				player_1.tickAnim.sprite.set_x_pos(150);
				player_2.tickAnim.sprite.set_x_pos(650);

				*starting_now = 1;
				player_1.set_player_state(mk::IDLE);
				player_2.set_player_state(mk::IDLE);
				*time_passed = 0;

			}
		}

	}
	else {

		if (*time_passed == 2) {
			player_2.add_win();
		}
		if (player_2.is_victorious() == true) {
			//std::cout << "MPAINW EDW OEO";
			if (*time_passed == 2) {
				player_2.set_player_state(mk::IDLE);
				if (player_1.get_health() >= 0) {
					if (player_1.get_character_name() == "Sonya") {
						bgm = al_load_sample(("../resources/sounds/announcer/Finish_her.wav"));
						al_play_sample(bgm, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
						bgm = al_load_sample(("../resources/sounds/announcer/Finish_her.wav"));
						al_play_sample(bgm, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
					}
					else {
						bgm = al_load_sample(("../resources/sounds/announcer/Finish_him.wav"));
						al_play_sample(bgm, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
						bgm = al_load_sample(("../resources/sounds/announcer/Finish_him.wav"));
						al_play_sample(bgm, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
					}

					player_1.set_player_state(mk::DAZE); //should be changed to dazed or shame
				}
			}

			if (*time_passed < 120) {
				if (player_1.get_character_name() == "Sonya") {
					if (*time_passed % 5 == 0) {
						al_draw_text(font4, al_map_rgb(0, 0, 0), 800 / 2, 160, ALLEGRO_ALIGN_CENTER, "FINISH HER");
						al_draw_text(font3, al_map_rgb(255, 255, 255), 800 / 2, 160, ALLEGRO_ALIGN_CENTER, "FINISH HER");
					}
					else {
						al_draw_text(font4, al_map_rgb(0, 0, 0), 800 / 2, 160, ALLEGRO_ALIGN_CENTER, "FINISH HER");
						al_draw_text(font3, al_map_rgb(255, 0, 0), 800 / 2, 160, ALLEGRO_ALIGN_CENTER, "FINISH HER");
					}
				}
				else {
					if (*time_passed % 5 == 0) {
						al_draw_text(font4, al_map_rgb(0, 0, 0), 800 / 2, 160, ALLEGRO_ALIGN_CENTER, "FINISH HIM");
						al_draw_text(font3, al_map_rgb(255, 255, 255), 800 / 2, 160, ALLEGRO_ALIGN_CENTER, "FINISH HIM");
					}
					else {
						al_draw_text(font4, al_map_rgb(0, 0, 0), 800 / 2, 160, ALLEGRO_ALIGN_CENTER, "FINISH HIM");
						al_draw_text(font3, al_map_rgb(255, 0, 0), 800 / 2, 160, ALLEGRO_ALIGN_CENTER, "FINISH HIM");
					}

				}
			}
			if (player_1.get_state() == mk::DEAD || *time_passed == 500) {
				player_1.set_player_state(mk::DEAD);
				player_2.add_win();
				*time_passed = 0;
			}

		}
		else {
			if (*time_passed == 2) {
				player_2.set_player_state(mk::WIN);
				//player_1.add_win();
				if (player_1.get_health() > 0) {
					player_1.set_player_state(mk::DEAD); //should be changed to dazed or shame
				}

			}
			else if (*time_passed == 65) {

				//asto etsi den kserw ti paizei mhn alla3ei
				bgm = al_load_sample(("../resources/sounds/announcer/" + player_2.get_character_name() + "_wins.wav").c_str());
				al_play_sample(bgm, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
				bgm = al_load_sample(("../resources/sounds/announcer/" + player_2.get_character_name() + "_wins.wav").c_str());
				al_play_sample(bgm, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
			}
			else if (*time_passed < 109) {

				if (*time_passed % 10 == 0) {
					al_draw_text(font4, al_map_rgb(0, 0, 0), 800 / 2, 110, ALLEGRO_ALIGN_CENTER, (player_2.get_character_name() + " WINS").c_str());
					al_draw_text(font3, al_map_rgb(255, 255, 255), 800 / 2, 110, ALLEGRO_ALIGN_CENTER, (player_2.get_character_name() + " WINS").c_str());
				}
				else {
					al_draw_text(font4, al_map_rgb(0, 0, 0), 800 / 2, 110, ALLEGRO_ALIGN_CENTER, (player_2.get_character_name() + " WINS").c_str());
					al_draw_text(font3, al_map_rgb(0, 0, 255), 800 / 2, 110, ALLEGRO_ALIGN_CENTER, (player_2.get_character_name() + " WINS").c_str());
				}

			}
			else if (*time_passed < 251) {

				al_draw_text(font4, al_map_rgb(0, 0, 0), 800 / 2, 110, ALLEGRO_ALIGN_CENTER, (player_2.get_character_name() + " WINS").c_str());
				al_draw_text(font3, al_map_rgb(255, 255, 255), 800 / 2, 110, ALLEGRO_ALIGN_CENTER, (player_2.get_character_name() + " WINS").c_str());

				//wait
			}
			else if ((*time_passed == 251) && player_2.get_health() == 100) {
				bgm = al_load_sample("../resources/sounds/announcer/Flawless_Victory.wav");
				al_play_sample(bgm, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
			}
			else if (*time_passed < 400 && player_2.get_health() == 100) {


				al_draw_text(font4, al_map_rgb(0, 0, 0), 800 / 2, 110, ALLEGRO_ALIGN_CENTER, (player_2.get_character_name() + " WINS").c_str());
				al_draw_text(font3, al_map_rgb(255, 255, 255), 800 / 2, 110, ALLEGRO_ALIGN_CENTER, (player_2.get_character_name() + " WINS").c_str());

				if (*time_passed % 5 == 0) {
					al_draw_text(font4, al_map_rgb(0, 0, 0), 800 / 2, 160, ALLEGRO_ALIGN_CENTER, "FLAWLESS VICTORY");
					al_draw_text(font3, al_map_rgb(255, 255, 255), 800 / 2, 160, ALLEGRO_ALIGN_CENTER, "FLAWLESS VICTORY");
				}
				else {
					al_draw_text(font4, al_map_rgb(0, 0, 0), 800 / 2, 160, ALLEGRO_ALIGN_CENTER, "FLAWLESS VICTORY");
					al_draw_text(font3, al_map_rgb(255, 0, 0), 800 / 2, 160, ALLEGRO_ALIGN_CENTER, "FLAWLESS VICTORY");
				}



			}
			else {
				std::cout << "\n MPAINW EDW RE MPAGASA??";
				player_1.reset_player_health();
				player_2.reset_player_health();
				player_1.tickAnim.sprite.set_x_pos(150);
				player_2.tickAnim.sprite.set_x_pos(650);

				*starting_now = 1;
				player_1.set_player_state(mk::IDLE);
				player_2.set_player_state(mk::IDLE);
				*time_passed = 0;

			}
		}

	}
}



	keycode_code hashit(std::string const& inString) {
		if (inString == "Exit") return Exit;
		else if (inString == "Start") return Start;
		else if (inString == "Up")    return Up;
		else if (inString == "Down")    return Down;
		else if (inString == "Left")    return Left;
		else if (inString == "Right")    return Right;
		else if (inString == "High_Punch")    return High_Punch;
		else if (inString == "Low_Punch")    return Low_Punch;
		else if (inString == "High_Kick")    return High_Kick;
		else if (inString == "Low_Kick")    return Low_Kick;
		else if (inString == "Block") return Block;
		else return UNKNOWN;
	}

	void logic::state_machine_key_press(mk::Fighter &player, input_checker &input, ALLEGRO_EVENT ev, bool * done, int * curFrame) {
		input.check_key_pressed(ev);
		ALLEGRO_SAMPLE *player_sound;
		switch (hashit(input.get_key())) {

		case Exit:
			*done = true;
			break;

		case Left:
			if (player.get_state() == mk::IDLE) {
				if (player.get_stance() == mk::RIGHT) {
					player.set_player_state(mk::MOVING_BACKWARDS);
				}
				else {
					player.set_player_state(mk::MOVING_FORWARD);
				}
				*curFrame = 0;
			}
			break;

		case Right:
			if (player.get_state() == mk::IDLE) {
				if (player.get_stance() == mk::RIGHT) {
					player.set_player_state(mk::MOVING_FORWARD);
				}
				else {
					player.set_player_state(mk::MOVING_BACKWARDS);
				}
				*curFrame = 0;
			}
			break;

		case Up:
			if (player.get_state() != mk::MOVING_FORWARD && player.get_state() != mk::MOVING_BACKWARDS &&player.get_state() != mk::IDLE) {
				break;
			}
			//remeber to put sonya name sounds in male
			player_sound = al_load_sample((("../resources/sounds/male/" + player.get_character_name() + "_Jump.wav").c_str()));
			al_play_sample(player_sound, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
			if (player.get_state() == mk::IDLE) {


				player.set_player_state(mk::JUMP_STRAIGHT);
				*curFrame = 2;
			}
			else if (player.get_state() == mk::MOVING_FORWARD) {
				if (player.get_stance() == mk::RIGHT) {

					player.set_player_state(mk::TWIRL);
				}
				else {
					player.set_player_state(mk::TWIRL_BACK);
				}
				*curFrame = 0;
			}
			else if (player.get_state() == mk::MOVING_BACKWARDS) {
				if (player.get_stance() == mk::RIGHT) {

					player.set_player_state(mk::TWIRL_BACK);
				}
				else {
					player.set_player_state(mk::TWIRL);
				}
				*curFrame = 0;

			}
			break;

		case Down:
			if (player.get_state() == mk::IDLE) {
				player.set_player_state(mk::CROUCH);
				*curFrame = 0;
			}

			break;

		case High_Punch:
			if (player.get_state() == mk::CROUCH && player.get_state() != mk::UPPERCUT) {

				player.set_player_state(mk::UPPERCUT);
				*curFrame = 0;
			}
			else if (player.get_state() == mk::IDLE) {
				if (player.get_punch_side() == 0) {
					player_sound = al_load_sample(("../resources/sounds/male/" + player.get_character_name() + ".wav").c_str());
					al_play_sample(player_sound, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
				}
				player.set_player_state(mk::PUNCH_HIGH);
				*curFrame = 0;
			}
			else if (player.get_state() == mk::MOVING_FORWARD && player.get_collision() == true) {
				player.set_player_state(mk::BACK_HAND);
				*curFrame = 0;
			}
			else if (player.get_state() == mk::JUMP_STRAIGHT) {
				player.set_player_state(mk::JUMP_PUNCH_STRAIGHT);
				*curFrame = 0;
			}
			break;


		case Low_Punch:
			if (player.get_state() == mk::CROUCH && player.get_state() != mk::UPPERCUT) {

				player.set_player_state(mk::UPPERCUT);
				*curFrame = 0;
			}
			else if (player.get_state() == mk::IDLE) {
				if (player.get_punch_side() == 0) {
					player_sound = al_load_sample(("../resources/sounds/male/" + player.get_character_name() + ".wav").c_str());
					al_play_sample(player_sound, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
				}
				player.set_player_state(mk::PUNCH_LOW);
				*curFrame = 0;
			}
			else if (player.get_state() == mk::JUMP_STRAIGHT) {
				player_sound = al_load_sample(("../resources/sounds/male/" + player.get_character_name() + ".wav").c_str());
				al_play_sample(player_sound, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
				player.set_player_state(mk::JUMP_PUNCH_STRAIGHT);
				*curFrame = 0;
			}
			break;
		case High_Kick:
			//std::cout << "i get fucking pressed \n";
			if (player.get_state() == mk::IDLE) {
				player_sound = al_load_sample(("../resources/sounds/male/" + player.get_character_name() + ".wav").c_str());
				al_play_sample(player_sound, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
				player.set_player_state(mk::KICK_HIGH);
				*curFrame = 0;
			}
			else if (player.get_state() == mk::CROUCH) {
				player_sound = al_load_sample(("../resources/sounds/male/" + player.get_character_name() + ".wav").c_str());
				al_play_sample(player_sound, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
				player.set_player_state(mk::KICK_DOWN);
				*curFrame = 0;
			}
			else if (player.get_state() == mk::MOVING_BACKWARDS && player.get_wider_collision() == true) {
				player_sound = al_load_sample("../resources/sounds/hitsounds/twirl.wav");
				al_play_sample(player_sound, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
				player.set_player_state(mk::KICK_ROUNDHOUSE);
				*curFrame = 0;
			}
			else if (player.get_state() == mk::JUMP_STRAIGHT) {
				player_sound = al_load_sample(("../resources/sounds/male/" + player.get_character_name() + ".wav").c_str());
				al_play_sample(player_sound, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
				player.set_player_state(mk::JUMP_STRAIGHT_KICK);
				*curFrame = 0;
			}
			break;
		case Low_Kick:
			if (player.get_state() == mk::IDLE) {
				player_sound = al_load_sample(("../resources/sounds/male/" + player.get_character_name() + ".wav").c_str());
				al_play_sample(player_sound, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
				player.set_player_state(mk::KICK_LOW);
				*curFrame = 0;
			}
			else if (player.get_state() == mk::CROUCH) {
				player_sound = al_load_sample(("../resources/sounds/male/" + player.get_character_name() + ".wav").c_str());
				al_play_sample(player_sound, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
				player.set_player_state(mk::KICK_DOWN);
				*curFrame = 0;
			}
			else if (player.get_state() == mk::MOVING_BACKWARDS && player.get_wider_collision() == true) {
				//player_sound = al_load_sample("../resources/sounds/hitsounds/twirl.wav");
				//al_play_sample(player_sound, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
				player.set_player_state(mk::FOOT_SWEEP);
				*curFrame = 0;
			}
			else if (player.get_state() == mk::JUMP_STRAIGHT) {
				player_sound = al_load_sample(("../resources/sounds/male/" + player.get_character_name() + ".wav").c_str());
				al_play_sample(player_sound, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
				player.set_player_state(mk::JUMP_STRAIGHT_KICK);
				*curFrame = 0;
			}
			break;
		case Block:
			if (player.get_state() == mk::IDLE) {
				player.set_player_state(mk::BLOCK_HIGH);
				*curFrame = 0;
			}
			else if (player.get_state() == mk::CROUCH) {
				player.set_player_state(mk::BLOCK_LOW);
				*curFrame = 0;
			}

			break;

		case UNKNOWN:
			return;
		}




	}

	void logic::state_machine_key_release(mk::Fighter &player, input_checker &input, ALLEGRO_EVENT ev, bool * done, int * curFrame) {
		input.check_key_pressed(ev);

		switch (hashit(input.get_key())) {
		case Exit:
			*done = true;
			break;

		case Left:
			if (player.get_state() == mk::MOVING_FORWARD || player.get_state() == mk::MOVING_BACKWARDS)
				player.set_player_state(mk::IDLE);
			break;

		case Right:
			if (player.get_state() == mk::MOVING_FORWARD || player.get_state() == mk::MOVING_BACKWARDS)
				player.set_player_state(mk::IDLE);
			break;

		case Up:
			break;

		case Down:
			if (player.get_state() == mk::CROUCH) {
				*curFrame = player.tickAnimator.get_player_frame_count()["Crouch"];
				player.set_player_state(mk::CROUCH_UP);
			}

			break;

		case Block:
			if (player.get_state() == mk::BLOCK_HIGH) {
				*curFrame = player.tickAnimator.get_player_frame_count()["BlockHigh"];

				player.set_player_state(mk::BLOCK_HIGH_RELEASE);
			}
			else if (player.get_state() == mk::BLOCK_LOW) {
				*curFrame = player.tickAnimator.get_player_frame_count()["BlockLow"];


				player.set_player_state(mk::CROUCH);
			}

			break;
		}


	}

	void logic::animation_manager(mk::Fighter &player_n, int * frameDelay, int * maxFrame, int * curFrame, engine2d::TickAnimation & player, int ground_height) {

		ALLEGRO_SAMPLE *player_sound;
		if (player_n.get_state() == mk::IDLE) {
			if (player.sprite.get_y_pos() != ground_height) {
				player.sprite.set_y_pos(ground_height);
			}
			if (player_n.get_health() <= 0) {
				player_n.set_player_state(mk::DEAD);
				return;
			}


			*frameDelay = player_n.tickAnimator.get_player_frame_delays()["Idle"];
			player.sprite.frameWidth = player_n.tickAnimator.get_player_Widths()["Idle"];
			*maxFrame = player_n.tickAnimator.get_player_frame_count()["Idle"];
			player.start_position_x = 0;
			player.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player_n.get_character_name() + "/" + player_n.get_character_name() + "_idle.png").c_str());
			*maxFrame = player_n.tickAnimator.get_player_frame_count()["Idle"];

			if (++(*curFrame) >= *maxFrame)
				*curFrame = 0;
		}
		else if (player_n.get_state() == mk::DAZE) {
			player.sprite.set_y_pos(ground_height);

			*frameDelay = player_n.tickAnimator.get_player_frame_delays()["Daze"];
			player.sprite.frameWidth = player_n.tickAnimator.get_player_Widths()["Daze"];
			*maxFrame = player_n.tickAnimator.get_player_frame_count()["Daze"];
			player.start_position_x = 0;
			player.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player_n.get_character_name() + "/" + player_n.get_character_name() + "_Daze.png").c_str());
			*maxFrame = player_n.tickAnimator.get_player_frame_count()["Daze"];

			if (++(*curFrame) >= *maxFrame)
				*curFrame = 0;
		}
		else if (player_n.get_state() == mk::CROUCH) {
			*frameDelay = player_n.tickAnimator.get_player_frame_delays()["Crouch"];
			player.sprite.frameWidth = player_n.tickAnimator.get_player_Widths()["Crouch"];
			*maxFrame = player_n.tickAnimator.get_player_frame_count()["Crouch"];
			player.start_position_x = 0;
			player.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player_n.get_character_name() + "/" + player_n.get_character_name() + "_Crouch.png").c_str());
			if (++(*curFrame) >= *maxFrame) {

				*curFrame = *maxFrame;
			}
		}

		else if (player_n.get_state() == mk::KICK_HIGH) {
			if (player_n.get_stance() == mk::LEFT) {
				player.sprite.change_x_pos(-10);
			}


			*frameDelay = player_n.tickAnimator.get_player_frame_delays()["ReadyKick"];
			player.sprite.frameWidth = player_n.tickAnimator.get_player_Widths()["ReadyKick"];
			if (*maxFrame != 2) {
				*maxFrame = player_n.tickAnimator.get_player_frame_count()["ReadyKick"];
			}
			std::cout << "My max frame is " << *maxFrame << "\n";
			std::cout << "My curr frame is " << *curFrame << "\n";
			player.start_position_x = 0;
			player.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player_n.get_character_name() + "/" + player_n.get_character_name() + "_Ready_Kick.png").c_str());
			//player.sprite.set_y_pos(ground_height + 20);
			if (*maxFrame == 2) {

				*frameDelay = player_n.tickAnimator.get_player_frame_delays()["EndKick"];
				player.sprite.frameWidth = player_n.tickAnimator.get_player_Widths()["EndKick"];
				*maxFrame = player_n.tickAnimator.get_player_frame_count()["EndKick"];
				player.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player_n.get_character_name() + "/" + player_n.get_character_name() + "_Kick_High.png").c_str());
				if ((*curFrame)++ >= *maxFrame) {

					player_n.set_player_state(mk::KICK_RESTORE);
					player.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player_n.get_character_name() + "/" + player_n.get_character_name() + "_Ready_Kick.png").c_str());
					*curFrame = 4;
					*frameDelay = player_n.tickAnimator.get_player_frame_delays()["ReadyKick"];
					player.sprite.frameWidth = player_n.tickAnimator.get_player_Widths()["ReadyKick"];
					*maxFrame = player_n.tickAnimator.get_player_frame_count()["ReadyKick"];


				}
			}
			else if ((*curFrame)++ >= *maxFrame) {

				*maxFrame = 2;
				*curFrame = 1;
				player.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player_n.get_character_name() + "/" + player_n.get_character_name() + "_Kick_High.png").c_str());
				*frameDelay = player_n.tickAnimator.get_player_frame_delays()["EndKick"];
				player.sprite.frameWidth = player_n.tickAnimator.get_player_Widths()["EndKick"];
				*maxFrame = player_n.tickAnimator.get_player_frame_count()["EndKick"];

			}
		}
		else if (player_n.get_state() == mk::KICK_LOW) {

			if (player_n.get_stance() == mk::LEFT) {
				player.sprite.change_x_pos(-10);
			}

			*frameDelay = player_n.tickAnimator.get_player_frame_delays()["ReadyKick"];
			player.sprite.frameWidth = player_n.tickAnimator.get_player_Widths()["ReadyKick"];
			if (*maxFrame != 2) {
				*maxFrame = player_n.tickAnimator.get_player_frame_count()["ReadyKick"];
			}

			player.start_position_x = 0;
			player.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player_n.get_character_name() + "/" + player_n.get_character_name() + "_Ready_Kick.png").c_str());
			//player.sprite.set_y_pos(ground_height + 20);
			if (*maxFrame == 2) {

				*frameDelay = player_n.tickAnimator.get_player_frame_delays()["EndKick"];
				player.sprite.frameWidth = player_n.tickAnimator.get_player_Widths()["EndKick"];
				*maxFrame = player_n.tickAnimator.get_player_frame_count()["EndKick"];
				player.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player_n.get_character_name() + "/" + player_n.get_character_name() + "_Kick_Low.png").c_str());
				if ((*curFrame)++ >= *maxFrame) {

					player_n.set_player_state(mk::KICK_RESTORE);
					player.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player_n.get_character_name() + "/" + player_n.get_character_name() + "_Ready_Kick.png").c_str());
					*curFrame = 4;
					*frameDelay = player_n.tickAnimator.get_player_frame_delays()["ReadyKick"];
					player.sprite.frameWidth = player_n.tickAnimator.get_player_Widths()["ReadyKick"];
					*maxFrame = player_n.tickAnimator.get_player_frame_count()["ReadyKick"];

				}
			}
			else if ((*curFrame)++ >= *maxFrame) {

				*maxFrame = 2;
				*curFrame = 1;
				player.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player_n.get_character_name() + "/" + player_n.get_character_name() + "_Kick_Low.png").c_str());
				*frameDelay = player_n.tickAnimator.get_player_frame_delays()["EndKick"];
				player.sprite.frameWidth = player_n.tickAnimator.get_player_Widths()["EndKick"];
				*maxFrame = player_n.tickAnimator.get_player_frame_count()["EndKick"];
			}
		}
		else if (player_n.get_state() == mk::KICK_DOWN) {
			*frameDelay = player_n.tickAnimator.get_player_frame_delays()["DownKick"];
			player.sprite.frameWidth = player_n.tickAnimator.get_player_Widths()["DownKick"];
			*maxFrame = player_n.tickAnimator.get_player_frame_count()["DownKick"];
			player.start_position_x = 0;
			//player.sprite.set_y_pos(ground_height + 65);
			player.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player_n.get_character_name() + "/" + player_n.get_character_name() + "_Down_Kick.png").c_str());
			if (++(*curFrame) >= *maxFrame) {

				*curFrame = *maxFrame;
				player_n.set_player_state(mk::CROUCH);
			}
		}
		else if (player_n.get_state() == mk::JUMP_PUNCH_STRAIGHT) {
			*frameDelay = player_n.tickAnimator.get_player_frame_delays()["Jump_Punch"];
			player.sprite.frameWidth = player_n.tickAnimator.get_player_Widths()["Jump_Punch"];
			*maxFrame = player_n.tickAnimator.get_player_frame_count()["Jump_Punch"];
			player.start_position_x = 0;
			//player.sprite.set_y_pos(ground_height + 65);
			player.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player_n.get_character_name() + "/" + player_n.get_character_name() + "_Jump_Punch.png").c_str());
			if (++(*curFrame) >= *maxFrame) {

				*curFrame = *maxFrame;
				*curFrame = 0;
				player_n.set_player_state(mk::JUMP_STRAIGHT);

			}
		}
		else if (player_n.get_state() == mk::JUMP_STRAIGHT_KICK && player_n.get_character_name()!="Sonya") {
			*frameDelay = player_n.tickAnimator.get_player_frame_delays()["Jump_Straight_Kick"];
			player.sprite.frameWidth = player_n.tickAnimator.get_player_Widths()["Jump_Straight_Kick"];
			*maxFrame = player_n.tickAnimator.get_player_frame_count()["Jump_Straight_Kick"];
			player.start_position_x = 0;
			//player.sprite.set_y_pos(ground_height + 65);
			player.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player_n.get_character_name() + "/" + player_n.get_character_name() + "_Jump_Straight_Kick.png").c_str());
			if (++(*curFrame) >= *maxFrame) {

				*curFrame = *maxFrame;
				*curFrame = 0;
				player_n.set_player_state(mk::JUMP_STRAIGHT);

			}
		}
		else if (player_n.get_state() == mk::PUNCH_HIGH) {

			if (player_n.get_stance() == mk::LEFT) {
				player.sprite.change_x_pos(-15);
			}
			if (player_n.get_punch_side() == 0) {
				*frameDelay = player_n.tickAnimator.get_player_frame_delays()["PunchHigh1"];
				player.sprite.frameWidth = player_n.tickAnimator.get_player_Widths()["PunchHigh1"];
				*maxFrame = player_n.tickAnimator.get_player_frame_count()["PunchHigh1"];
				player.start_position_x = 0;
				player.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player_n.get_character_name() + "/" + player_n.get_character_name() + "_Punch_High1.png").c_str());
			}
			else {
				*frameDelay = player_n.tickAnimator.get_player_frame_delays()["PunchHigh2"];
				player.sprite.frameWidth = player_n.tickAnimator.get_player_Widths()["PunchHigh2"];
				*maxFrame = player_n.tickAnimator.get_player_frame_count()["PunchHigh2"];
				player.start_position_x = 0;
				player.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player_n.get_character_name() + "/" + player_n.get_character_name() + "_Punch_High2.png").c_str());
			}

			if (++(*curFrame) >= *maxFrame) {

				*curFrame = *maxFrame;
				player_n.set_player_state(mk::IDLE);
				player_n.change_punch_side();
				if (player_n.get_stance() == mk::LEFT) {
					player.sprite.change_x_pos(15 * (*curFrame));
				}
			}
		}
		else if (player_n.get_state() == mk::BACK_HAND) {

			if (player_n.get_stance() == mk::LEFT) {
				player.sprite.change_x_pos(-15);
			}

			*frameDelay = player_n.tickAnimator.get_player_frame_delays()["BackHand"];
			player.sprite.frameWidth = player_n.tickAnimator.get_player_Widths()["BackHand"];
			*maxFrame = player_n.tickAnimator.get_player_frame_count()["BackHand"];
			player.start_position_x = 0;
			player.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player_n.get_character_name() + "/" + player_n.get_character_name() + "_Punch_BackHand.png").c_str());


			if (++(*curFrame) >= *maxFrame) {
				*curFrame = *maxFrame;
				player_n.set_player_state(mk::IDLE);
				if (player_n.get_stance() == mk::LEFT) {
					player.sprite.change_x_pos(15 * (*curFrame));
				}
			}
		}
		else if (player_n.get_state() == mk::KICK_ROUNDHOUSE) {

			if (player_n.get_stance() == mk::LEFT) {
				player.sprite.change_x_pos(-15);
			}

			*frameDelay = player_n.tickAnimator.get_player_frame_delays()["KickRoundhouse"];
			player.sprite.frameWidth = player_n.tickAnimator.get_player_Widths()["KickRoundhouse"];
			*maxFrame = player_n.tickAnimator.get_player_frame_count()["KickRoundhouse"];
			player.start_position_x = 0;
			player.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player_n.get_character_name() + "/" + player_n.get_character_name() + "_Roundhouse_Kick.png").c_str());


			if (++(*curFrame) >= *maxFrame) {
				*curFrame = *maxFrame;
				player_n.set_player_state(mk::IDLE);
				if (player_n.get_stance() == mk::LEFT) {
					player.sprite.change_x_pos(15 * (*curFrame));
				}
			}

		}
		else if (player_n.get_state() == mk::FOOT_SWEEP) {

			if (player_n.get_stance() == mk::LEFT) {
				player.sprite.change_x_pos(-15);
			}

			*frameDelay = player_n.tickAnimator.get_player_frame_delays()["FootSweep"];
			player.sprite.frameWidth = player_n.tickAnimator.get_player_Widths()["FootSweep"];
			*maxFrame = player_n.tickAnimator.get_player_frame_count()["FootSweep"];
			player.start_position_x = 0;
			player.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player_n.get_character_name() + "/" + player_n.get_character_name() + "_Foot_Sweep.png").c_str());


			if (++(*curFrame) >= *maxFrame) {
				*curFrame = *maxFrame;
				player_n.set_player_state(mk::IDLE);
				if (player_n.get_stance() == mk::LEFT) {
					player.sprite.change_x_pos(15 * (*curFrame));
				}
			}

		}
		else if (player_n.get_state() == mk::PUNCH_LOW) {
			if (player_n.get_stance() == mk::LEFT) {
				player.sprite.change_x_pos(-10);
			}

			if (player_n.get_punch_side() == 0) {

				*frameDelay = player_n.tickAnimator.get_player_frame_delays()["PunchLow1"];
				player.sprite.frameWidth = player_n.tickAnimator.get_player_Widths()["PunchLow1"];
				*maxFrame = player_n.tickAnimator.get_player_frame_count()["PunchLow1"];
				player.start_position_x = 0;
				player.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player_n.get_character_name() + "/" + player_n.get_character_name() + "_Punch_Low1.png").c_str());
			}
			else {

				*frameDelay = player_n.tickAnimator.get_player_frame_delays()["PunchLow2"];
				player.sprite.frameWidth = player_n.tickAnimator.get_player_Widths()["PunchLow2"];
				*maxFrame = player_n.tickAnimator.get_player_frame_count()["PunchLow2"];
				player.start_position_x = 0;
				player.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player_n.get_character_name() + "/" + player_n.get_character_name() + "_Punch_Low2.png").c_str());
			}

			if (++(*curFrame) >= *maxFrame) {

				*curFrame = *maxFrame;
				player_n.set_player_state(mk::IDLE);
				player_n.change_punch_side();
				if (player_n.get_stance() == mk::LEFT) {
					player.sprite.change_x_pos(10 * (*curFrame));
				}

			}
		}

		else if (player_n.get_state() == mk::KICK_RESTORE) {
			if (player_n.get_stance() == mk::LEFT) {
				player.sprite.change_x_pos(8 * (*curFrame));
			}
			*frameDelay = player_n.tickAnimator.get_player_frame_delays()["ReadyKick"];
			player.sprite.frameWidth = player_n.tickAnimator.get_player_Widths()["ReadyKick"];
			*maxFrame = player_n.tickAnimator.get_player_frame_count()["ReadyKick"];
			player.start_position_x = 0;
			player.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player_n.get_character_name() + "/" + player_n.get_character_name() + "_Ready_Kick.png").c_str());

			if (--*  curFrame <= 0) {
				player_n.set_player_state(mk::IDLE);
			}
		}
		else if (player_n.get_state() == mk::BLOCK_LOW) {
			*frameDelay = player_n.tickAnimator.get_player_frame_delays()["BlockLow"];
			player.sprite.frameWidth = player_n.tickAnimator.get_player_Widths()["BlockLow"];
			*maxFrame = player_n.tickAnimator.get_player_frame_count()["BlockLow"];
			player.start_position_x = 0;
			//player.sprite.set_y_pos(ground_height + 65);
			player.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player_n.get_character_name() + "/" + player_n.get_character_name() + "_Block_Low.png").c_str());
			if (++(*curFrame) >= *maxFrame) {

				*curFrame = *maxFrame;
			}
		}
		else if (player_n.get_state() == mk::BLOCK_HIGH) {
			*frameDelay = player_n.tickAnimator.get_player_frame_delays()["BlockHigh"];
			player.sprite.frameWidth = player_n.tickAnimator.get_player_Widths()["BlockHigh"];
			*maxFrame = player_n.tickAnimator.get_player_frame_count()["BlockHigh"];
			player.start_position_x = 0;
			player.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player_n.get_character_name() + "/" + player_n.get_character_name() + "_Block_High.png").c_str());

			if (++(*curFrame) >= *maxFrame) {

				*curFrame = *maxFrame;
			}
		}
		else if (player_n.get_state() == mk::BLOCK_HIGH_RELEASE) {
			*frameDelay = player_n.tickAnimator.get_player_frame_delays()["BlockHigh"];
			player.sprite.frameWidth = player_n.tickAnimator.get_player_Widths()["BlockHigh"];
			*maxFrame = player_n.tickAnimator.get_player_frame_count()["BlockHigh"];
			player.start_position_x = 0;
			player.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player_n.get_character_name() + "/" + player_n.get_character_name() + "_Block_High.png").c_str());
			if (--*  curFrame <= 1) {
				player_n.set_player_state(mk::IDLE);
			}
		}

		else if (player_n.get_state() == mk::WIN) {
			if (player.sprite.get_y_pos() != ground_height) {
				player.sprite.set_y_pos(ground_height);
			}
			*frameDelay = player_n.tickAnimator.get_player_frame_delays()["Win"];
			player.sprite.frameWidth = player_n.tickAnimator.get_player_Widths()["Win"];
			*maxFrame = player_n.tickAnimator.get_player_frame_count()["Win"];
			player.start_position_x = 0;
			player.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player_n.get_character_name() + "/" + player_n.get_character_name() + "_Win.png").c_str());

			if (++(*curFrame) >= *maxFrame) {

				*curFrame = *maxFrame;
			}
		}
		else if (player_n.get_state() == mk::CROUCH_UP) {
			*frameDelay = player_n.tickAnimator.get_player_frame_delays()["Crouch"];
			player.sprite.frameWidth = player_n.tickAnimator.get_player_Widths()["Crouch"];
			*maxFrame = player_n.tickAnimator.get_player_frame_count()["Crouch"];
			player.start_position_x = 0;
			player.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player_n.get_character_name() + "/" + player_n.get_character_name() + "_Crouch.png").c_str());

			if (--*  curFrame <= 1) {
				player_n.set_player_state(mk::IDLE);
			}

		}
		else if (player_n.get_state() == mk::TWIRL) {
			*frameDelay = player_n.tickAnimator.get_player_frame_delays()["Twirl"];
			player.sprite.frameWidth = player_n.tickAnimator.get_player_Widths()["Twirl"];
			*maxFrame = player_n.tickAnimator.get_player_frame_count()["Twirl"];
			player.start_position_x = 0;
			player.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player_n.get_character_name() + "/" + player_n.get_character_name() + "_Twirl.png").c_str());
			if (*curFrame == 1) {
				player_sound = al_load_sample("../resources/sounds/hitsounds/twirl.wav");
				al_play_sample(player_sound, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
			}

			if (++(*curFrame) >= *maxFrame) {

				*curFrame = 1;
			}
		}
		else if (player_n.get_state() == mk::TWIRL_BACK) {
			*frameDelay = player_n.tickAnimator.get_player_frame_delays()["Twirl"];
			player.sprite.frameWidth = player_n.tickAnimator.get_player_Widths()["Twirl"];
			*maxFrame = player_n.tickAnimator.get_player_frame_count()["Twirl"];
			player.start_position_x = 0;
			player.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player_n.get_character_name() + "/" + player_n.get_character_name() + "_Twirl.png").c_str());
			if (*curFrame == 1) {
				player_sound = al_load_sample("../resources/sounds/hitsounds/twirl.wav");
				al_play_sample(player_sound, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
			}
			if (++(*curFrame) >= *maxFrame) {

				*curFrame = 1;
			}
		}
		else if (player_n.get_state() == mk::UPPERCUT) {
			*frameDelay = player_n.tickAnimator.get_player_frame_delays()["UpperCut"];
			player.sprite.frameWidth = player_n.tickAnimator.get_player_Widths()["UpperCut"];
			*maxFrame = player_n.tickAnimator.get_player_frame_count()["UpperCut"];
			player.start_position_x = 0;
			//player.sprite.set_y_pos(ground_height - 34);

			player.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player_n.get_character_name() + "/" + player_n.get_character_name() + "_Uppercut.png").c_str());
			if (*curFrame == 2) {
				player_sound = al_load_sample(("../resources/sounds/hitsounds/Uppercut.wav"));
				al_play_sample(player_sound, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
			}
			if (++(*curFrame) >= *maxFrame) {



				player_n.set_player_state(mk::IDLE);
			}
		}
		else if (player_n.get_state() == mk::MOVING_FORWARD) {
			*frameDelay = player_n.tickAnimator.get_player_frame_delays()["Moving"];
			player.start_position_x = 0;
			player.sprite.frameWidth = player_n.tickAnimator.get_player_Widths()["Moving"];
			*maxFrame = player_n.tickAnimator.get_player_frame_count()["Moving"];
			player.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player_n.get_character_name() + "/" + player_n.get_character_name() + "_Moving.png").c_str());
			*maxFrame = player_n.tickAnimator.get_player_frame_count()["Moving"];
			if (++(*curFrame) >= *maxFrame)
				*  curFrame = 0;

		}
		else if (player_n.get_state() == mk::MOVING_BACKWARDS) {
			*frameDelay = player_n.tickAnimator.get_player_frame_delays()["Moving"];
			player.start_position_x = 0;
			player.sprite.frameWidth = player_n.tickAnimator.get_player_Widths()["Moving"];
			*maxFrame = player_n.tickAnimator.get_player_frame_count()["Moving"];
			player.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player_n.get_character_name() + "/" + player_n.get_character_name() + "_Moving.png").c_str());
			*maxFrame = 0;
			if (--(*curFrame) <= *maxFrame)
				*  curFrame = player_n.tickAnimator.get_player_frame_count()["Moving"];
		}
		else if (player_n.get_state() == mk::JUMP_STRAIGHT) {
			*frameDelay = player_n.tickAnimator.get_player_frame_delays()["Jumping"];
			player.start_position_x = 0;
			player.sprite.frameWidth = player_n.tickAnimator.get_player_Widths()["Jumping"];
			*maxFrame = player_n.tickAnimator.get_player_frame_count()["Jumping"];
			player.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player_n.get_character_name() + "/" + player_n.get_character_name() + "_JumpStraight.png").c_str());

			if (--(*curFrame) <= 0) {
				*curFrame = 0;
			}


		}
		else if (player_n.get_state() == mk::CHANGING_STANCE) {
			*frameDelay = player_n.tickAnimator.get_player_frame_delays()["FlipStance"];
			player.start_position_x = 0;
			player.sprite.frameWidth = player_n.tickAnimator.get_player_Widths()["FlipStance"];
			*maxFrame = player_n.tickAnimator.get_player_frame_count()["FlipStance"];
			player.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player_n.get_character_name() + "/" + player_n.get_character_name() + "_FlipStance.png").c_str());

			if (++(*curFrame) >= *maxFrame) {
				*curFrame = 0;
				player_n.set_player_state(mk::IDLE);
				if (player_n.get_stance() == mk::RIGHT) {
					player_n.set_stance(mk::LEFT);
				}
				else {
					player_n.set_stance(mk::RIGHT);
				}

			}
		}

		else if (player_n.get_state() == mk::HIT_HARD) {
			*frameDelay = player_n.tickAnimator.get_player_frame_delays()["HitHard"];
			player.sprite.frameWidth = player_n.tickAnimator.get_player_Widths()["HitHard"];
			*maxFrame = player_n.tickAnimator.get_player_frame_count()["HitHard"];
			player.start_position_x = 0;
			player.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player_n.get_character_name() + "/" + player_n.get_character_name() + "_Hit_Hard.png").c_str());

			if (++(*curFrame) >= *maxFrame) {

				*curFrame = *maxFrame;
			}
		}
		else if (player_n.get_state() == mk::HIT_HEAD) {
			*frameDelay = player_n.tickAnimator.get_player_frame_delays()["HitHead"];
			player.sprite.frameWidth = player_n.tickAnimator.get_player_Widths()["HitHead"];
			*maxFrame = player_n.tickAnimator.get_player_frame_count()["HitHead"];
			player.start_position_x = 0;
			player.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player_n.get_character_name() + "/" + player_n.get_character_name() + "_Head_Hit.png").c_str());

			if (++(*curFrame) >= *maxFrame) {

				*curFrame = 0;
				player_n.set_player_state(mk::IDLE);
			}
		}
		else if (player_n.get_state() == mk::HIT_LOW) {
			*frameDelay = player_n.tickAnimator.get_player_frame_delays()["HitLow"];
			player.sprite.frameWidth = player_n.tickAnimator.get_player_Widths()["HitLow"];
			*maxFrame = player_n.tickAnimator.get_player_frame_count()["HitLow"];
			player.start_position_x = 0;
			player.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player_n.get_character_name() + "/" + player_n.get_character_name() + "_Low_hit.png").c_str());

			if (++(*curFrame) >= *maxFrame) {

				*curFrame = 0;
				player_n.set_player_state(mk::IDLE);
			}
		}
		else if (player_n.get_state() == mk::TRIP) {

			*frameDelay = player_n.tickAnimator.get_player_frame_delays()["Trip"];
			player.sprite.frameWidth = player_n.tickAnimator.get_player_Widths()["Trip"];
			*maxFrame = player_n.tickAnimator.get_player_frame_count()["Trip"];
			player.start_position_x = 0;
			player.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player_n.get_character_name() + "/" + player_n.get_character_name() + "_Trip.png").c_str());

			if (++(*curFrame) >= *maxFrame) {

				*curFrame = 0;
				player_n.set_player_state(mk::GETUP);
			}
		}
		else if (player_n.get_state() == mk::GETUP) {
			player.sprite.set_y_pos(ground_height);
			if (player_n.get_health() <= 0) {

				player_n.set_player_state(mk::DEAD);
				player.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player_n.get_character_name() + "/" + player_n.get_character_name() + "_Hit_Hard.png").c_str());
				*curFrame = 6;
			}
			else {

				*frameDelay = player_n.tickAnimator.get_player_frame_delays()["GetUp"];
				player.sprite.frameWidth = player_n.tickAnimator.get_player_Widths()["GetUp"];
				*maxFrame = player_n.tickAnimator.get_player_frame_count()["GetUp"];
				player.start_position_x = 0;
				player.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player_n.get_character_name() + "/" + player_n.get_character_name() + "_Get_Up.png").c_str());

				if ((*curFrame)++ >= *maxFrame) {
					player.sprite.set_y_pos(ground_height);
					player.sprite.frameWidth = player_n.tickAnimator.get_player_Widths()["Idle"];
					player.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player_n.get_character_name() + "/" + player_n.get_character_name() + "_Idle.png").c_str());
					*curFrame = 0;
					player_n.set_player_state(mk::IDLE);



				}
			}

		}
		else if (player_n.get_state() == mk::DEAD) {
			//player.sprite.set_y_pos(ground_height +30);
			*frameDelay = player_n.tickAnimator.get_player_frame_delays()["HitHard"];
			player.sprite.frameWidth = player_n.tickAnimator.get_player_Widths()["HitHard"];
			player.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player_n.get_character_name() + "/" + player_n.get_character_name() + "_Hit_Hard.png").c_str());

			*curFrame = 6;

		}

	}



	void logic::movement_manager(mk::Fighter& player_1, engine2d::TickAnimation& player1, engine2d::TickAnimation& player2, engine2d::background& arena, int width, int frameWidth, int speed_x, int * done_up, int max_jump_height, int ground_height, int speed_y, mk::Fighter& player_2, int * curFrame) {
		ALLEGRO_SAMPLE *player_sound;
		ALLEGRO_SAMPLE *player_sound2;

		if (player_1.get_state() == mk::MOVING_BACKWARDS) {

			if (player_1.get_stance() == mk::RIGHT) {
				//moving left

				if (player1.sprite.get_x_pos() - speed_x > 0) {
					player1.sprite.change_x_pos(-speed_x);
				}
				else if ((arena.fg_curr) >= (arena.fg_min_curr) && !(player2.sprite.get_x_pos() >= width - frameWidth)) {

					arena.change_fg_curr(-(arena.fg_scale));
					player2.sprite.change_x_pos(arena.fg_scale);
					if (arena.bg_curr >= arena.bg_min_curr) {
						arena.change_bg_curr(-(arena.bg_scale_backwards));
					}
				}
			}
			else if (player_1.get_stance() == mk::LEFT) {
				if (player1.sprite.get_x_pos() < width - 3 * frameWidth / 2) {
					player1.sprite.change_x_pos(speed_x);
				}
				else if ((arena.fg_curr) <= (arena.fg_max_curr) && !(player2.sprite.get_x_pos() <= 5)) {

					arena.change_fg_curr(arena.fg_scale);
					player2.sprite.change_x_pos(-arena.fg_scale);
					if ((arena.bg_curr) <= (arena.bg_max_curr)) {
						arena.change_bg_curr(arena.bg_scale_forward);
					}
				}
			}
		}

		if (player_1.get_state() == mk::MOVING_FORWARD) {

			if (player_1.get_stance() == mk::RIGHT) {
				//moving right
				if (player_1.get_collision() == false || player_2.get_state() == mk::TWIRL_BACK || player_2.get_state() == mk::JUMP_STRAIGHT) {
					if (player1.sprite.get_x_pos() < width - frameWidth) {
						player1.sprite.change_x_pos(speed_x);
					}
					else if ((arena.fg_curr) <= (arena.fg_max_curr) && !(player2.sprite.get_x_pos() <= 5)) {

						arena.change_fg_curr(arena.fg_scale);
						player2.sprite.change_x_pos(-arena.fg_scale);
						if ((arena.bg_curr) <= (arena.bg_max_curr)) {
							arena.change_bg_curr(arena.bg_scale_forward);
						}

					}
				}

			}
			else if (player_1.get_stance() == mk::LEFT) {
				//moving left
				if (player_1.get_collision() == false || player_2.get_state() == mk::TWIRL || player_2.get_state() == mk::JUMP_STRAIGHT) {
					if (player1.sprite.get_x_pos() - speed_x > 0) {
						player1.sprite.change_x_pos(-speed_x);
					}
					else if ((arena.fg_curr) >= (arena.fg_min_curr) && !(player2.sprite.get_x_pos() >= width - frameWidth)) {

						arena.change_fg_curr(-(arena.fg_scale));
						player2.sprite.change_x_pos(arena.fg_scale);
						if (arena.bg_curr >= arena.bg_min_curr) {
							arena.change_bg_curr(-(arena.bg_scale_backwards));
						}

					}

				}
			}

		}

		if (player_1.get_state() == mk::JUMP_STRAIGHT) {

			if (player1.sprite.get_y_pos() <= -max_jump_height + ground_height) {

				*done_up = 1;
			}
			if (*done_up == 1) {
				player1.sprite.change_y_pos(speed_y);
			}
			else {
				player1.sprite.change_y_pos(-speed_y);
			}
			if (player1.sprite.get_y_pos() == ground_height && (*done_up == 1)) {
				*done_up = 0;
				*curFrame = 2;
				player_1.set_player_state(mk::IDLE);

			}
		}


		if (player_1.get_state() == mk::JUMP_PUNCH_STRAIGHT) {

			if (player1.sprite.get_y_pos() <= -max_jump_height + ground_height) {

				*done_up = 1;
			}
			if (*done_up == 1) {
				player1.sprite.change_y_pos(speed_y);
			}
			else {
				player1.sprite.change_y_pos(-speed_y);
			}
			if (player1.sprite.get_y_pos() == ground_height && (*done_up == 1)) {
				*done_up = 0;
				*curFrame = 2;
				player_1.set_player_state(mk::IDLE);

			}
		}

		if (player_1.get_state() == mk::JUMP_STRAIGHT_KICK) {

			if (player1.sprite.get_y_pos() <= -max_jump_height + ground_height) {

				*done_up = 1;
			}
			if (*done_up == 1) {
				player1.sprite.change_y_pos(speed_y);
			}
			else {
				player1.sprite.change_y_pos(-speed_y);
			}
			if (player1.sprite.get_y_pos() == ground_height && (*done_up == 1)) {
				*done_up = 0;
				*curFrame = 2;
				player_1.set_player_state(mk::IDLE);

			}
		}

		if (player_1.get_state() == mk::TWIRL) {

			//printf("my y pos is %d", player1.sprite.get_y_pos());

			if (player1.sprite.get_x_pos() < width - frameWidth) {
				player1.sprite.change_x_pos(3);
			}


			if (player1.sprite.get_y_pos() <= -max_jump_height + ground_height) {
				player1.sprite.change_x_pos(5);
				*done_up = 1;
			}
			if (*done_up == 1) {
				player1.sprite.change_y_pos(speed_y);
			}
			else {
				player1.sprite.change_y_pos(-speed_y);
			}


			if (player1.sprite.get_y_pos() >= ground_height - 10 && (*done_up == 1)) {
				*done_up = 0;

				player_1.set_player_state(mk::IDLE);

			}

		}
		if (player_1.get_state() == mk::TWIRL_BACK) {

			//printf("my y pos is %d", player1.sprite.get_y_pos());

			if (player1.sprite.get_x_pos() - 3 > 0) {
				player1.sprite.change_x_pos(-3);
			}

			if (player1.sprite.get_y_pos() <= -max_jump_height + ground_height) {
				player1.sprite.change_x_pos(-5);
				*done_up = 1;
			}
			if (*done_up == 1) {
				player1.sprite.change_y_pos(speed_y);
			}
			else {
				player1.sprite.change_y_pos(-speed_y);
			}
			if (player1.sprite.get_y_pos() >= ground_height - 10 && (*done_up == 1)) {
				*done_up = 0;

				player_1.set_player_state(mk::IDLE);

			}

		}
		if (player_2.get_state() == mk::UPPERCUT) {
			if (player_1.get_collision() == true && player_1.get_state() != mk::CROUCH && player_1.get_state() != mk::BLOCK_HIGH && player_1.get_state() != mk::BLOCK_LOW && player_1.get_state() != mk::HIT_HARD) {
				player_1.set_player_state(mk::HIT_HARD);
				*curFrame = 0;
				*done_up = 0;
				player_1.change_health(-10); /// note this value is for testing
				player_sound = al_load_sample("../resources/sounds/hitsounds/Uppercut_hit.wav");
				al_play_sample(player_sound, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);

				player_sound = al_load_sample(("../resources/sounds/male/" + player_1.get_character_name() + "_Hard_Hit.wav").c_str()); // to be changed not same for all
				al_play_sample(player_sound, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);

			}

		}
		if (player_2.get_state() == mk::PUNCH_HIGH) {
			if ((player_1.get_collision() == 1 || player_2.get_collision()==1) && player_1.get_state() != mk::CROUCH && player_1.get_state() != mk::BLOCK_HIGH && player_1.get_state() != mk::BLOCK_LOW && player_1.get_state() != mk::HIT_HEAD) {

				player_1.set_player_state(mk::HIT_HEAD);
				*curFrame = 0;
				*done_up = 0;
				player_1.change_health(-5); /// note this value is for testing
				player_sound = al_load_sample("../resources/sounds/hitsounds/punch1.wav");
				al_play_sample(player_sound, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
				player_sound = al_load_sample(("../resources/sounds/male/" + player_1.get_character_name() + "_Hit.wav").c_str());
				al_play_sample(player_sound, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);


			}

		}
		if (player_2.get_state() == mk::JUMP_PUNCH_STRAIGHT) {
			if (player_1.get_wider_collision() == true && player_1.get_state() != mk::CROUCH && player_1.get_state() != mk::BLOCK_HIGH && player_1.get_state() != mk::BLOCK_LOW && player_1.get_state() != mk::HIT_HEAD) {
				std::cout << "\nPlayer1 " << player_1.tickAnim.sprite.get_y_pos();
				std::cout << "\nPlayer2 " << player_2.tickAnim.sprite.get_y_pos();
				if (player_2.tickAnim.sprite.get_y_pos() == player_1.tickAnim.sprite.get_y_pos() || player_2.tickAnim.sprite.get_y_pos() == player_1.tickAnim.sprite.get_y_pos() - 100 || player_2.tickAnim.sprite.get_y_pos() == player_1.tickAnim.sprite.get_y_pos() - 101 || player_2.tickAnim.sprite.get_y_pos() == player_1.tickAnim.sprite.get_y_pos() - 133 || player_2.tickAnim.sprite.get_y_pos() == player_1.tickAnim.sprite.get_y_pos() - 105) {
					player_1.set_player_state(mk::HIT_HEAD);
					*curFrame = 0;
					*done_up = 0;
					player_1.change_health(-5); /// note this value is for testing
					player_sound = al_load_sample("../resources/sounds/hitsounds/punch1.wav");
					al_play_sample(player_sound, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
					player_sound = al_load_sample(("../resources/sounds/male/" + player_1.get_character_name() + "_Hit.wav").c_str());
					al_play_sample(player_sound, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);

				}

			}

		}

		if (player_2.get_state() == mk::JUMP_STRAIGHT_KICK) {
			if (player_1.get_wider_collision() == true && player_1.get_state() != mk::CROUCH && player_1.get_state() != mk::BLOCK_HIGH && player_1.get_state() != mk::BLOCK_LOW && player_1.get_state() != mk::HIT_HEAD) {
				std::cout << "\nPlayer1 " << player_1.tickAnim.sprite.get_y_pos();
				std::cout << "\nPlayer2 " << player_2.tickAnim.sprite.get_y_pos();
				if (player_2.tickAnim.sprite.get_y_pos() == player_1.tickAnim.sprite.get_y_pos() || player_2.tickAnim.sprite.get_y_pos() == player_1.tickAnim.sprite.get_y_pos() - 100 || player_2.tickAnim.sprite.get_y_pos() == player_1.tickAnim.sprite.get_y_pos() - 101 || player_2.tickAnim.sprite.get_y_pos() == player_1.tickAnim.sprite.get_y_pos() - 133 || player_2.tickAnim.sprite.get_y_pos() == player_1.tickAnim.sprite.get_y_pos() - 105) {
					player_1.set_player_state(mk::HIT_HEAD);
					*curFrame = 0;
					*done_up = 0;
					player_1.change_health(-5); /// note this value is for testing
					player_sound = al_load_sample("../resources/sounds/hitsounds/punch1.wav");
					al_play_sample(player_sound, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
					player_sound = al_load_sample(("../resources/sounds/male/" + player_1.get_character_name() + "_Hit.wav").c_str());
					al_play_sample(player_sound, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);

				}

			}

		}



		if (player_2.get_state() == mk::PUNCH_LOW) {
			if (player_1.get_collision() == true && player_1.get_state() != mk::CROUCH && player_1.get_state() != mk::BLOCK_HIGH && player_1.get_state() != mk::BLOCK_LOW && player_1.get_state() != mk::HIT_LOW) {

				player_1.set_player_state(mk::HIT_LOW);
				*curFrame = 0;
				*done_up = 0;
				player_1.change_health(-5); /// note this value is for testing
				player_sound = al_load_sample("../resources/sounds/hitsounds/punch1.wav");
				al_play_sample(player_sound, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
				player_sound = al_load_sample(("../resources/sounds/male/" + player_1.get_character_name() + "_Hit.wav").c_str());
				al_play_sample(player_sound, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);


			}

		}
		if (player_2.get_state() == mk::KICK_DOWN) {
			if (player_1.get_collision() == true && player_1.get_state() != mk::CROUCH && player_1.get_state() != mk::BLOCK_HIGH && player_1.get_state() != mk::BLOCK_LOW && player_1.get_state() != mk::HIT_LOW) {

				player_1.set_player_state(mk::HIT_LOW);
				*curFrame = 0;
				*done_up = 0;
				player_1.change_health(-5); /// note this value is for testing
				player_sound = al_load_sample("../resources/sounds/hitsounds/punch1.wav");
				al_play_sample(player_sound, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
				player_sound = al_load_sample(("../resources/sounds/male/" + player_1.get_character_name() + "_Hit.wav").c_str());
				al_play_sample(player_sound, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);


			}

		}


		if (player_2.get_state() == mk::KICK_LOW) {
			if (player_1.get_collision() == true && player_1.get_state() != mk::CROUCH && player_1.get_state() != mk::BLOCK_HIGH && player_1.get_state() != mk::BLOCK_LOW && player_1.get_state() != mk::HIT_LOW) {

				player_1.set_player_state(mk::HIT_HEAD);
				if (player_1.get_stance() == mk::LEFT) {
					player_1.tickAnim.sprite.change_x_pos(35);
				}
				else {
					player_1.tickAnim.sprite.change_x_pos(-35);
				}
				*curFrame = 0;
				*done_up = 0;
				player_1.change_health(-5); /// note this value is for testing
				player_sound = al_load_sample("../resources/sounds/hitsounds/High_Kick.wav");
				al_play_sample(player_sound, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
				player_sound = al_load_sample(("../resources/sounds/male/" + player_1.get_character_name() + "_Hit.wav").c_str());
				al_play_sample(player_sound, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);


			}

		}

		if (player_2.get_state() == mk::BACK_HAND) {
			if (player_1.get_collision() == true && player_1.get_state() != mk::CROUCH && player_1.get_state() != mk::BLOCK_HIGH && player_1.get_state() != mk::BLOCK_LOW && player_1.get_state() != mk::HIT_HEAD) {

				player_1.set_player_state(mk::HIT_HEAD);
				*curFrame = 0;
				*done_up = 0;
				if (player_2.get_stance() == mk::RIGHT) {
					player_2.tickAnim.sprite.change_x_pos(35);
				}
				else {
					player_2.tickAnim.sprite.change_x_pos(-35);
				}
				if (player_1.get_stance() == mk::LEFT) {
					player_1.tickAnim.sprite.change_x_pos(20);
				}
				else {
					player_1.tickAnim.sprite.change_x_pos(-20);
				}
				player_1.change_health(-5); /// note this value is for testing
				player_sound = al_load_sample("../resources/sounds/hitsounds/punch2.wav");
				al_play_sample(player_sound, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
				player_sound = al_load_sample(("../resources/sounds/male/" + player_1.get_character_name() + "_Hit.wav").c_str());
				al_play_sample(player_sound, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);


			}

		}

		if (player_2.get_state() == mk::KICK_HIGH) {
			if (player_1.get_collision() == true && player_1.get_state() != mk::CROUCH && player_1.get_state() != mk::BLOCK_HIGH && player_1.get_state() != mk::BLOCK_LOW && player_1.get_state() != mk::HIT_HEAD) {

				player_1.set_player_state(mk::HIT_HEAD);
				if (player_1.get_stance() == mk::LEFT) {
					player_1.tickAnim.sprite.change_x_pos(35);
				}
				else {
					player_1.tickAnim.sprite.change_x_pos(-35);
				}
				*curFrame = 0;
				*done_up = 0;
				player_1.change_health(-5); /// note this value is for testing
				player_sound = al_load_sample("../resources/sounds/hitsounds/High_Kick.wav");
				al_play_sample(player_sound, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
				player_sound = al_load_sample(("../resources/sounds/male/" + player_1.get_character_name() + "_Hit.wav").c_str());
				al_play_sample(player_sound, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);


			}

		}
		if (player_2.get_state() == mk::KICK_ROUNDHOUSE) {
			if (player_1.get_collision() == true && player_1.get_state() != mk::CROUCH && player_1.get_state() != mk::BLOCK_HIGH && player_1.get_state() != mk::BLOCK_LOW && player_1.get_state() != mk::HIT_HEAD) {

				player_1.set_player_state(mk::HIT_HEAD);
				if (player_1.get_stance() == mk::LEFT) {
					player_1.tickAnim.sprite.change_x_pos(13);
				}
				else {
					player_1.tickAnim.sprite.change_x_pos(-13);
				}
				*curFrame = 0;
				*done_up = 0;
				player_1.change_health(-5); /// note this value is for testing
				player_sound = al_load_sample("../resources/sounds/hitsounds/roundhouse_kick.wav");
				al_play_sample(player_sound, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
				player_sound = al_load_sample(("../resources/sounds/male/" + player_1.get_character_name() + "_Hit.wav").c_str());
				al_play_sample(player_sound, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);


			}

		}
		if (player_2.get_state() == mk::FOOT_SWEEP) {
			if (player_1.get_wider_collision() == true && player_1.get_state() != mk::CROUCH && player_1.get_state() != mk::BLOCK_LOW  && player_1.get_state() != mk::HIT_HEAD && player_1.get_state() != mk::TRIP) {

				player_1.set_player_state(mk::TRIP);
				/*if (player_1.get_stance() == mk::LEFT) {
					player_1.tickAnim.sprite.change_x_pos(13);
				}
				else {
					player_1.tickAnim.sprite.change_x_pos(-13);
				}*/
				*curFrame = 0;
				*done_up = 0;
				player_1.change_health(-5); /// note this value is for testing
				/*player_sound = al_load_sample("../resources/sounds/hitsounds/roundhouse_kick.wav");
				al_play_sample(player_sound, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
				player_sound = al_load_sample("../resources/sounds/male/SubZero_Hit.wav");
				al_play_sample(player_sound, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);*/


			}

		}

		if (player_1.get_state() == mk::HIT_HEAD) {

		}

		if (player_1.get_state() == mk::HIT_HARD) {


			if (player_1.get_stance() == mk::RIGHT) {
				if (player1.sprite.get_x_pos() - 3 > 0) {
					player1.sprite.change_x_pos(-3);
				}
				std::cout << player1.sprite.get_x_pos() << "\n";
				if (player1.sprite.get_y_pos() <= -max_jump_height - 50 + ground_height) {
					player1.sprite.change_x_pos(-4);
					*done_up = 1;
					player_sound = al_load_sample("../resources/sounds/announcer/excellent.wav");
					al_play_sample(player_sound, 1.5, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
				}
				if (*done_up == 1) {
					player1.sprite.change_y_pos(speed_y + 2);
				}
				else {
					player1.sprite.change_y_pos(-speed_y - 2);
				}
				//shake
				if (player1.sprite.get_y_pos() >= ground_height - 40 && (*done_up == 1)) {

					if (arena.shake_up == false) {
						arena.bg_y -= 9;
						arena.fg_y -= 9;
						player1.sprite.change_y_pos(-9);
						player2.sprite.change_y_pos(-9);
						arena.shake_up = true;
					}
					else
					{
						arena.bg_y += 9;
						arena.fg_y += 9;
						player1.sprite.change_y_pos(9);
						player2.sprite.change_y_pos(9);
						arena.shake_up = false;
					}
				}
				if (player1.sprite.get_y_pos() >= ground_height - 10 && (*done_up == 1)) {
					*done_up = 0;
					*curFrame = 0;
					player1.sprite.change_y_pos(60);
					player_1.set_player_state(mk::GETUP);

				}
			}
			else {
				if (player1.sprite.get_x_pos() < width - frameWidth) {
					player1.sprite.change_x_pos(3);
				}


				if (player1.sprite.get_y_pos() <= -max_jump_height - 50 + ground_height) {
					player1.sprite.change_x_pos(4);
					*done_up = 1;
					player_sound = al_load_sample("../resources/sounds/announcer/excellent.wav");
					al_play_sample(player_sound, 1.5, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
				}
				if (*done_up == 1) {
					player1.sprite.change_y_pos(speed_y + 2);
				}
				else {
					player1.sprite.change_y_pos(-speed_y - 2);
				}


				if (player1.sprite.get_y_pos() >= ground_height - 40 && (*done_up == 1)) {

					if (arena.shake_up == false) {
						arena.bg_y -= 5;
						arena.fg_y -= 5;
						player1.sprite.change_y_pos(-5);
						player2.sprite.change_y_pos(-5);
						arena.shake_up = true;
					}
					else
					{
						arena.bg_y += 5;
						arena.fg_y += 5;
						player1.sprite.change_y_pos(5);
						player2.sprite.change_y_pos(5);
						arena.shake_up = false;
					}
				}


				if (player1.sprite.get_y_pos() >= ground_height - 10 && (*done_up == 1)) {
					*done_up = 0;
					*curFrame = 0;
					player1.sprite.change_y_pos(60);
					player_sound2 = al_load_sample("../resources/sounds/hitsounds/GroundHit_2.wav");
					al_play_sample(player_sound2, 2.4, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
					player_1.set_player_state(mk::GETUP);

				}


			}
		}


		if (player_1.get_stance() == mk::RIGHT && (player2.sprite.get_x_pos() < player1.sprite.get_x_pos()) && (player1.sprite.get_y_pos() == player2.sprite.get_y_pos() || player_2.get_state() == mk::CROUCH) && player_1.get_state() != mk::TWIRL && player_1.get_state() != mk::TWIRL_BACK && player_1.get_state() != mk::CHANGING_STANCE) {
			//player_1.set_stance(mk::LEFT);
			player_1.set_player_state(mk::CHANGING_STANCE);
			*curFrame = 0;

		}
		else if (player_1.get_stance() == mk::LEFT && (player2.sprite.get_x_pos() > player1.sprite.get_x_pos()) && (player1.sprite.get_y_pos() == player2.sprite.get_y_pos() || player_2.get_state() == mk::CROUCH) && player_1.get_state() != mk::TWIRL && player_1.get_state() != mk::TWIRL_BACK && player_1.get_state() != mk::CHANGING_STANCE) {
			//player_1.set_stance(mk::RIGHT);

			player_1.set_player_state(mk::CHANGING_STANCE);
			*curFrame = 0;
		}



	}
	void logic::toast_checker(mk::Fighter & player_1, mk::Fighter & player_2, engine2d::toasty & toast)
	{
		if (player_2.get_state() == mk::UPPERCUT) {

			if (player_1.get_wider_collision() == true && player_1.get_state() != mk::CROUCH && player_1.get_state() != mk::BLOCK_HIGH && player_1.get_state() != mk::BLOCK_LOW && player_1.get_state() != mk::HIT_HARD) {

				if (player_1.tickAnim.sprite.get_y_pos() < 320) {

					toast.disabled = 0;
				}

			}

		}
	}









