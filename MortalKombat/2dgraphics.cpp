#include "2dgraphics.h"
#include "Fighter.h"

void engine2d::healthbar(int x, int y, int w, int h, int percent, std::string name, ALLEGRO_FONT* font)
{
	const char * name2 = name.c_str();

	al_draw_rectangle(x, y, x + w, y + h, al_map_rgb(255, 255, 0), 5);
	al_draw_filled_rectangle(x, y, x + w, y + h, al_map_rgb(255, 0, 0));

	if (percent > 0)
		al_draw_filled_rectangle(x, y, x + (w*percent / 100), y + h, al_map_rgb(0, 255, 0));

	al_draw_text(font, al_map_rgb(0, 0, 0), x + 81, y - 2, ALLEGRO_ALIGN_CENTER, name2);
	al_draw_text(font, al_map_rgb(255, 255, 2), x + 80, y - 4, ALLEGRO_ALIGN_CENTER, name2);
}

void engine2d::fade_out() {
	float alpha_from_0_to_1 = 0;
	while (alpha_from_0_to_1 < 1) {
		al_draw_filled_rectangle(0, 0, 800, 600, al_map_rgba(0, 0, 0, alpha_from_0_to_1));
		alpha_from_0_to_1 += 20;
		al_flip_display();
	}
}

std::map<std::string, int> engine2d::choose_correct_width(std::string fighter_name) {
	ALLEGRO_CONFIG* cfg = al_load_config_file("../resources/config.cfg");
	std::map<std::string, int> player_Widths;
	player_Widths["Idle"] = atoi(al_get_config_value(cfg, (fighter_name + "_Widths").c_str(), "Idle"));
	player_Widths["Moving"] = atoi(al_get_config_value(cfg, (fighter_name + "_Widths").c_str(), "Moving"));
	player_Widths["Jumping"] = atoi(al_get_config_value(cfg, (fighter_name + "_Widths").c_str(), "Jumping"));
	player_Widths["Crouch"] = atoi(al_get_config_value(cfg, (fighter_name + "_Widths").c_str(), "Crouch"));
	player_Widths["UpperCut"] = atoi(al_get_config_value(cfg, (fighter_name + "_Widths").c_str(), "UpperCut"));
	player_Widths["FlipStance"] = atoi(al_get_config_value(cfg, (fighter_name + "_Widths").c_str(), "FlipStance"));
	player_Widths["Twirl"] = atoi(al_get_config_value(cfg, (fighter_name + "_Widths").c_str(), "Twirl"));
	player_Widths["HitHard"] = atoi(al_get_config_value(cfg, (fighter_name + "_Widths").c_str(), "HitHard"));
	player_Widths["GetUp"] = atoi(al_get_config_value(cfg, (fighter_name + "_Widths").c_str(), "GetUp"));
	player_Widths["Win"] = atoi(al_get_config_value(cfg, (fighter_name + "_Widths").c_str(), "Win"));
	player_Widths["BlockHigh"] = atoi(al_get_config_value(cfg, (fighter_name + "_Widths").c_str(), "BlockHigh"));
	player_Widths["BlockLow"] = atoi(al_get_config_value(cfg, (fighter_name + "_Widths").c_str(), "BlockLow"));
	player_Widths["ReadyKick"] = atoi(al_get_config_value(cfg, (fighter_name + "_Widths").c_str(), "Kick_Ready"));
	player_Widths["EndKick"] = atoi(al_get_config_value(cfg, (fighter_name + "_Widths").c_str(), "Kick_End"));
	player_Widths["DownKick"] = atoi(al_get_config_value(cfg, (fighter_name + "_Widths").c_str(), "Kick_Down"));
	player_Widths["PunchHigh1"] = atoi(al_get_config_value(cfg, (fighter_name + "_Widths").c_str(), "Punch_High1"));
	player_Widths["PunchHigh2"] = atoi(al_get_config_value(cfg, (fighter_name + "_Widths").c_str(), "Punch_High2"));
	player_Widths["PunchLow1"] = atoi(al_get_config_value(cfg, (fighter_name + "_Widths").c_str(), "Punch_Low1"));
	player_Widths["PunchLow2"] = atoi(al_get_config_value(cfg, (fighter_name + "_Widths").c_str(), "Punch_Low2"));
	player_Widths["HitHead"] = atoi(al_get_config_value(cfg, (fighter_name + "_Widths").c_str(), "HitHead"));
	player_Widths["BackHand"] = atoi(al_get_config_value(cfg, (fighter_name + "_Widths").c_str(), "BackHand"));
	player_Widths["KickRoundhouse"] = atoi(al_get_config_value(cfg, (fighter_name + "_Widths").c_str(), "Kick_Roundhouse"));
	player_Widths["Trip"] = atoi(al_get_config_value(cfg, (fighter_name + "_Widths").c_str(), "Trip"));
	player_Widths["FootSweep"] = atoi(al_get_config_value(cfg, (fighter_name + "_Widths").c_str(), "FootSweep"));
	player_Widths["HitLow"] = atoi(al_get_config_value(cfg, (fighter_name + "_Widths").c_str(), "HitLow"));
	player_Widths["Jump_Punch"] = atoi(al_get_config_value(cfg, (fighter_name + "_Widths").c_str(), "Jump_Punch"));
	player_Widths["Jump_Straight_Kick"] = atoi(al_get_config_value(cfg, (fighter_name + "_Widths").c_str(), "Jump_Straight_Kick"));
	player_Widths["Daze"] = atoi(al_get_config_value(cfg, (fighter_name + "_Widths").c_str(), "Daze"));
	//... to be expanded
	al_destroy_config(cfg);
	return player_Widths;
}


std::map<std::string, int> engine2d::choose_correct_frame_count(std::string fighter_name) {
	ALLEGRO_CONFIG* cfg = al_load_config_file("../resources/config.cfg");
	std::map<std::string, int> player_frames;
	player_frames["Idle"] = atoi(al_get_config_value(cfg, (fighter_name + "_frame_count").c_str(), "Idle"));
	player_frames["Moving"] = atoi(al_get_config_value(cfg, (fighter_name + "_frame_count").c_str(), "Moving"));
	player_frames["Jumping"] = atoi(al_get_config_value(cfg, (fighter_name + "_frame_count").c_str(), "Jumping"));
	player_frames["Crouch"] = atoi(al_get_config_value(cfg, (fighter_name + "_frame_count").c_str(), "Crouch"));
	player_frames["UpperCut"] = atoi(al_get_config_value(cfg, (fighter_name + "_frame_count").c_str(), "UpperCut"));
	player_frames["FlipStance"] = atoi(al_get_config_value(cfg, (fighter_name + "_frame_count").c_str(), "FlipStance"));
	player_frames["Twirl"] = atoi(al_get_config_value(cfg, (fighter_name + "_frame_count").c_str(), "Twirl"));
	player_frames["HitHard"] = atoi(al_get_config_value(cfg, (fighter_name + "_frame_count").c_str(), "HitHard"));
	player_frames["GetUp"] = atoi(al_get_config_value(cfg, (fighter_name + "_frame_count").c_str(), "GetUp"));
	player_frames["Win"] = atoi(al_get_config_value(cfg, (fighter_name + "_frame_count").c_str(), "Win"));
	player_frames["BlockHigh"] = atoi(al_get_config_value(cfg, (fighter_name + "_frame_count").c_str(), "BlockHigh"));
	player_frames["BlockLow"] = atoi(al_get_config_value(cfg, (fighter_name + "_frame_count").c_str(), "BlockLow"));
	player_frames["ReadyKick"] = atoi(al_get_config_value(cfg, (fighter_name + "_frame_count").c_str(), "Kick_Ready"));
	player_frames["EndKick"] = atoi(al_get_config_value(cfg, (fighter_name + "_frame_count").c_str(), "Kick_End"));
	player_frames["DownKick"] = atoi(al_get_config_value(cfg, (fighter_name + "_frame_count").c_str(), "Kick_Down"));
	player_frames["PunchHigh1"] = atoi(al_get_config_value(cfg, (fighter_name + "_frame_count").c_str(), "Punch_High1"));
	player_frames["PunchHigh2"] = atoi(al_get_config_value(cfg, (fighter_name + "_frame_count").c_str(), "Punch_High2"));
	player_frames["PunchLow1"] = atoi(al_get_config_value(cfg, (fighter_name + "_frame_count").c_str(), "Punch_Low1"));
	player_frames["PunchLow2"] = atoi(al_get_config_value(cfg, (fighter_name + "_frame_count").c_str(), "Punch_Low2"));
	player_frames["HitHead"] = atoi(al_get_config_value(cfg, (fighter_name + "_frame_count").c_str(), "HitHead"));
	player_frames["BackHand"] = atoi(al_get_config_value(cfg, (fighter_name + "_frame_count").c_str(), "BackHand"));
	player_frames["KickRoundhouse"] = atoi(al_get_config_value(cfg, (fighter_name + "_frame_count").c_str(), "Kick_Roundhouse"));
	player_frames["Trip"] = atoi(al_get_config_value(cfg, (fighter_name + "_frame_count").c_str(), "Trip"));
	player_frames["FootSweep"] = atoi(al_get_config_value(cfg, (fighter_name + "_frame_count").c_str(), "FootSweep"));
	player_frames["HitLow"] = atoi(al_get_config_value(cfg, (fighter_name + "_frame_count").c_str(), "HitLow"));
	player_frames["Jump_Punch"] = atoi(al_get_config_value(cfg, (fighter_name + "_frame_count").c_str(), "Jump_Punch"));
	player_frames["Jump_Straight_Kick"] = atoi(al_get_config_value(cfg, (fighter_name + "_frame_count").c_str(), "Jump_Straight_Kick"));
	player_frames["Daze"] = atoi(al_get_config_value(cfg, (fighter_name + "_frame_count").c_str(), "Daze"));
	//... to be expanded
	al_destroy_config(cfg);
	return player_frames;
}

std::map<std::string, int> engine2d::choose_correct_delay_count(std::string fighter_name) {
	ALLEGRO_CONFIG* cfg = al_load_config_file("../resources/config.cfg");
	std::map<std::string, int> player_Delays;
	player_Delays["Idle"] = atoi(al_get_config_value(cfg, (fighter_name + "_Delays").c_str(), "Idle"));
	player_Delays["Moving"] = atoi(al_get_config_value(cfg, (fighter_name + "_Delays").c_str(), "Moving"));
	player_Delays["Jumping"] = atoi(al_get_config_value(cfg, (fighter_name + "_Delays").c_str(), "Jumping"));
	player_Delays["Crouch"] = atoi(al_get_config_value(cfg, (fighter_name + "_Delays").c_str(), "Crouch"));
	player_Delays["UpperCut"] = atoi(al_get_config_value(cfg, (fighter_name + "_Delays").c_str(), "UpperCut"));
	player_Delays["FlipStance"] = atoi(al_get_config_value(cfg, (fighter_name + "_Delays").c_str(), "FlipStance"));
	player_Delays["Twirl"] = atoi(al_get_config_value(cfg, (fighter_name + "_Delays").c_str(), "Twirl"));
	player_Delays["HitHard"] = atoi(al_get_config_value(cfg, (fighter_name + "_Delays").c_str(), "HitHard"));
	player_Delays["GetUp"] = atoi(al_get_config_value(cfg, (fighter_name + "_Delays").c_str(), "GetUp"));
	player_Delays["Win"] = atoi(al_get_config_value(cfg, (fighter_name + "_Delays").c_str(), "Win"));
	player_Delays["BlockHigh"] = atoi(al_get_config_value(cfg, (fighter_name + "_Delays").c_str(), "BlockHigh"));
	player_Delays["BlockLow"] = atoi(al_get_config_value(cfg, (fighter_name + "_Delays").c_str(), "BlockLow"));
	player_Delays["ReadyKick"] = atoi(al_get_config_value(cfg, (fighter_name + "_Delays").c_str(), "Kick_Ready"));
	player_Delays["EndKick"] = atoi(al_get_config_value(cfg, (fighter_name + "_Delays").c_str(), "Kick_End"));
	player_Delays["DownKick"] = atoi(al_get_config_value(cfg, (fighter_name + "_Delays").c_str(), "Kick_Down"));
	player_Delays["PunchHigh1"] = atoi(al_get_config_value(cfg, (fighter_name + "_Delays").c_str(), "Punch_High1"));
	player_Delays["PunchHigh2"] = atoi(al_get_config_value(cfg, (fighter_name + "_Delays").c_str(), "Punch_High2"));
	player_Delays["PunchLow1"] = atoi(al_get_config_value(cfg, (fighter_name + "_Delays").c_str(), "Punch_Low1"));
	player_Delays["PunchLow2"] = atoi(al_get_config_value(cfg, (fighter_name + "_Delays").c_str(), "Punch_Low2"));
	player_Delays["HitHead"] = atoi(al_get_config_value(cfg, (fighter_name + "_Delays").c_str(), "HitHead"));
	player_Delays["BackHand"] = atoi(al_get_config_value(cfg, (fighter_name + "_Delays").c_str(), "BackHand"));
	player_Delays["KickRoundhouse"] = atoi(al_get_config_value(cfg, (fighter_name + "_Delays").c_str(), "Kick_Roundhouse"));
	player_Delays["Trip"] = atoi(al_get_config_value(cfg, (fighter_name + "_Delays").c_str(), "Trip"));
	player_Delays["FootSweep"] = atoi(al_get_config_value(cfg, (fighter_name + "_Delays").c_str(), "FootSweep"));
	player_Delays["HitLow"] = atoi(al_get_config_value(cfg, (fighter_name + "_Delays").c_str(), "HitLow"));
	player_Delays["Jump_Punch"] = atoi(al_get_config_value(cfg, (fighter_name + "_Delays").c_str(), "Jump_Punch"));
	player_Delays["Jump_Straight_Kick"] = atoi(al_get_config_value(cfg, (fighter_name + "_Delays").c_str(), "Jump_Straight_Kick"));
	player_Delays["Daze"] = atoi(al_get_config_value(cfg, (fighter_name + "_Delays").c_str(), "Daze"));
	//... to be expanded
	al_destroy_config(cfg);
	return player_Delays;
}

void engine2d::draw_my_rect(rectangle p1_rec, ALLEGRO_FONT* font, ALLEGRO_COLOR color) {

	al_draw_rectangle(p1_rec.get_x_pos(), p1_rec.get_y_pos(), p1_rec.get_x_pos() + p1_rec.get_width(), p1_rec.get_y_pos() + p1_rec.get_height(), color, 4);
	al_draw_text(font, color, p1_rec.get_x_pos() + 20, p1_rec.get_y_pos() + 20, ALLEGRO_ALIGN_LEFT, p1_rec.get_name().c_str());
}

std::string engine2d::check_which_is_hovered(int x_pos, int y_pos) {
	if (x_pos == 65) {
		return "JohnnyCage";
	}
	else if (x_pos == 200 && y_pos == 111) {
		return "Kano";
	}
	else if (x_pos == 200 && y_pos == 296) {
		return "Raiden";
	}
	else if (x_pos == 335 && y_pos == 296) {
		return "LuKang";
	}
	else if (x_pos == 470 && y_pos == 296) {
		return "Scorpion";
	}
	else if (x_pos == 470 && y_pos == 111) {
		return "SubZero";
	}
	else if (x_pos == 605) {
		return "Sonya";
	}
	else {
		return "None";
	}


}

void engine2d::set_correct_background_attributes(engine2d::background& background)
{
	ALLEGRO_CONFIG* cfg = al_load_config_file("../resources/config.cfg");

	std::cout << "Background name: " << (background.get_name().c_str());

	background.bg_curr = atoi(al_get_config_value(cfg, (background.get_name()).c_str(), "bg_curr"));
	background.fg_curr = atoi(al_get_config_value(cfg, (background.get_name()).c_str(), "fg_curr"));

	background.bg_height = atoi(al_get_config_value(cfg, (background.get_name()).c_str(), "bg_height"));
	background.fg_height = atoi(al_get_config_value(cfg, (background.get_name()).c_str(), "fg_height"));

	background.bg_width = atoi(al_get_config_value(cfg, (background.get_name()).c_str(), "bg_width"));
	background.fg_width = atoi(al_get_config_value(cfg, (background.get_name()).c_str(), "fg_width"));

	background.bg_max_curr = atoi(al_get_config_value(cfg, (background.get_name()).c_str(), "bg_max_curr"));
	background.fg_max_curr = atoi(al_get_config_value(cfg, (background.get_name()).c_str(), "fg_max_curr"));

	background.bg_min_curr = atoi(al_get_config_value(cfg, (background.get_name()).c_str(), "bg_min_curr"));
	background.fg_min_curr = atoi(al_get_config_value(cfg, (background.get_name()).c_str(), "fg_min_curr"));

	background.bg_scale_forward = atoi(al_get_config_value(cfg, (background.get_name()).c_str(), "bg_scale_forward"));
	background.bg_scale_backwards = atoi(al_get_config_value(cfg, (background.get_name()).c_str(), "bg_scale_backwards"));

	background.fg_scale = atoi(al_get_config_value(cfg, (background.get_name()).c_str(), "fg_scale"));

	background.fg_y = atoi(al_get_config_value(cfg, (background.get_name()).c_str(), "fg_y"));
	background.bg_y = atoi(al_get_config_value(cfg, (background.get_name()).c_str(), "bg_y"));

	background.fg_x = atoi(al_get_config_value(cfg, (background.get_name()).c_str(), "fg_x"));
	background.bg_x = atoi(al_get_config_value(cfg, (background.get_name()).c_str(), "bg_x"));
	al_destroy_config(cfg);
}