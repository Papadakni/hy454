#pragma once
#include "Allegro_Initialiser.h"


namespace engine2d {
	typedef unsigned long timestamp_t;

	enum animatorstate_t {
		ANIMATOR_FINISHED = 0,
		ANIMATOR_RUNNING = 1,
		ANIMATOR_STOPPED = 2
	};
	/* original tryout*/
	class Sprite {
	public: //for now
		int frameWidth;
		int frameHeight;
		int x_pos;
		int y_pos;
		std::string name;
		ALLEGRO_BITMAP *sprite_sheet;
		bool debug = 0;

		Sprite() {}

		Sprite(int frameWidth, int frameHeight, int x_pos, int y_pos, std::string name) {
			this->frameWidth = frameWidth;
			this->frameHeight = frameHeight;
			this->x_pos = x_pos;
			this->y_pos = y_pos;
			this->name = name;
		}

		void set_sprite_sheet(const char * sheet_name) {
			al_destroy_bitmap(sprite_sheet);
			this->sprite_sheet = al_load_bitmap(sheet_name);
			al_convert_mask_to_alpha(this->sprite_sheet, al_map_rgb(255, 0, 255));
			al_convert_mask_to_alpha(this->sprite_sheet, al_map_rgb(0, 255, 0));
			
			if (!debug) al_convert_mask_to_alpha(this->sprite_sheet, al_map_rgb(255, 0, 0));
		}

		void sprite_debug_toggle() {
			debug = !debug;
		}

		void change_x_pos(int num) {
			this->x_pos += num;
		}

		void change_y_pos(int num) {
			this->y_pos += num;
		}

		void set_frame_width(int width) {
			this->frameWidth = width;
		}

		void set_frame_height(int height) {
			this->frameHeight = height;
		}

		int get_x_pos() {
			return this->x_pos;
		}

		int get_y_pos() {
			return this->y_pos;
		}

		void set_y_pos(int ypos) {
			this->y_pos = ypos;
		}

		void set_x_pos(int xpos) {
			this->x_pos = xpos;
		}

		ALLEGRO_BITMAP * get_sprite_sheet() {
			return this->sprite_sheet;

		}

		friend class TickAnimation;
		friend class TickAnimator;
	};

	class TickAnimation {
	public: //for now

		int start_position_x;
		int start_position_y;

		Sprite sprite;
		std::string name;

		TickAnimation() {}

		TickAnimation(int start_position_x, int start_position_y, int start_frame, int end_frame, std::string name, Sprite sprite) {

			this->start_position_x = start_position_x;
			this->start_position_y = start_position_y;

			this->name = name;
			//this->current_frame = start_frame;


			this->sprite = sprite;
		}

	public:
		void set_start_pos_x(int pos) {
			this->start_position_x = pos;
		}

		void set_start_pos_y(int pos) {
			this->start_position_y = pos;
		}

		int get_start_pos_x() {
			return this->start_position_x;
		}

		int get_start_pos_y() {
			return this->start_position_y;
		}

		Sprite get_Sprite() {
			return this->sprite;
		}

		friend class TickAnimator;
	};

	class Animator {
	public:
		typedef void(*FinishCallback)(Animator*, void*);
	protected:
		timestamp_t lastTime;

		animatorstate_t state;
		FinishCallback onFinish;
		void* finishClosure;
		void NotifyStopped(void);


		void Stop(void);
		bool HasFinished(void) const
		{
			return state != ANIMATOR_RUNNING;
		}
		virtual void TimeShift(timestamp_t offset);
		//virtual void Progress(timestamp_t currTime) = 0;
		void SetOnFinish(FinishCallback f, void* c = (void*)0)
		{
			onFinish = f, finishClosure = c;
		}


		Animator(void); virtual ~Animator() {};
	};



	class TickAnimator : Animator {
	public:
		TickAnimation animation;
		int current_frame;
		int frames_changed;
		int going_in_reverse;
		std::map<std::string, int> player_Widths;
		std::map<std::string, int> player_frame_count;
		std::map<std::string, int> frame_delays;


		TickAnimator() {}

		TickAnimator(TickAnimation animation) {
			this->animation = animation;
			int frames_changed = 0;
			int going_in_reverse = 0;
		}

		void Change_animation(TickAnimation animation) {
			this->animation = animation;
		}

		void set_player_Widths(std::map<std::string, int> player_Widths) {
			this->player_Widths = player_Widths;
		}
		void set_player_frame_count(std::map<std::string, int> player_frame_count) {
			this->player_frame_count = player_frame_count;
		}
		void set_player_frame_delays(std::map<std::string, int> player_frame_delays) {
			this->frame_delays = player_frame_delays;
		}


		std::map<std::string, int> get_player_Widths() {
			return this->player_Widths;
		}
		std::map<std::string, int> get_player_frame_count() {
			return this->player_frame_count;
		}
		std::map<std::string, int> get_player_frame_delays() {
			return this->frame_delays;
		}



		int  start_animation() {



		}


	};

	class rectangle {
		int x_pos;
		int y_pos;
		int width;
		int height;
		ALLEGRO_COLOR color;
		std::string name;

	public:
		rectangle(int x, int y, int width, int height, ALLEGRO_COLOR color, std::string name) {
			this->x_pos = x;
			this->y_pos = y;
			this->width = width;
			this->height = height;
			this->color = color;
			this->name = name;
		}
		void set_x_pos(int x) {
			this->x_pos = x;
		}
		void set_y_pos(int y) {
			this->y_pos = y;
		}

		void change_x_pos(int x) {
			this->x_pos += x;
		}
		void change_y_pos(int y) {
			this->y_pos += y;
		}
		int get_x_pos() {
			return this->x_pos;

		}
		int get_y_pos() {
			return this->y_pos;

		}
		int get_width() {
			return this->width;

		}
		int get_height() {
			return this->height;

		}
		std::string get_name() {
			return this->name;

		}

		ALLEGRO_COLOR get_color() {
			return this->color;
		}
		void print() {
			printf("Xpos %d, ypos %d \n", this->x_pos, this->y_pos);
		}



	};

	class background {
	public:
		int fg_width;
		int fg_height;
		int fg_curr;

		int bg_width;
		int bg_height;
		int bg_curr;

		int bg_max_curr;
		int fg_max_curr;

		int bg_min_curr;
		int fg_min_curr;

		int	bg_scale_forward;
		int bg_scale_backwards;

		int fg_scale;

		int fg_y;
		int bg_y;

		int fg_x;
		int bg_x;

		//TickAnimation Background_anim;

		bool shake_up = false;

		std::string name;

		ALLEGRO_BITMAP *Fg_Bitmap;
		ALLEGRO_BITMAP *Bg_Bitmap;

		background() {}

		background(std::string selected_arena) {

			this->Fg_Bitmap = al_load_bitmap(("../resources/arenas/" + selected_arena + "/" + selected_arena + "_fg.png").c_str());
			al_convert_mask_to_alpha(this->Fg_Bitmap, al_map_rgb(255, 0, 255));
			al_convert_mask_to_alpha(this->Fg_Bitmap, al_map_rgb(0, 255, 0));

			this->Bg_Bitmap = al_load_bitmap(("../resources/arenas/" + selected_arena + "/" + selected_arena + "_bg.png").c_str());
			al_convert_mask_to_alpha(this->Bg_Bitmap, al_map_rgb(255, 0, 255));
			al_convert_mask_to_alpha(this->Bg_Bitmap, al_map_rgb(0, 255, 0));

			this->name = selected_arena;
		}

		void change_arena(std::string name) {

			this->Fg_Bitmap = al_load_bitmap(("../resources/arenas/" + name + "/" + name + "_fg.png").c_str());
			al_convert_mask_to_alpha(this->Fg_Bitmap, al_map_rgb(255, 0, 255));
			al_convert_mask_to_alpha(this->Fg_Bitmap, al_map_rgb(0, 255, 0));


			this->Bg_Bitmap = al_load_bitmap(("../resources/arenas/" + name + "/" + name + "_bg.png").c_str());
			al_convert_mask_to_alpha(this->Bg_Bitmap, al_map_rgb(255, 0, 255));
			al_convert_mask_to_alpha(this->Bg_Bitmap, al_map_rgb(0, 255, 0));

			this->name = name;

		}

		void change_bg_curr(int amount) {

			this->bg_curr += amount;
		}

		void change_fg_curr(int amount) {

			this->fg_curr += amount;
		}


		std::string get_name() {
			return this->name;
		}

		ALLEGRO_BITMAP * get_fg_sheet() {
			return this->Fg_Bitmap;

		}
		ALLEGRO_BITMAP * get_bg_sheet() {
			return this->Bg_Bitmap;

		}
	};


	class countdown {
		int left_bit;
		int right_bit;

		ALLEGRO_BITMAP *Timer;

	public:
		countdown() {}

		countdown(int dummy) {
			this->Timer = al_load_bitmap("../resources/misc/numbers.png");
			al_convert_mask_to_alpha(this->Timer, al_map_rgb(255, 0, 255));
			al_convert_mask_to_alpha(this->Timer, al_map_rgb(0, 255, 0));

			left_bit = 9;
			right_bit = 9;

		}

		int get_left_bit() {
			return this->left_bit;
		}

		int get_right_bit() {
			return this->right_bit;
		}
		ALLEGRO_BITMAP * get_bitmap() {
			return this->Timer;
		}
		bool is_finished() {
			if (right_bit == 0 && left_bit == 0) {
				return true;
			}
			return false;
		}

		void tick_down() {
			if (!is_finished()) {
				if (this->right_bit == 0 && this->left_bit != 0) {
					this->left_bit--;

					this->right_bit = 10;
				}
				this->right_bit--;
			}
		}
		void reset_timer() {
			left_bit = 9;
			right_bit = 9;
		}


	};


	class win_counter {
		ALLEGRO_BITMAP *icon;
	public:
		win_counter() {
			this->icon = al_load_bitmap("../resources/misc/wins.png");
		}

		void draw(const char * name, int wins) {
			if (name == "Player_1") {
				if (wins == 1) {
					al_draw_bitmap(this->icon, 60, 120, 0);
				}
				else if (wins == 2) {
					al_draw_bitmap(this->icon, 60, 120, 0);
					al_draw_bitmap(this->icon, 80, 120, 0);
				}
			}
			else {
				if (wins == 1) {
					al_draw_bitmap(this->icon, 780, 120, 0);
				}
				else if (wins == 2) {
					al_draw_bitmap(this->icon, 780, 120, 0);
					al_draw_bitmap(this->icon, 760, 120, 0);
				}

			}


		}

	};


	class toasty {
		ALLEGRO_BITMAP *icon;
		int y_pos;
		int going_up;
		
	public:
		int disabled;
		int playing;
		toasty(int dud) {
			y_pos = 640;
			this->icon = al_load_bitmap("../resources/CharacterSheets/topsecret/savidis2.png");
			al_convert_mask_to_alpha(this->icon, al_map_rgb(255, 0, 255));
			 going_up =1;
			 disabled = 1;
		}
		void play() {
			al_play_sample(al_load_sample("../resources/sounds/musiccues/TOASTY!.wav"), 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
		}
		void draw_and_play() {
			if (!disabled) {
				if (y_pos == 460 && going_up == 1) {
					al_draw_scaled_rotated_bitmap(this->icon, 0, 0, 630, this->y_pos, 1.2, 1.2, 50, 0);
					al_play_sample(al_load_sample("../resources/sounds/musiccues/TOASTY!.wav"), 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
				}

				if (going_up == 1) {
					if (y_pos > 460) {
						al_draw_scaled_rotated_bitmap(this->icon, 0, 0, 630, this->y_pos, 1.2, 1.2, 50, 0);
						this->y_pos -= 5;
					}
					else {
						going_up = 0;
					}
				}
				else if (going_up == 0) {
					if (y_pos < 640) {
						al_draw_scaled_rotated_bitmap(this->icon, 0, 0, 630, this->y_pos, 1.2, 1.2, 50, 0);
						this->y_pos += 5;
					}
					else {
						going_up = 1;
						disabled = 1;
					}
				}

			}
		}
			

		

	};





	void healthbar(int x, int y, int w, int h, int percent, std::string name, ALLEGRO_FONT* font);

	void draw_my_rect(rectangle p1_rec, ALLEGRO_FONT* font, ALLEGRO_COLOR color);

	std::map<std::string, int> choose_correct_width(std::string fighter_name);


	std::map<std::string, int> choose_correct_frame_count(std::string fighter_name);

	std::map<std::string, int> choose_correct_delay_count(std::string fighter_name);

	std::string check_which_is_hovered(int x_pos, int y_pos);

	void set_correct_background_attributes(background& background);

	void fade_out();

}