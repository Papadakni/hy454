#pragma once
#include "Fighter.h"

namespace logic {

	void state_machine_key_press(mk::Fighter &player, input_checker &input, ALLEGRO_EVENT ev, bool * done, int * curFrame);
	void state_machine_key_release(mk::Fighter &player, input_checker &input, ALLEGRO_EVENT ev, bool * done, int * curFrame);
	void animation_manager(mk::Fighter &player_n, int * frameDelay, int * maxFrame, int * curFrame, engine2d::TickAnimation & player,int ground_height);
	void movement_manager(mk::Fighter& player_1, engine2d::TickAnimation& player1, engine2d::TickAnimation& player2, engine2d::background& arena,  int width, int frameWidth, int speed_x, int * done_up, int max_jump_height, int ground_height, int speed_y, mk::Fighter& player_2, int * CurFrame);
	void collision_check_players(mk::Fighter& player_1, mk::Fighter& player_2);	
	void collision_check_players_wider(mk::Fighter& player_1, mk::Fighter& player_2);
	void start_round_manager(int *current_round, engine2d::background arena, int *starting_now, ALLEGRO_FONT* font3, ALLEGRO_FONT* font4, int width, int time_passed, ALLEGRO_SAMPLE *bgm, ALLEGRO_SAMPLE_ID &id);
	bool win_checker(mk::Fighter &player_1, mk::Fighter &player_2);
	void end_round_manager(mk::Fighter &player_1, mk::Fighter &player_2, int *current_round, int *starting_now, ALLEGRO_FONT* font3, ALLEGRO_FONT* font4, int * time_passed, ALLEGRO_SAMPLE *bgm);
	void toast_checker(mk::Fighter &player_1, mk::Fighter &player_2, engine2d::toasty &toast);
}