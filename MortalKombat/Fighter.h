#pragma once
#include "2dgraphics.h"
#include "Controller.h"


namespace mk {
	enum player_state {
		IDLE,
		MOVING_FORWARD,
		MOVING_BACKWARDS,
		CROUCH,
		CROUCH_UP,
		JUMP_STRAIGHT,
		UPPERCUT,
		CHANGING_STANCE,
		TWIRL,
		TWIRL_BACK,
		HIT_HARD,
		GETUP,
		WIN,
		DEAD,
		BLOCK_HIGH,
		BLOCK_HIGH_RELEASE,
		BLOCK_LOW,
		KICK_HIGH,
		KICK_LOW,
		KICK_RESTORE,
		KICK_DOWN,
		KICK_ROUNDHOUSE,
		PUNCH_HIGH,
		PUNCH_LOW,
		HIT_HEAD,
		BACK_HAND,
		TRIP,
		FOOT_SWEEP,
		HIT_LOW,
		JUMP_PUNCH_STRAIGHT,
		JUMP_PUNCH,
		JUMP_STRAIGHT_KICK,
		DAZE
	};

	enum player_stance {
		RIGHT,
		LEFT,
	};


#define	FIGHTER_ACTION_DELAY_MSECS	150

	class Fighter final {
	private:
		//engine2d::Sprite*			sprite = nullptr;
		//std::string					nextAction;
		//logic::StateTransitions		stateTransitions;

		std::string	name;
		int         health;
		int         score;
		std::string character_selected;

		enum player_state state;
		player_stance stance;
		int wins;
		bool in_collision;
		bool in_wider_collision;
		int block_hits;
		int punch_side;
		
	public:
		engine2d::TickAnimator tickAnimator;
		engine2d::TickAnimation tickAnim;
		input_checker player_input;

		Fighter(std::string	name) {
			this->name = name;
			this->health = 100;
			this->state = IDLE;
			int wins = 0;
			in_collision = 0;
			in_wider_collision = 0;
			block_hits = 0;
			punch_side = 0;
		}

		void change_health(int amount) {
			this->health += amount;
		}

		void reset_player_health() {
			this->health = 100;
		}

		void select_fighter(std::string name) {
			this->character_selected = name;
		}
		void change_punch_side() {
			if (this->punch_side == 0) {
				this->punch_side = 1;
			}
			else {
				punch_side = 0;
			}
		}
		int get_punch_side() {
			return this->punch_side;
		}

		void add_block_hit() {
			this->block_hits++;
		}
		void reset_block_hits() {
			this->block_hits = 0;
		}
		int get_block_hits() {
			return this->block_hits;
		}

		void set_collision(bool col) {
			this->in_collision = col;
		}
		void set_wider_collision(bool col) {
			this->in_wider_collision = col;
		}

		void set_player_state(enum player_state state) {
			this->state = state;
		}

		void set_input_checker(input_checker  player_input) {
			this->player_input = player_input;
		}


		void set_player_animation(engine2d::TickAnimation anim) {
			this->tickAnim = anim;
		}

		void set_player_animator(engine2d::TickAnimator  anim) {
			this->tickAnimator = anim;
		}

		engine2d::TickAnimation get_player_anim() {
			return this->tickAnim;
		}

		engine2d::TickAnimator  get_player_animator() {
			return this->tickAnimator;
		}

		input_checker get_player_input_checker() {
			return this->player_input;
		}

		std::string  get_character_name() {
			return this->character_selected;
		}

		bool get_collision() {
			return this->in_collision;
		}
		bool get_wider_collision() {
			return this->in_wider_collision;
		}

		int get_health() {
			return this->health;
		}
		int get_wins() {
			return this->wins;
		}

		enum player_state get_state() {
			return this->state;
		}

		void set_stance(enum player_stance st) {
			this->stance = st;
		}

		enum player_stance get_stance() {
			return this->stance;
		}

		void add_win() {
			this->wins++;
		}
		void reset_win() {
			this->wins=0;
		}

		bool is_victorious() {
			if (this->wins == 2) {
				//this->wins = 0;
				return true;

			}
			else {
				return false;
			}

		}

		/*control_map get_controls() {
			return this->controls;
		}*/
		// TODO: 
	};
}