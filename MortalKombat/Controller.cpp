#include "Controller.h"


void get_control_config(std::string playername, input_checker& input)
{
	ALLEGRO_CONFIG* cfg = al_load_config_file("../resources/config.cfg");
	std::map<std::string, int> Player_Controls;
	Player_Controls["Up"] = atoi(al_get_config_value(cfg, (playername + " Controls").c_str(), "Up"));
	Player_Controls["Down"] = atoi(al_get_config_value(cfg, (playername + " Controls").c_str(), "Down"));
	Player_Controls["Left"] = atoi(al_get_config_value(cfg, (playername + " Controls").c_str(), "Left"));
	Player_Controls["Right"] = atoi(al_get_config_value(cfg, (playername + " Controls").c_str(), "Right"));
	Player_Controls["High_Punch"] = atoi(al_get_config_value(cfg, (playername + " Controls").c_str(), "High_Punch"));
	Player_Controls["Exit"] = atoi(al_get_config_value(cfg, (playername + " Controls").c_str(), "Exit"));
	Player_Controls["Start"] = atoi(al_get_config_value(cfg, (playername + " Controls").c_str(), "Start"));
	Player_Controls["Block"] = atoi(al_get_config_value(cfg, (playername + " Controls").c_str(), "Block"));
	Player_Controls["High_Kick"] = atoi(al_get_config_value(cfg, (playername + " Controls").c_str(), "High_Kick"));
	Player_Controls["Low_Kick"] = atoi(al_get_config_value(cfg, (playername + " Controls").c_str(), "Low_Kick"));
	Player_Controls["Low_Punch"] = atoi(al_get_config_value(cfg, (playername + " Controls").c_str(), "Low_Punch"));
	input.set_controls(Player_Controls);
	al_destroy_config(cfg);
}