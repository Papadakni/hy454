#include "2dgraphics.h"
#include "Fighter.h"
#include "Controls.h"
#include "Logic.h"
#include "Defines.h"
#include "Menu.h"

int width;
int height;

float alpha_from_0_to_1 = 0;

int maxFrame,maxFrame2, curFrame, frameCount, frameDelay, frameWidth, frameHeight, frameCount2, frameDelay2, curFrame2;

int speed_x, speed_y, max_jump_height, ground_height;
int my_custom_ticker = 0;

int done_up = 0; //flags
int done_up2 = 0;
int starting_now = 1;

int current_round = 1;

int tournament = 1;

bool done = false; // in order to quit with escape key

int main()
{
	using namespace mk;

	init_all();

	//READ CONFIG FILE FOR GLOBALS
	ALLEGRO_CONFIG* cfg = al_load_config_file("../resources/config.cfg");

	//Resolution
	width = atoi(al_get_config_value(cfg, "Screen Resolution", "Width"));
	height = atoi(al_get_config_value(cfg, "Screen Resolution", "Height"));

	//Character Move Stats
	speed_x = atoi(al_get_config_value(cfg, "Character Move Stats", "speed_x"));
	speed_y = atoi(al_get_config_value(cfg, "Character Move Stats", "speed_y"));
	max_jump_height = atoi(al_get_config_value(cfg, "Character Move Stats", "max_jump_height"));
	ground_height = atoi(al_get_config_value(cfg, "Character Move Stats", "ground_height"));
	maxFrame = atoi(al_get_config_value(cfg, "Initialiser_frames", "maxFrame"));
	frameCount = atoi(al_get_config_value(cfg, "Initialiser_frames", "frameCount"));
	frameCount2 = atoi(al_get_config_value(cfg, "Initialiser_frames", "frameCount2"));
	frameDelay = atoi(al_get_config_value(cfg, "Initialiser_frames", "frameDelay"));
	frameWidth = atoi(al_get_config_value(cfg, "Initialiser_frames", "frameWidth"));
	frameHeight = atoi(al_get_config_value(cfg, "Initialiser_frames", "frameHeight"));
	frameDelay2 = atoi(al_get_config_value(cfg, "Initialiser_frames", "frameDelay2"));
	curFrame2 = atoi(al_get_config_value(cfg, "Initialiser_frames", "curFrame2"));

	//Win counter icon
	engine2d::win_counter win_count = engine2d::win_counter();

	input_checker p1_input = input_checker();
	get_control_config("Player_1", p1_input);

	input_checker p2_input = input_checker();
	get_control_config("Player_2", p2_input);

	mk::Fighter player_1("Player_1");
	mk::Fighter player_2("Player_2");

	engine2d::rectangle p1_rec(width / 2, height / 2, width - 800 + 122, height - 600 + 182, al_map_rgb(0, 0, 255), "P1");
	engine2d::rectangle p2_rec(width / 2, height / 2, 122, 182, al_map_rgb(255, 0, 0), "P2");
	//Many stuff should be init from config file 

	engine2d::countdown time1(1);

	ALLEGRO_TIMER* timer = al_create_timer(1.0 / 60.0);

	engine2d::toasty toast(1);

	ALLEGRO_EVENT_QUEUE* event_queue = al_create_event_queue();
	
	//Display Fullscreen
	if (atoi(al_get_config_value(cfg, "Screen Resolution", "Fullscreen"))) 
		al_set_new_display_flags(ALLEGRO_FULLSCREEN);
	

	ALLEGRO_DISPLAY* disp = al_create_display(SCREEN_WIDTH, SCREEN_HEIGHT);

	//Various Fonts
	ALLEGRO_FONT* font = al_create_builtin_font();
	ALLEGRO_FONT* font2 = al_load_ttf_font("../resources/fonts/mk2.ttf", 30, 0);
	ALLEGRO_FONT* font3 = al_load_ttf_font("../resources/fonts/mk1.ttf", 35, 0);
	ALLEGRO_FONT* font4 = al_load_ttf_font("../resources/fonts/mk1.ttf", 33, 0);
	ALLEGRO_FONT* font5 = al_load_ttf_font("../resources/fonts/mk1.ttf", 54, 0);
	ALLEGRO_FONT* font6 = al_load_ttf_font("../resources/fonts/mk1.ttf", 56, 0);
	ALLEGRO_FONT* font7 = al_load_ttf_font("../resources/fonts/mk1.ttf", 15, 0);
	ALLEGRO_FONT* font8 = al_load_ttf_font("../resources/fonts/mk1.ttf", 25, 0);
	ALLEGRO_FONT* fonts[] = { font , font2, font3, font4, font5, font6, font7, font8 };
	al_reserve_samples(31);

	//Sounds
	ALLEGRO_SAMPLE *ui_sample = al_load_sample("../resources/sounds/ui/mk1-00163.wav");
	ALLEGRO_SAMPLE *bong = al_load_sample("../resources/sounds/musiccues/bong.wav");
	ALLEGRO_SAMPLE *bgm = al_load_sample("../resources/sounds/ui/mk1-00163.wav");
	ALLEGRO_SAMPLE *bgm2 = al_load_sample("../resources/sounds/ui/mk1-00163.wav");
	ALLEGRO_SAMPLE_ID bgm_id;

	//Timers
	al_start_timer(timer);
	al_register_event_source(event_queue, al_get_timer_event_source(timer));
	al_register_event_source(event_queue, al_get_keyboard_event_source());

	//Arena 
	srand(time(NULL));
	int  v1 = (rand() % 6) + 1;
	std::cout << v1;
	std::string random = "arena";
	random += std::to_string(v1);
	std::string selected_arena = (al_get_config_value(cfg, "Arenas", random.c_str()));
	engine2d::background arena(selected_arena);
	engine2d::set_correct_background_attributes(arena);

	//Players
	player_1.select_fighter("Kano");
	player_2.select_fighter("SubZero");

	engine2d::Sprite p1(frameWidth, frameHeight, 100, 350, "player1");
	
	
	p1.set_sprite_sheet(("../resources/CharacterSheets/" + player_1.get_character_name() + "/" + player_1.get_character_name() + "_idle.png").c_str());

	engine2d::TickAnimation player1(0, 10, 0, 11, "player1", p1);
	engine2d::TickAnimator anim_player1(player1);
	player_1.set_stance(RIGHT);
	player_1.set_input_checker(p1_input);
	player_1.tickAnim = player1;
	player_1.tickAnim.sprite.frameWidth = 54;
	


	engine2d::Sprite p2(frameWidth, frameHeight, 100, 350, "player2");
	p2.set_sprite_sheet(("../resources/CharacterSheets/" + player_2.get_character_name() + "/" + player_2.get_character_name() + "_idle.png").c_str());
	
	engine2d::TickAnimation player2(0, 10, 0, 11, "player2", p2);
	engine2d::TickAnimator anim_player2(player2);
	player_2.set_stance(LEFT);
	player2.sprite.set_x_pos(650);
	player_2.set_input_checker(p2_input);
	player_2.tickAnim = player2;
	player_2.tickAnim.sprite.frameWidth = 51;
	

	game_state gamestate = INTRO;
	Menu::playIntro(gamestate);

	while (!done) {
		if (gamestate == EXIT_GAME) {
			done = 1;
		} else if (gamestate == MENU) {
			get_control_config("Player_1", p1_input);
			get_control_config("Player_2", p2_input);
			player_1.set_input_checker(p1_input);
			player_2.set_input_checker(p2_input);
			
			player_1.tickAnim.sprite.debug = atoi(al_get_config_value(cfg, "Debug", "Debug_Sprite_Mode"));
			player_2.tickAnim.sprite.debug = atoi(al_get_config_value(cfg, "Debug", "Debug_Sprite_Mode"));
			
			
			al_flush_event_queue(event_queue); // for spamming space at intro screen
			Menu::mainScreen(gamestate, event_queue);
		}
		else if (gamestate == OPTIONS) {
			Menu::optionsScreen(gamestate, event_queue, disp);
		}
		else if (gamestate == CONTROLS) {
			Menu::controlsScreen(gamestate, event_queue);
		}
		else if (gamestate == CHARACTER_SELECT) {
			
			Menu::characterSelectScreen(gamestate, event_queue, player_1, player_2,disp);
		}
		else if (gamestate == STAGE_SELECT) {
			Menu::StageSelectScreen(gamestate, event_queue, arena, disp);
		}
		else if (gamestate == FIGHT) {



			ALLEGRO_EVENT ev;
			al_wait_for_event(event_queue, &ev);


			

			//CONTROLS
			if (ev.type == ALLEGRO_EVENT_KEY_DOWN && starting_now != 1) {

				logic::state_machine_key_press(player_1, player_1.player_input, ev, &done, &curFrame);
				player_1.player_input.clean_keys();

				logic::state_machine_key_press(player_2, player_2.player_input, ev, &done, &curFrame2);
				player_2.player_input.clean_keys();

			}
			else if (ev.type == ALLEGRO_EVENT_KEY_UP && starting_now != 1) {
				logic::state_machine_key_release(player_1, player_1.player_input, ev, &done, &curFrame);
				player_1.player_input.clean_keys();
				logic::state_machine_key_release(player_2, player_2.player_input, ev, &done, &curFrame2);
				player_2.player_input.clean_keys();

			}

			//ANIMATION
			else if (ev.type == ALLEGRO_EVENT_TIMER) {

				if (++frameCount >= frameDelay) {
					logic::animation_manager(player_1, &frameDelay, &maxFrame, &curFrame, player_1.tickAnim, ground_height);

					frameCount = 0;
				}
				if (++frameCount2 >= frameDelay2) {
					logic::animation_manager(player_2, &frameDelay2, &maxFrame2, &curFrame2, player_2.tickAnim, ground_height);


					frameCount2 = 0;
				}
			}


			//Collision
			logic::collision_check_players(player_1, player_2);
			logic::collision_check_players_wider(player_1, player_2);

			//MOVEMENT

			logic::movement_manager(player_1, player_1.tickAnim, player_2.tickAnim, arena, width, frameWidth, speed_x, &done_up, max_jump_height, ground_height, speed_y, player_2, &curFrame);
			logic::movement_manager(player_2, player_2.tickAnim, player_1.tickAnim, arena, width, frameWidth, speed_x, &done_up2, max_jump_height, ground_height, speed_y, player_1, &curFrame2);
			//DRAWING DISPLAY HERE

			//BACK GROUND
			al_draw_tinted_scaled_rotated_bitmap_region(arena.get_bg_sheet(), arena.bg_curr, 0, arena.bg_width, arena.bg_height, al_map_rgb(255, 255, 255), 0, 0, arena.bg_x, arena.bg_y, 3, 1.5, 0, 0);
			al_draw_tinted_scaled_rotated_bitmap_region(arena.get_fg_sheet(), arena.fg_curr, 0, arena.fg_width, arena.fg_height, al_map_rgb(255, 255, 255), 0, 0, arena.fg_x, arena.fg_y, 2, 2, 0, 0);

			//DRAW PLAYERS
			al_draw_tinted_scaled_rotated_bitmap_region(player_1.tickAnim.sprite.sprite_sheet, player_1.tickAnim.start_position_x + ((curFrame)* player_1.tickAnim.sprite.frameWidth), 0, player_1.tickAnim.sprite.frameWidth, player_1.tickAnim.sprite.frameHeight, al_map_rgb(255, 255, 255), 0, 0, player_1.tickAnim.sprite.get_x_pos(), player_1.tickAnim.sprite.get_y_pos(), 2.0, 2.0, 0, (player_1.get_stance() == RIGHT) ? 0 : ALLEGRO_FLIP_HORIZONTAL);
			al_draw_tinted_scaled_rotated_bitmap_region(player_2.tickAnim.sprite.sprite_sheet, player_2.tickAnim.start_position_x + ((curFrame2)* player_2.tickAnim.sprite.frameWidth), 0, player_2.tickAnim.sprite.frameWidth, player_2.tickAnim.sprite.frameHeight, al_map_rgb(192, 192, 192), 0, 0, player_2.tickAnim.sprite.get_x_pos(), player_2.tickAnim.sprite.get_y_pos(), 2.0, 2.0, 0, (player_2.get_stance() == RIGHT) ? 0 : ALLEGRO_FLIP_HORIZONTAL);

			//DRAW TOAST
			//toast checker 
			logic::toast_checker(player_1, player_2, toast);
			logic::toast_checker(player_2, player_1, toast);
			toast.draw_and_play();
			//DRAW READY FIGHT
			if (starting_now == 1) {
				time1.reset_timer();

				logic::start_round_manager(&current_round, arena, &starting_now, font3, font4, width, my_custom_ticker, bgm, bgm_id);

				//fade
				while (alpha_from_0_to_1 > 0) {
					player_1.tickAnim.sprite.set_x_pos(150);
					player_2.tickAnim.sprite.set_x_pos(650);
					player_1.tickAnim.sprite.set_y_pos(ground_height);
					player_2.tickAnim.sprite.set_y_pos(ground_height);
					//BACK GROUND
					al_draw_tinted_scaled_rotated_bitmap_region(arena.get_bg_sheet(), arena.bg_curr, 0, arena.bg_width, arena.bg_height, al_map_rgb(255, 255, 255), 0, 0, arena.bg_x, arena.bg_y, 3, 1.5, 0, 0);
					al_draw_tinted_scaled_rotated_bitmap_region(arena.get_fg_sheet(), arena.fg_curr, 0, arena.fg_width, arena.fg_height, al_map_rgb(255, 255, 255), 0, 0, arena.fg_x, arena.fg_y, 2, 2, 0, 0);

					//times
					al_draw_tinted_scaled_rotated_bitmap_region(time1.get_bitmap(), time1.get_left_bit() * 18, 0, 18, 19, al_map_rgb(255, 255, 255), 0, 0, 375, 65, 1.5, 1.5, 0, 0);
					al_draw_tinted_scaled_rotated_bitmap_region(time1.get_bitmap(), time1.get_right_bit() * 18, 0, 18, 19, al_map_rgb(255, 255, 255), 0, 0, 404, 65, 1.5, 1.5, 0, 0);

					//players
					al_draw_tinted_scaled_rotated_bitmap_region(player_1.tickAnim.sprite.sprite_sheet, player_1.tickAnim.start_position_x + ((curFrame)* player_1.tickAnim.sprite.frameWidth), 0, player_1.tickAnim.sprite.frameWidth, player_1.tickAnim.sprite.frameHeight, al_map_rgb(255, 255, 255), 0, 0, player_1.tickAnim.sprite.get_x_pos(), player_1.tickAnim.sprite.get_y_pos(), 1.7, 1.7, 0, (player_1.get_stance() == RIGHT) ? 0 : ALLEGRO_FLIP_HORIZONTAL);
					al_draw_tinted_scaled_rotated_bitmap_region(player_2.tickAnim.sprite.sprite_sheet, player_2.tickAnim.start_position_x + ((curFrame2)* player_2.tickAnim.sprite.frameWidth), 0, player_2.tickAnim.sprite.frameWidth, player_2.tickAnim.sprite.frameHeight, al_map_rgb(192, 192, 192), 0, 0, player_2.tickAnim.sprite.get_x_pos(), player_2.tickAnim.sprite.get_y_pos(), 1.7, 1.7, 0, (player_2.get_stance() == RIGHT) ? 0 : ALLEGRO_FLIP_HORIZONTAL);

					//HEALTHBARS (should have fighter health instead of 100 - 70
					engine2d::healthbar(60, 80, 250, 28, player_1.get_health(), player_1.get_character_name(), font2);
					engine2d::healthbar(540, 80, 250, 28, player_2.get_health(), player_2.get_character_name(), font2);

					win_count.draw("Player_1", player_1.get_wins());
					win_count.draw("Player_2", player_2.get_wins());

					al_draw_filled_rectangle(0, 0, 800, 600, al_map_rgba(0, 0, 0, alpha_from_0_to_1));
					alpha_from_0_to_1 -= 0.4;
					al_flip_display();
				}
				//toast.draw_and_play();
			}
			//HEALTHBARS (should have fighter health instead of 100 - 70
			engine2d::healthbar(60, 80, 250, 28, player_1.get_health(), player_1.get_character_name(), font2);
			engine2d::healthbar(540, 80, 250, 28, player_2.get_health(), player_2.get_character_name(), font2);

			

			//END ROUND
			if (logic::win_checker(player_1, player_2) == true || time1.is_finished()) {
				if (my_custom_ticker > 500) {

					al_stop_sample(&bgm_id);
					my_custom_ticker = 0;

				}

				logic::end_round_manager(player_1, player_2, &current_round, &starting_now, font5, font6, &my_custom_ticker, bgm2);
				//fade
				if (starting_now == 1) {
					while (alpha_from_0_to_1 < 250) {
						player_1.tickAnim.sprite.set_x_pos(150);
						player_2.tickAnim.sprite.set_x_pos(650);
						player_1.tickAnim.sprite.set_y_pos(ground_height);
						player_2.tickAnim.sprite.set_y_pos(ground_height);
						//BACK GROUND
						al_draw_tinted_scaled_rotated_bitmap_region(arena.get_bg_sheet(), arena.bg_curr, 0, arena.bg_width, arena.bg_height, al_map_rgb(255, 255, 255), 0, 0, arena.bg_x, arena.bg_y, 3, 1.5, 0, 0);
						al_draw_tinted_scaled_rotated_bitmap_region(arena.get_fg_sheet(), arena.fg_curr, 0, arena.fg_width, arena.fg_height, al_map_rgb(255, 255, 255), 0, 0, arena.fg_x, arena.fg_y, 2, 2, 0, 0);


						al_draw_tinted_scaled_rotated_bitmap_region(time1.get_bitmap(), time1.get_left_bit() * 18, 0, 18, 19, al_map_rgb(255, 255, 255), 0, 0, 375, 65, 1.5, 1.5, 0, 0);
						al_draw_tinted_scaled_rotated_bitmap_region(time1.get_bitmap(), time1.get_right_bit() * 18, 0, 18, 19, al_map_rgb(255, 255, 255), 0, 0, 404, 65, 1.5, 1.5, 0, 0);


						al_draw_tinted_scaled_rotated_bitmap_region(player_1.tickAnim.sprite.sprite_sheet, player_1.tickAnim.start_position_x + ((curFrame)* player_1.tickAnim.sprite.frameWidth), 0, player_1.tickAnim.sprite.frameWidth, player_1.tickAnim.sprite.frameHeight, al_map_rgb(255, 255, 255), 0, 0, player_1.tickAnim.sprite.get_x_pos(), player_1.tickAnim.sprite.get_y_pos(), 1.7, 1.7, 0, (player_1.get_stance() == RIGHT) ? 0 : ALLEGRO_FLIP_HORIZONTAL);
						al_draw_tinted_scaled_rotated_bitmap_region(player_2.tickAnim.sprite.sprite_sheet, player_2.tickAnim.start_position_x + ((curFrame2)* player_2.tickAnim.sprite.frameWidth), 0, player_2.tickAnim.sprite.frameWidth, player_2.tickAnim.sprite.frameHeight, al_map_rgb(192, 192, 192), 0, 0, player_2.tickAnim.sprite.get_x_pos(), player_2.tickAnim.sprite.get_y_pos(), 1.7, 1.7, 0, (player_2.get_stance() == RIGHT) ? 0 : ALLEGRO_FLIP_HORIZONTAL);

						//HEALTHBARS (should have fighter health instead of 100 - 70
						engine2d::healthbar(60, 80, 250, 28, player_1.get_health(), player_1.get_character_name(), font2);
						engine2d::healthbar(540, 80, 250, 28, player_2.get_health(), player_2.get_character_name(), font2);

						win_count.draw("Player_1", player_1.get_wins());
						win_count.draw("Player_2", player_2.get_wins());

						al_draw_filled_rectangle(0, 0, 800, 600, al_map_rgba(0, 0, 0, alpha_from_0_to_1));
						alpha_from_0_to_1 += 0.3;
						al_flip_display();
					}
					player_1.tickAnim.sprite.set_x_pos(150);
					player_2.tickAnim.sprite.set_x_pos(650);
					//game ends go back to character select
					if (player_1.get_wins() >= 3 || player_2.get_wins() >= 3) {
						gamestate = CHARACTER_SELECT;
						current_round = 1;
						player_1.reset_win();
						player_2.reset_win();
						al_flush_event_queue(event_queue);
						al_clear_to_color(al_map_rgb(0, 0, 0));
						al_play_sample(al_load_sample("../resources/sounds/musiccues/bong.wav"), 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
					}
				}

			}
			//Draw timer

			al_draw_tinted_scaled_rotated_bitmap_region(time1.get_bitmap(), time1.get_left_bit() * 16, 0, 16, 19, al_map_rgb(255, 255, 255), 0, 0, 375, 65, 1.5, 1.5, 0, 0);
			al_draw_tinted_scaled_rotated_bitmap_region(time1.get_bitmap(), time1.get_right_bit() * 16, 0, 16, 19, al_map_rgb(255, 255, 255), 0, 0, 404, 65, 1.5, 1.5, 0, 0);

			my_custom_ticker++;

			if (my_custom_ticker % 60 == 0 && starting_now != 1 && !time1.is_finished()) {
				time1.tick_down();
			}

			win_count.draw("Player_1", player_1.get_wins());
			win_count.draw("Player_2", player_2.get_wins());


			al_flip_display();
			al_clear_to_color(al_map_rgb(0, 0, 0));
		}
		
	}

	al_destroy_font(font);
	al_destroy_font(font2);
	al_destroy_font(font3);
	al_destroy_font(font4);


	return 0;
}