#pragma once

constexpr auto SCREEN_WIDTH = 800;
constexpr auto SCREEN_HEIGHT = 600;

enum game_state {
	INTRO,
	MENU,
	OPTIONS,
	CONTROLS,
	CHARACTER_SELECT,
	FIGHT,
	EXIT_GAME,
	STAGE_SELECT
};