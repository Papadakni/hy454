#pragma once
#include "Allegro_Initialiser.h"



using key_combination = std::list<std::string>;

extern bool test_key(const std::string& keyCode);

inline bool test_keys(const key_combination& keys) {
	for (auto& key : keys)
		if (!test_key(key))
			return false;
	return true;
}
class InputController final {
public:
	using Logical = std::set<std::string>;
private:
	using Actions = std::list<std::pair<key_combination, std::string>>;
	using All = std::list<InputController*>;
	Actions actions;
	Logical logical;
	static All all;		
	void SetLogical(const std::string& id) { logical.insert(id); }
public:
	void AddAction(const key_combination& keys, const std::string& logical)
	{
		actions.push_back(std::make_pair(keys, logical));
	}
	void Handle(void) {
		logical.clear();
		for (auto& i : actions)
			if (test_keys(i.first))
				SetLogical(i.second);
	}
	const Logical& GetLogical(void) const
	{
		return logical;
	}
	static void HandleAll(void) {
		for (auto* handler : all)
			handler->Handle();
	}

	InputController(void) { all.push_back(this); }
	~InputController() { all.remove(this); }
};

class input_checker {
	std::map<std::string, int> key_map;
	std::string  key1;

public:
	input_checker() {}


	void set_controls(std::map<std::string, int> keys) {
		this->key_map = keys;
	}
	void check_key_pressed(ALLEGRO_EVENT key1) {

		for (std::map<std::string, int>::iterator it = this->key_map.begin(); it != this->key_map.end(); ++it) {
			if (it->second == key1.keyboard.keycode) {
				this->key1 = it->first;
			}

		}
	}


	std::string get_key() {
		return this->key1;
	}
	void clean_keys() {
		this->key1 = "EMPTY";


	}
	bool is_empty_key_1() {
		if (this->key1 == "EMPTY") {
			return true;
		}
		return false;
	}


	void print_keys() {
		std::cout << "key1 " << key1 << "\n";
	}

};

void get_control_config(std::string playername, input_checker &input);