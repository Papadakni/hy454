#include "Allegro_Initialiser.h"

//Function so that you don't have to write check for every  init
void must_init(bool test, const char *description)
{
	if (test) return;

	std::cerr << "couldn't initialize " << description << std::endl;
	exit(1);
}

void init_all() {
	must_init(al_init(), "allegro");
    must_init(al_install_keyboard(), "keyboard");
	must_init(al_init_font_addon(), "fonts");
	must_init(al_init_ttf_addon(), "true type fonts");
	must_init(al_init_primitives_addon(), "primitives");
    must_init(al_init_image_addon(), "images");
	must_init(al_install_audio(), "audio");
	must_init(al_init_acodec_addon(), "codecs");
	must_init(al_reserve_samples(1), "samples");
}