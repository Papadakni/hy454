#include "Menu.h"

static int portrait_xpos[] = { 66, 202, 338, 474, 610,
						  66, 202, 338, 474, 610 };

static int portrait_ypos[] = { 105, 105, 105, 105, 105,
					  300, 300, 300, 300, 300 };


static int portrait2_xpos[] = { 26, 305, 593, 
						  26, 305, 593,  };

static int portrait2_ypos[] = { 242, 242, 242,
					  410, 410, 410,  };

const char * character_names[10] = { "JohnnyCage", "Kano", "Hidden", "SubZero", "Sonya",
											"None", "Raiden", "LiuKang", "Scorpion", "None" };

const char * stage_names[6] = { "Palace_Gates",
		"Courtyard",
		"The_Pit",
		"Throne_Room",
		"Warrior_Shrine",
		"Goro_Lair" };

const char * option_names[6] = { "Debug", "Music", "Controls" };

const char * control_names[] = {
	"Block_1", "Block_2",
	"High_Kick_1", "High_Kick_2",
	"Low_Kick_1", "Low_Kick_2",
	"High_Punch_1", "High_Punch_2",
	"Low_Punch_1", "Low_Punch_2"
};

//DO NOT EXPAND THIS
const char * key_names[] = {
	"EMPTY",
	"A",
	"B",
	"C",
	"D",
	"E",
	"F",
	"G",
	"H",
	"I",
	"J",
	"K",
	"L",
	"M",
	"N",
	"O",
	"P",
	"Q",
	"R",
	"S",
	"T",
	"U",
	"V",
	"W",
	"X",
	"Y",
	"Z",

	"0",
	"1",
	"2",
	"3",
	"4",
	"5",
	"6",
	"7",
	"8",
	"9",

	"PAD_0",
	"PAD_1",
	"PAD_2",
	"PAD_3",
	"PAD_4",
	"PAD_5",
	"PAD_6",
	"PAD_7",
	"PAD_8",
	"PAD_9",

	"F1",
	"F2",
	"F3",
	"F4",
	"F5",
	"F6",
	"F7",
	"F8",
	"F9",
	"F10",
	"F11",
	"F12",

	"ESCAPE",
	"TILDE",
	"MINUS",
	"EQUALS",
	"BACKSPACE",
	"TAB",
	"OPENBRACE",
	"CLOSEBRACE",
	"ENTER",
	"SEMICOLON",
	"QUOTE",
	"BACKSLASH",
	"BACKSLASH2", /* DirectInput calls this DIK_OEM_102: "< > | on UK/Germany keyboards" */
	"COMMA",
	"FULLSTOP",
	"SLASH",
	"SPACE",

	"INSERT",
	"DELETE",
	"HOME",
	"END",
	"PGUP",
	"PGDN",
	"LEFT",
	"RIGHT",
	"UP",
	"DOWN",

	"PAD_SLASH",
	"PAD_ASTERISK",
	"PAD_MINUS",
	"PAD_PLUS",
	"PAD_DELETE",
	"PAD_ENTER",

	"PRINTSCREEN",
	"PAUSE",

	"ABNT_C1",
	"YEN",
	"KANA",
	"CONVERT",
	"NOCONVERT",
	"AT",
	"CIRCUMFLEX",
	"COLON2",
	"KANJI",

	"PAD_EQUALS",	/* MacOS X */
	"BACKQUOTE",	/* MacOS X */
	"SEMICOLON2",	/* MacOS X -- TODO: ask lillo what this should be */
	"COMMAND",	/* MacOS X */

	"BACK",        /* Android back key */
	"VOLUME_UP",
	"VOLUME_DOWN",

	/* Android game keys */
	"SEARCH",
	"DPAD_CENTER",
	"BUTTON_X",
	"BUTTON_Y",
	"DPAD_UP",
	"DPAD_DOWN",
	"DPAD_LEFT",
	"DPAD_RIGHT",
	"SELECT",
	"START",
	"BUTTON_L1",
	"BUTTON_R1",
	"BUTTON_L2",
	"BUTTON_R2",
	"BUTTON_A",
	"BUTTON_B",
	"THUMBL",
	"THUMBR",

	"UNKNOWN",

	"MODIFIERS",

	"LSHIFT",
	"RSHIFT",
	"LCTRL",
	"RCTRL",
	"ALT",
	"ALTGR",
	"LWIN",
	"RWIN",
	"MENU",
	"SCROLLLOCK",
	"NUMLOCK",
	"CAPSLOCK",

	"INVALID"
};

void Menu::playIntro(game_state& gamestate)
{
	ALLEGRO_FONT* font_mk1_15 = al_load_ttf_font("../resources/fonts/mk1.ttf", 15, 0);
	ALLEGRO_FONT* font_mk1_25 = al_load_ttf_font("../resources/fonts/mk1.ttf", 25, 0);
	ALLEGRO_FONT* font_mk1_33 = al_load_ttf_font("../resources/fonts/mk1.ttf", 33, 0);
	ALLEGRO_FONT* font_mk1_35 = al_load_ttf_font("../resources/fonts/mk1.ttf", 35, 0);
	

	for (float alpha = 0.0; alpha < 250; alpha += 0.2) {

		al_draw_text(font_mk1_35, al_map_rgb(255, 255, 2), SCREEN_WIDTH / 2, SCREEN_HEIGHT - 550, ALLEGRO_ALIGN_CENTER, "MORTAL_KOMBAT  ");
		al_draw_text(font_mk1_15, al_map_rgb(255, 255, 2), SCREEN_WIDTH / 2 + 124, SCREEN_HEIGHT - 530, ALLEGRO_ALIGN_CENTER, "TM");
		al_draw_text(font_mk1_33, al_map_rgb(255, 255, 2), SCREEN_WIDTH / 2, SCREEN_HEIGHT - 500, ALLEGRO_ALIGN_CENTER, "(c) 1993 ACCLAIM ENTERTAINMENT. INC");

		al_draw_text(font_mk1_33, al_map_rgb(255, 255, 2), SCREEN_WIDTH / 2, SCREEN_HEIGHT - 440, ALLEGRO_ALIGN_CENTER, "UNIVERSITY OF CRETE  ");
		al_draw_text(font_mk1_25, al_map_rgb(255, 255, 2), SCREEN_WIDTH / 2, SCREEN_HEIGHT - 400, ALLEGRO_ALIGN_CENTER, "CS - 454. DEVELOPMENT OF INTELLIGENT INTERFACES AND GAMES  ");
		al_draw_text(font_mk1_25, al_map_rgb(255, 255, 2), SCREEN_WIDTH / 2, SCREEN_HEIGHT - 360, ALLEGRO_ALIGN_CENTER, "TERM PROJECT, FALL SEMESTER 2018  ");

		al_draw_text(font_mk1_33, al_map_rgb(255, 255, 2), SCREEN_WIDTH / 2, SCREEN_HEIGHT - 270, ALLEGRO_ALIGN_CENTER, "PROGRAMMED BY   ");
		al_draw_text(font_mk1_25, al_map_rgb(255, 255, 2), SCREEN_WIDTH / 2, SCREEN_HEIGHT - 220, ALLEGRO_ALIGN_CENTER, "CSD 3469 NIKOLAOS     PAPADAKIS ");
		al_draw_text(font_mk1_25, al_map_rgb(255, 255, 2), SCREEN_WIDTH / 2, SCREEN_HEIGHT - 180, ALLEGRO_ALIGN_CENTER, "CSD 3431 STAVROS      MAVRIKOS  ");
		al_draw_text(font_mk1_25, al_map_rgb(255, 255, 2), SCREEN_WIDTH / 2 + 5, SCREEN_HEIGHT - 140, ALLEGRO_ALIGN_CENTER, "CSD 3267 PANAGIOTIS ZERVAKIS   ");
		al_draw_filled_rectangle(0, 0, 800, 600, al_map_rgba(0, 0, 0, alpha));
		al_flip_display();
		al_clear_to_color(al_map_rgb(0, 0, 0));

		if (alpha == 0.0) delay(2000);
	}

	gamestate = MENU;

	al_destroy_font(font_mk1_15);
	al_destroy_font(font_mk1_25);
	al_destroy_font(font_mk1_33);
	al_destroy_font(font_mk1_35);
}

void Menu::mainScreen(game_state & gamestate, ALLEGRO_EVENT_QUEUE * event_queue)
{
	ALLEGRO_EVENT ev;
	ALLEGRO_BITMAP* menu_clear = al_load_bitmap("../resources/menus/Menu_clear.png");
	ALLEGRO_BITMAP* menu_tournament = al_load_bitmap("../resources/menus/Menu_tournament.png");
	ALLEGRO_BITMAP* menu_options = al_load_bitmap("../resources/menus/Menu_options.png");
	ALLEGRO_BITMAP* menu_selection = menu_tournament;

	int menu_width = al_get_bitmap_width(menu_clear);
	int menu_height = al_get_bitmap_height(menu_clear);

	enum button_selection {
		TOURNAMENT_BUTTON,
		OPTIONS_BUTTON
	} selection = TOURNAMENT_BUTTON;

	//Fade in
	for (float alpha = 250; alpha > 0; alpha -= 0.5) {
		al_draw_scaled_bitmap(menu_clear, 0, 0, menu_width, menu_height, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 0);
		al_draw_filled_rectangle(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, al_map_rgba(0, 0, 0, alpha));
		al_flip_display();
	}

	//Starting sound

	al_play_sample(al_load_sample("../resources/sounds/musiccues/start.wav"), 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);

	while (gamestate == MENU) {

		al_draw_scaled_bitmap(menu_selection, 0, 0, menu_width, menu_height, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 0);
		al_flip_display();
		al_clear_to_color(al_map_rgb(0, 0, 0));

		al_wait_for_event(event_queue, &ev);

		if (ev.type == ALLEGRO_EVENT_KEY_DOWN) {
			switch (ev.keyboard.keycode) {

			case ALLEGRO_KEY_ESCAPE:
				gamestate = EXIT_GAME;
				break;

			case ALLEGRO_KEY_SPACE:

				//Play [B]ong
				al_play_sample(al_load_sample("../resources/sounds/musiccues/bong.wav"), 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);

				if (selection == TOURNAMENT_BUTTON) {
					gamestate = CHARACTER_SELECT;
					menu_selection = menu_tournament;
					al_destroy_bitmap(menu_options);
				}
				else {
					gamestate = OPTIONS;
					menu_selection = menu_options;
					al_destroy_bitmap(menu_tournament);
				}

				//Flicker fade out
				for (float alpha = 0.0; alpha < 250; alpha += 0.5) {

					//al_draw_scaled_bitmap((int)alpha % 5 == 0 ? menu_selection : menu_clear, 0, 0, 514, 463, 0, 0, width, height, 0);
					if ((int)alpha % 5 == 0) {
						al_draw_scaled_bitmap(menu_selection, 0, 0, menu_width, menu_height, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 0);
					}
					else {
						al_draw_scaled_bitmap(menu_clear, 0, 0, menu_width, menu_height, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 0);
					}

					al_draw_filled_rectangle(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, al_map_rgba(0, 0, 0, alpha));
					al_flip_display();
					al_clear_to_color(al_map_rgb(0, 0, 0));
				}
				al_flush_event_queue(event_queue);
				break;

			case ALLEGRO_KEY_LEFT:
				/*al_play_sample(al_load_sample("../resources/sounds/ui/mk1-00163.wav"), 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
				selection = selection == TOURNAMENT_BUTTON ? OPTIONS_BUTTON : TOURNAMENT_BUTTON;
				menu_selection = selection == TOURNAMENT_BUTTON ? menu_tournament : menu_options;
				break;*/

			case ALLEGRO_KEY_RIGHT:
				al_play_sample(al_load_sample("../resources/sounds/ui/mk1-00163.wav"), 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
				selection = selection == TOURNAMENT_BUTTON ? OPTIONS_BUTTON : TOURNAMENT_BUTTON;
				menu_selection = selection == TOURNAMENT_BUTTON ? menu_tournament : menu_options;
				break;
			}
		}
	}

	al_destroy_bitmap(menu_clear);
}

void Menu::optionChange(enum selector_move action, option_selector &hover) {
	
	int option = hover;

	switch (action) {
	case UP:
		option > 0 ? option -= 1 : option -= 0;
		break;
	case DOWN:
		option < 2 ? option += 1 : option += 0;
		break;
	default:
		perror("Wtf is this button?");
		break;
	}

	hover = static_cast<option_selector>(option);
}

void Menu::optionsScreen(game_state & gamestate, ALLEGRO_EVENT_QUEUE * event_queue, ALLEGRO_DISPLAY * disp)
{
	ALLEGRO_CONFIG* cfg = al_load_config_file("../resources/config.cfg");
	ALLEGRO_BITMAP* menu = al_load_bitmap("../resources/menus/Options_Blank.png");
	ALLEGRO_BITMAP* menu_debug = al_load_bitmap("../resources/menus/Options_Debug.png");
	ALLEGRO_BITMAP* menu_music = al_load_bitmap("../resources/menus/Options_Music.png");
	ALLEGRO_BITMAP* menu_controls = al_load_bitmap("../resources/menus/Options_Controls.png");
	ALLEGRO_FONT* font = al_load_ttf_font("../resources/fonts/mk1.ttf", 40, 0);
	ALLEGRO_EVENT ev;

	ALLEGRO_BITMAP* menus[] = { menu_debug, menu_music, menu_controls };
	int debug;
	int music;

	int menu_width = al_get_bitmap_width(menu);
	int menu_height = al_get_bitmap_height(menu);

	option_selector hover = Debug;

	//Fade in
	for (float alpha = 250; alpha > 0.0; alpha -= 0.5) {
		al_draw_scaled_bitmap(menu, 0, 0, menu_width, menu_height, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 0);
		al_draw_filled_rectangle(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, al_map_rgba(0, 0, 0, alpha));
		al_flip_display();
	}

	debug = atoi(al_get_config_value(cfg, "Debug", "Debug_Sprite_Mode"));
	music = atoi(al_get_config_value(cfg, "Music", "Music"));

	al_draw_scaled_bitmap(menu_debug, 0, 0, menu_width, menu_height, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 0);
	al_draw_text(font, al_map_rgb(0, 0, 0), SCREEN_WIDTH / 2 + 4, 260 + 4, ALLEGRO_ALIGN_CENTER, debug ? "ON" : "OFF");
	al_draw_text(font, al_map_rgb(255, 255, 255), SCREEN_WIDTH / 2, 260, ALLEGRO_ALIGN_CENTER, debug ? "ON" : "OFF");
	al_draw_text(font, al_map_rgb(0, 0, 0), SCREEN_WIDTH / 2 + 4, 400 + 4, ALLEGRO_ALIGN_CENTER, music ? "ON" : "OFF");
	al_draw_text(font, al_map_rgb(255, 255, 255), SCREEN_WIDTH / 2, 400, ALLEGRO_ALIGN_CENTER, music ? "ON" : "OFF");
	al_flip_display();

	while (gamestate == OPTIONS) {
		al_wait_for_event(event_queue, &ev);

		al_draw_scaled_bitmap(menus[hover], 0, 0, menu_width, menu_height, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 0);
		al_draw_text(font, al_map_rgb(0, 0, 0), SCREEN_WIDTH / 2 + 4, 260 + 4, ALLEGRO_ALIGN_CENTER, debug ? "ON" : "OFF");
		al_draw_text(font, al_map_rgb(255, 255, 255), SCREEN_WIDTH / 2, 260, ALLEGRO_ALIGN_CENTER, debug ? "ON" : "OFF");
		al_draw_text(font, al_map_rgb(0, 0, 0), SCREEN_WIDTH / 2 + 4, 400 + 4, ALLEGRO_ALIGN_CENTER, music ? "ON" : "OFF");
		al_draw_text(font, al_map_rgb(255, 255, 255), SCREEN_WIDTH / 2, 400, ALLEGRO_ALIGN_CENTER, music ? "ON" : "OFF");
		al_flip_display();

		if (ev.type == ALLEGRO_EVENT_KEY_DOWN) {
			switch (ev.keyboard.keycode) {
			case ALLEGRO_KEY_BACKSPACE:
				//Fade out
				for (float alpha = 0.0; alpha < 250; alpha += 0.5) {
					al_draw_scaled_bitmap(menu, 0, 0, menu_width, menu_height, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 0);
					al_draw_filled_rectangle(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, al_map_rgba(0, 0, 0, alpha));
					al_flip_display();
				}
				
				gamestate = MENU;
				break;

			case ALLEGRO_KEY_DOWN:
				al_play_sample(al_load_sample("../resources/sounds/ui/mk1-00163.wav"), 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
				optionChange(DOWN, hover);
				al_flip_display();
				break;

			case ALLEGRO_KEY_UP:
				al_play_sample(al_load_sample("../resources/sounds/ui/mk1-00163.wav"), 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
				optionChange(UP, hover);
				break;

			case ALLEGRO_KEY_SPACE:
				switch (hover) {
				case Menu::Debug:
					debug = debug ? 0 : 1;
					break;
				case Menu::Music:
					music = music ? 0 : 1;
					break;
				case Menu::Controls:
					gamestate = CONTROLS;
					break;
				default:
					break;
				}
				break;

			case ALLEGRO_KEY_ESCAPE:

				gamestate = EXIT_GAME;
				break;

			}
		}
	}

	al_set_config_value(cfg, "Debug", "Debug_Sprite_Mode", std::to_string(debug).c_str());
	al_set_config_value(cfg, "Music", "Music", std::to_string(music).c_str());
	al_save_config_file("../resources/config.cfg", cfg);
	al_destroy_config(cfg);
}

void Menu::controlChange(selector_move action, control_selector & hover)
{
	int control = hover;
	al_play_sample(al_load_sample("../resources/sounds/ui/mk1-00163.wav"), 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);

	switch (action) {
	case UP:
		control > 1 ? control -= 2 : control -= 0;
		break;
	case DOWN:
		control < 8 ? control += 2 : control += 0;
		break;
	case LEFT:
		(control % 2 == 1) ? control -= 1 : control -= 0;
		break;
	case RIGHT:
		(control % 2 == 0) ? control += 1 : control += 0;
		break;
	default:
		perror("Wtf is this button?");
		break;
	}

	hover = static_cast<control_selector>(control);
}

void Menu::checkKeyValidityAndConfirm(int*(&keys)[10], int nkey, int pos) {
	
	if (nkey > ALLEGRO_KEY_PAD_ENTER) return;

	int ok = 1;

	for (int i = 0; i < 10; i++) {
		if (*keys[i] == nkey) ok = 0;
	}

	if (ok) 
		*keys[pos] = nkey;
	else
		std::cerr << "\nKey already assigned!\n";
}

void Menu::changeKey(enum control_selector key, int*(&keys)[10], ALLEGRO_EVENT_QUEUE * event_queue)
{
	ALLEGRO_BITMAP* menu = al_load_bitmap("../resources/menus/Options_Controls_Config.png");
	ALLEGRO_FONT* font = al_load_ttf_font("../resources/fonts/mk1.ttf", 40, 0);
	ALLEGRO_EVENT ev;

	int menu_width = al_get_bitmap_width(menu);
	int menu_height = al_get_bitmap_height(menu);

	int end = 0;

	al_draw_scaled_bitmap(menu, 0, 0, menu_width, menu_height, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 0);
	al_draw_text(font, al_map_rgb(0, 0, 0), SCREEN_WIDTH / 7 + 4, 275 + 4, ALLEGRO_ALIGN_CENTER, key_names[*keys[0]]);
	al_draw_text(font, al_map_rgb(0, 0, 0), SCREEN_WIDTH / 7 + 4, 340 + 4, ALLEGRO_ALIGN_CENTER, key_names[*keys[2]]);
	al_draw_text(font, al_map_rgb(0, 0, 0), SCREEN_WIDTH / 7 + 4, 410 + 4, ALLEGRO_ALIGN_CENTER, key_names[*keys[4]]);
	al_draw_text(font, al_map_rgb(0, 0, 0), SCREEN_WIDTH / 7 + 4, 475 + 4, ALLEGRO_ALIGN_CENTER, key_names[*keys[6]]);
	al_draw_text(font, al_map_rgb(0, 0, 0), SCREEN_WIDTH / 7 + 4, 540 + 4, ALLEGRO_ALIGN_CENTER, key_names[*keys[8]]);
	al_draw_text(font, al_map_rgb(0, 0, 0), SCREEN_WIDTH - SCREEN_WIDTH / 7 + 4, 275 + 4, ALLEGRO_ALIGN_CENTER, key_names[*keys[1]]);
	al_draw_text(font, al_map_rgb(0, 0, 0), SCREEN_WIDTH - SCREEN_WIDTH / 7 + 4, 340 + 4, ALLEGRO_ALIGN_CENTER, key_names[*keys[3]]);
	al_draw_text(font, al_map_rgb(0, 0, 0), SCREEN_WIDTH - SCREEN_WIDTH / 7 + 4, 410 + 4, ALLEGRO_ALIGN_CENTER, key_names[*keys[5]]);
	al_draw_text(font, al_map_rgb(0, 0, 0), SCREEN_WIDTH - SCREEN_WIDTH / 7 + 4, 475 + 4, ALLEGRO_ALIGN_CENTER, key_names[*keys[7]]);
	al_draw_text(font, al_map_rgb(0, 0, 0), SCREEN_WIDTH - SCREEN_WIDTH / 7 + 4, 540 + 4, ALLEGRO_ALIGN_CENTER, key_names[*keys[9]]);

	al_draw_text(font, (key == Block_1) ? al_map_rgb(250, 0, 0) : al_map_rgb(255, 255, 255), SCREEN_WIDTH / 7, 275, ALLEGRO_ALIGN_CENTER, key_names[*keys[0]]);
	al_draw_text(font, (key == High_Kick_1) ? al_map_rgb(250, 0, 0) : al_map_rgb(255, 255, 255), SCREEN_WIDTH / 7, 340, ALLEGRO_ALIGN_CENTER, key_names[*keys[2]]);
	al_draw_text(font, (key == Low_Kick_1) ? al_map_rgb(250, 0, 0) : al_map_rgb(255, 255, 255), SCREEN_WIDTH / 7, 410, ALLEGRO_ALIGN_CENTER, key_names[*keys[4]]);
	al_draw_text(font, (key == High_Punch_1) ? al_map_rgb(250, 0, 0) : al_map_rgb(255, 255, 255), SCREEN_WIDTH / 7, 475, ALLEGRO_ALIGN_CENTER, key_names[*keys[6]]);
	al_draw_text(font, (key == Low_Punch_1) ? al_map_rgb(250, 0, 0) : al_map_rgb(255, 255, 255), SCREEN_WIDTH / 7, 540, ALLEGRO_ALIGN_CENTER, key_names[*keys[8]]);
	al_draw_text(font, (key == Block_2) ? al_map_rgb(250, 0, 0) : al_map_rgb(255, 255, 255), SCREEN_WIDTH - SCREEN_WIDTH / 7, 275, ALLEGRO_ALIGN_CENTER, key_names[*keys[1]]);
	al_draw_text(font, (key == High_Kick_2) ? al_map_rgb(250, 0, 0) : al_map_rgb(255, 255, 255), SCREEN_WIDTH - SCREEN_WIDTH / 7, 340, ALLEGRO_ALIGN_CENTER, key_names[*keys[3]]);
	al_draw_text(font, (key == Low_Kick_2) ? al_map_rgb(250, 0, 0) : al_map_rgb(255, 255, 255), SCREEN_WIDTH - SCREEN_WIDTH / 7, 410, ALLEGRO_ALIGN_CENTER, key_names[*keys[5]]);
	al_draw_text(font, (key == High_Punch_2) ? al_map_rgb(250, 0, 0) : al_map_rgb(255, 255, 255), SCREEN_WIDTH - SCREEN_WIDTH / 7, 475, ALLEGRO_ALIGN_CENTER, key_names[*keys[7]]);
	al_draw_text(font, (key == Low_Punch_2) ? al_map_rgb(250, 0, 0) : al_map_rgb(255, 255, 255), SCREEN_WIDTH - SCREEN_WIDTH / 7, 540, ALLEGRO_ALIGN_CENTER, key_names[*keys[9]]);
	al_flip_display();

	while (!end) {
		al_wait_for_event(event_queue, &ev);
		if (ev.type == ALLEGRO_EVENT_KEY_DOWN) {
			switch (ev.keyboard.keycode) {

			case ALLEGRO_KEY_BACKSPACE:
			case ALLEGRO_KEY_ESCAPE:
			case ALLEGRO_KEY_SPACE:
			case ALLEGRO_KEY_UP:
			case ALLEGRO_KEY_DOWN:
			case ALLEGRO_KEY_LEFT:
			case ALLEGRO_KEY_RIGHT:
			case ALLEGRO_KEY_W:
			case ALLEGRO_KEY_A:
			case ALLEGRO_KEY_S:
			case ALLEGRO_KEY_D:
				//Keys that cannot be reassigned
				break;
			default:
				checkKeyValidityAndConfirm(keys, ev.keyboard.keycode, key);
				break;
			}
			end = 1;
		}
	}

	al_destroy_bitmap(menu);
	al_destroy_font(font);
}

void Menu::controlsScreen(game_state & gamestate, ALLEGRO_EVENT_QUEUE * event_queue)
{
	ALLEGRO_CONFIG* cfg = al_load_config_file("../resources/config.cfg");
	ALLEGRO_BITMAP* menu = al_load_bitmap("../resources/menus/Options_Controls_Config.png");
	ALLEGRO_FONT* font = al_load_ttf_font("../resources/fonts/mk1.ttf", 40, 0);
	ALLEGRO_EVENT ev;

	int menu_width = al_get_bitmap_width(menu);
	int menu_height = al_get_bitmap_height(menu);

	control_selector hover = Block_1;

	int block1 = atoi(al_get_config_value(cfg, "Player_1 Controls", "Block"));
	int block2 = atoi(al_get_config_value(cfg, "Player_2 Controls", "Block"));
	int hKick1 = atoi(al_get_config_value(cfg, "Player_1 Controls", "High_Kick"));
	int hKick2 = atoi(al_get_config_value(cfg, "Player_2 Controls", "High_Kick"));
	int lKick1 = atoi(al_get_config_value(cfg, "Player_1 Controls", "Low_Kick"));
	int lKick2 = atoi(al_get_config_value(cfg, "Player_2 Controls", "Low_Kick"));
	int hPunch1 = atoi(al_get_config_value(cfg, "Player_1 Controls", "High_Punch"));
	int hPunch2 = atoi(al_get_config_value(cfg, "Player_2 Controls", "High_Punch"));
	int lPunch1 = atoi(al_get_config_value(cfg, "Player_1 Controls", "Low_Punch"));
	int lPunch2 = atoi(al_get_config_value(cfg, "Player_2 Controls", "Low_Punch"));

	int *keys[10] = {
		&block1, &block2,
		&hKick1, &hKick2,
		&lKick1, &lKick2,
		&hPunch1, &hPunch2,
		&lPunch1, &lPunch2
	};

	//Fade in
	for (float alpha = 250; alpha > 0.0; alpha -= 0.5) {
		al_draw_scaled_bitmap(menu, 0, 0, menu_width, menu_height, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 0);
		al_draw_filled_rectangle(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, al_map_rgba(0, 0, 0, alpha));
		al_flip_display();
	}

	al_draw_scaled_bitmap(menu, 0, 0, menu_width, menu_height, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 0);
	al_draw_text(font, al_map_rgb(0, 0, 0), SCREEN_WIDTH / 7 + 4, 275 + 4, ALLEGRO_ALIGN_CENTER, key_names[block1]);
	al_draw_text(font, al_map_rgb(0, 0, 0), SCREEN_WIDTH / 7 + 4, 340 + 4, ALLEGRO_ALIGN_CENTER, key_names[hKick1]);
	al_draw_text(font, al_map_rgb(0, 0, 0), SCREEN_WIDTH / 7 + 4, 410 + 4, ALLEGRO_ALIGN_CENTER, key_names[lKick1]);
	al_draw_text(font, al_map_rgb(0, 0, 0), SCREEN_WIDTH / 7 + 4, 475 + 4, ALLEGRO_ALIGN_CENTER, key_names[hPunch1]);
	al_draw_text(font, al_map_rgb(0, 0, 0), SCREEN_WIDTH / 7 + 4, 540 + 4, ALLEGRO_ALIGN_CENTER, key_names[lPunch1]);
	al_draw_text(font, al_map_rgb(0, 0, 0), SCREEN_WIDTH - SCREEN_WIDTH / 7 + 4, 275 + 4, ALLEGRO_ALIGN_CENTER, key_names[block2]);
	al_draw_text(font, al_map_rgb(0, 0, 0), SCREEN_WIDTH - SCREEN_WIDTH / 7 + 4, 340 + 4, ALLEGRO_ALIGN_CENTER, key_names[hKick2]);
	al_draw_text(font, al_map_rgb(0, 0, 0), SCREEN_WIDTH - SCREEN_WIDTH / 7 + 4, 410 + 4, ALLEGRO_ALIGN_CENTER, key_names[lKick2]);
	al_draw_text(font, al_map_rgb(0, 0, 0), SCREEN_WIDTH - SCREEN_WIDTH / 7 + 4, 475 + 4, ALLEGRO_ALIGN_CENTER, key_names[hPunch2]);
	al_draw_text(font, al_map_rgb(0, 0, 0), SCREEN_WIDTH - SCREEN_WIDTH / 7 + 4, 540 + 4, ALLEGRO_ALIGN_CENTER, key_names[lPunch2]);

	al_draw_text(font, (hover == Block_1) ? al_map_rgb(0, 255, 0) : al_map_rgb(255, 255, 255), SCREEN_WIDTH / 7, 275, ALLEGRO_ALIGN_CENTER, key_names[block1]);
	al_draw_text(font, (hover == High_Kick_1) ? al_map_rgb(0, 255, 0) : al_map_rgb(255, 255, 255), SCREEN_WIDTH / 7, 340, ALLEGRO_ALIGN_CENTER, key_names[hKick1]);
	al_draw_text(font, (hover == Low_Kick_1) ? al_map_rgb(0, 255, 0) : al_map_rgb(255, 255, 255), SCREEN_WIDTH / 7, 410, ALLEGRO_ALIGN_CENTER, key_names[lKick1]);
	al_draw_text(font, (hover == High_Punch_1) ? al_map_rgb(0, 255, 0) : al_map_rgb(255, 255, 255), SCREEN_WIDTH / 7, 475, ALLEGRO_ALIGN_CENTER, key_names[hPunch1]);
	al_draw_text(font, (hover == Low_Punch_1) ? al_map_rgb(0, 255, 0) : al_map_rgb(255, 255, 255), SCREEN_WIDTH / 7, 540, ALLEGRO_ALIGN_CENTER, key_names[lPunch1]);
	al_draw_text(font, (hover == Block_2) ? al_map_rgb(0, 255, 0) : al_map_rgb(255, 255, 255), SCREEN_WIDTH - SCREEN_WIDTH / 7, 275, ALLEGRO_ALIGN_CENTER, key_names[block2]);
	al_draw_text(font, (hover == High_Kick_2) ? al_map_rgb(0, 255, 0) : al_map_rgb(255, 255, 255), SCREEN_WIDTH - SCREEN_WIDTH / 7, 340, ALLEGRO_ALIGN_CENTER, key_names[hKick2]);
	al_draw_text(font, (hover == Low_Kick_2) ? al_map_rgb(0, 255, 0) : al_map_rgb(255, 255, 255), SCREEN_WIDTH - SCREEN_WIDTH / 7, 410, ALLEGRO_ALIGN_CENTER, key_names[lKick2]);
	al_draw_text(font, (hover == High_Punch_2) ? al_map_rgb(0, 255, 0) : al_map_rgb(255, 255, 255), SCREEN_WIDTH - SCREEN_WIDTH / 7, 475, ALLEGRO_ALIGN_CENTER, key_names[hPunch2]);
	al_draw_text(font, (hover == Low_Punch_2) ? al_map_rgb(0, 255, 0) : al_map_rgb(255, 255, 255), SCREEN_WIDTH - SCREEN_WIDTH / 7, 540, ALLEGRO_ALIGN_CENTER, key_names[lPunch2]);
	al_flip_display();

	while (gamestate == CONTROLS) {
		al_wait_for_event(event_queue, &ev);

		al_draw_scaled_bitmap(menu, 0, 0, menu_width, menu_height, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 0);
		al_draw_text(font, al_map_rgb(0, 0, 0), SCREEN_WIDTH / 7 + 4, 275 + 4, ALLEGRO_ALIGN_CENTER, key_names[block1]);
		al_draw_text(font, al_map_rgb(0, 0, 0), SCREEN_WIDTH / 7 + 4, 340 + 4, ALLEGRO_ALIGN_CENTER, key_names[hKick1]);
		al_draw_text(font, al_map_rgb(0, 0, 0), SCREEN_WIDTH / 7 + 4, 410 + 4, ALLEGRO_ALIGN_CENTER, key_names[lKick1]);
		al_draw_text(font, al_map_rgb(0, 0, 0), SCREEN_WIDTH / 7 + 4, 475 + 4, ALLEGRO_ALIGN_CENTER, key_names[hPunch1]);
		al_draw_text(font, al_map_rgb(0, 0, 0), SCREEN_WIDTH / 7 + 4, 540 + 4, ALLEGRO_ALIGN_CENTER, key_names[lPunch1]);
		al_draw_text(font, al_map_rgb(0, 0, 0), SCREEN_WIDTH - SCREEN_WIDTH / 7 + 4, 275 + 4, ALLEGRO_ALIGN_CENTER, key_names[block2]);
		al_draw_text(font, al_map_rgb(0, 0, 0), SCREEN_WIDTH - SCREEN_WIDTH / 7 + 4, 340 + 4, ALLEGRO_ALIGN_CENTER, key_names[hKick2]);
		al_draw_text(font, al_map_rgb(0, 0, 0), SCREEN_WIDTH - SCREEN_WIDTH / 7 + 4, 410 + 4, ALLEGRO_ALIGN_CENTER, key_names[lKick2]);
		al_draw_text(font, al_map_rgb(0, 0, 0), SCREEN_WIDTH - SCREEN_WIDTH / 7 + 4, 475 + 4, ALLEGRO_ALIGN_CENTER, key_names[hPunch2]);
		al_draw_text(font, al_map_rgb(0, 0, 0), SCREEN_WIDTH - SCREEN_WIDTH / 7 + 4, 540 + 4, ALLEGRO_ALIGN_CENTER, key_names[lPunch2]);

		al_draw_text(font, (hover == Block_1) ? al_map_rgb(0, 255, 0) : al_map_rgb(255, 255, 255), SCREEN_WIDTH / 7, 275, ALLEGRO_ALIGN_CENTER, key_names[block1]);
		al_draw_text(font, (hover == High_Kick_1) ? al_map_rgb(0, 255, 0) : al_map_rgb(255, 255, 255), SCREEN_WIDTH / 7, 340, ALLEGRO_ALIGN_CENTER, key_names[hKick1]);
		al_draw_text(font, (hover == Low_Kick_1) ? al_map_rgb(0, 255, 0) : al_map_rgb(255, 255, 255), SCREEN_WIDTH / 7, 410, ALLEGRO_ALIGN_CENTER, key_names[lKick1]);
		al_draw_text(font, (hover == High_Punch_1) ? al_map_rgb(0, 255, 0) : al_map_rgb(255, 255, 255), SCREEN_WIDTH / 7, 475, ALLEGRO_ALIGN_CENTER, key_names[hPunch1]);
		al_draw_text(font, (hover == Low_Punch_1) ? al_map_rgb(0, 255, 0) : al_map_rgb(255, 255, 255), SCREEN_WIDTH / 7, 540, ALLEGRO_ALIGN_CENTER, key_names[lPunch1]);
		al_draw_text(font, (hover == Block_2) ? al_map_rgb(0, 255, 0) : al_map_rgb(255, 255, 255), SCREEN_WIDTH - SCREEN_WIDTH / 7, 275, ALLEGRO_ALIGN_CENTER, key_names[block2]);
		al_draw_text(font, (hover == High_Kick_2) ? al_map_rgb(0, 255, 0) : al_map_rgb(255, 255, 255), SCREEN_WIDTH - SCREEN_WIDTH / 7, 340, ALLEGRO_ALIGN_CENTER, key_names[hKick2]);
		al_draw_text(font, (hover == Low_Kick_2) ? al_map_rgb(0, 255, 0) : al_map_rgb(255, 255, 255), SCREEN_WIDTH - SCREEN_WIDTH / 7, 410, ALLEGRO_ALIGN_CENTER, key_names[lKick2]);
		al_draw_text(font, (hover == High_Punch_2) ? al_map_rgb(0, 255, 0) : al_map_rgb(255, 255, 255), SCREEN_WIDTH - SCREEN_WIDTH / 7, 475, ALLEGRO_ALIGN_CENTER, key_names[hPunch2]);
		al_draw_text(font, (hover == Low_Punch_2) ? al_map_rgb(0, 255, 0) : al_map_rgb(255, 255, 255), SCREEN_WIDTH - SCREEN_WIDTH / 7, 540, ALLEGRO_ALIGN_CENTER, key_names[lPunch2]);
		al_flip_display();

		if (ev.type == ALLEGRO_EVENT_KEY_DOWN) {
			switch (ev.keyboard.keycode) {
			case ALLEGRO_KEY_BACKSPACE:

				//Fade out
				for (float alpha = 0.0; alpha < 250; alpha += 0.5) {
					al_draw_scaled_bitmap(menu, 0, 0, menu_width, menu_height, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 0);
					al_draw_filled_rectangle(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, al_map_rgba(0, 0, 0, alpha));
					al_flip_display();
				}
				gamestate = OPTIONS;
				break;

			case ALLEGRO_KEY_DOWN:
				controlChange(DOWN, hover);
				break;

			case ALLEGRO_KEY_UP:
				
				controlChange(UP, hover);
				break;

			case ALLEGRO_KEY_RIGHT:
				controlChange(RIGHT, hover);
				break;

			case ALLEGRO_KEY_LEFT:
				controlChange(LEFT, hover);
				break;

			case ALLEGRO_KEY_SPACE:
				changeKey(hover, keys, event_queue);
				break;

			case ALLEGRO_KEY_ESCAPE:
				gamestate = EXIT_GAME;
				break;

			}
		}
	}

	al_set_config_value(cfg, "Player_1 Controls", "Block", std::to_string(block1).c_str());
	al_set_config_value(cfg, "Player_1 Controls", "High_Kick", std::to_string(hKick1).c_str());
	al_set_config_value(cfg, "Player_1 Controls", "Low_Kick", std::to_string(lKick1).c_str());
	al_set_config_value(cfg, "Player_1 Controls", "High_Punch", std::to_string(hPunch1).c_str());
	al_set_config_value(cfg, "Player_1 Controls", "Low_Punch", std::to_string(lPunch1).c_str());

	al_set_config_value(cfg, "Player_2 Controls", "Block", std::to_string(block2).c_str());
	al_set_config_value(cfg, "Player_2 Controls", "High_Kick", std::to_string(hKick2).c_str());
	al_set_config_value(cfg, "Player_2 Controls", "Low_Kick", std::to_string(lKick2).c_str());
	al_set_config_value(cfg, "Player_2 Controls", "High_Punch", std::to_string(hPunch2).c_str());
	al_set_config_value(cfg, "Player_2 Controls", "Low_Punch", std::to_string(lPunch2).c_str());

	al_save_config_file("../resources/config.cfg", cfg);
	al_destroy_bitmap(menu);
	al_destroy_font(font);
	al_destroy_config(cfg);
}

void Menu::changeSelection(enum selector_move action, enum character_selector & player_selector, engine2d::rectangle& outline_box)
{
	int character = player_selector;
	switch (action) {
	case UP:
		character - 5 > 0 ? character -= 5 : character -= 0;
		break;
	case DOWN:
		(character > JohnnyCage && character < SonyaBlade) ? character += 5 : character += 0;
		break;
	case LEFT:
		(character != JohnnyCage && character != Raiden) ? character -= 1 : character -= 0;
		break;
	case RIGHT:
		(character != SonyaBlade && character != Scorpion) ? character += 1 : character += 0;
		break;
	}

	player_selector = static_cast<character_selector>(character);
	outline_box.set_x_pos(portrait_xpos[player_selector]);
	outline_box.set_y_pos(portrait_ypos[player_selector]);
}

void Menu::changeSelectionstage(enum selector_move action, enum stage_selector & stager_selector, engine2d::rectangle& outline_box)
{
	int stage = stager_selector;
	switch (action) {
	case UP:
		(stage > The_Pit && stage <= Goro_Lair) ? stage -= 3 : stage += 0;
		break;
	case DOWN:
		(stage >= Palace_Gates && stage <= The_Pit) ? stage += 3 : stage += 0;
		break;
	case LEFT:
		(stage != Palace_Gates && stage != Throne_Room) ? stage -= 1 : stage -= 0;
		break;
	case RIGHT:
		(stage != The_Pit && stage != Goro_Lair) ? stage += 1 : stage += 0;
		break;
	}

	stager_selector = static_cast<stage_selector>(stage);
	outline_box.set_x_pos(portrait2_xpos[stager_selector]);
	outline_box.set_y_pos(portrait2_ypos[stager_selector]);
}



/* TODO: To animation na ginetai mesa ston tickAnimator kai oxi edw. Egw apla na dinw ton swsto paixth ston tickAnimator */
void Menu::playIdleAnimations(mk::Fighter & player1, mk::Fighter & player2, character_selector & p1_selector, character_selector & p2_selector, ALLEGRO_BITMAP * screen,
								engine2d::rectangle & p1_rect, engine2d::rectangle & p2_rect, ALLEGRO_FONT * font_name)
{
	static int frame_count1 = 0;
	static int frame_count2 = 0;

	static int frame_p1 = 0;
	static int frame_p2 = 0;

	if (character_names[p1_selector] != "None" && character_names[p1_selector] != "Hidden") {

		player1.select_fighter(character_names[p1_selector]);
		player1.tickAnimator.set_player_Widths(engine2d::choose_correct_width(player1.get_character_name()));
		player1.tickAnimator.set_player_frame_count(engine2d::choose_correct_frame_count(player1.get_character_name()));
		player1.tickAnimator.set_player_frame_delays(engine2d::choose_correct_delay_count(player1.get_character_name()));
		int frameDelay = player1.tickAnimator.get_player_frame_delays()["Idle"];

		if (++frame_count1 >= frameDelay) {
			player1.tickAnim.sprite.frameWidth = player1.tickAnimator.get_player_Widths()["Idle"];
			player1.tickAnim.start_position_x = 0;
			player1.tickAnim.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player1.get_character_name() + "/" + player1.get_character_name() + "_idle.png").c_str());
			int maxFrame = player1.tickAnimator.get_player_frame_count()["Idle"];

			if (++frame_p1 >= maxFrame)
				frame_p1 = 0;

			frame_count1 = 0;
		}

		al_draw_tinted_scaled_rotated_bitmap_region(player1.tickAnim.sprite.sprite_sheet, player1.tickAnim.start_position_x + (frame_p1 * player1.tickAnim.sprite.frameWidth), 0, player1.tickAnim.sprite.frameWidth, player1.tickAnim.sprite.frameHeight, al_map_rgb(255, 255, 255), 0, 0, player1.tickAnim.sprite.frameWidth, 350, 2.0, 2.0, 0, 0);
		al_draw_text(font_name, al_map_rgb(0, 0, 255), 80, 340, ALLEGRO_ALIGN_CENTER, character_names[p1_selector]);
	}

	if (character_names[p2_selector] != "None" && character_names[p2_selector] != "Hidden") {

		player2.select_fighter(character_names[p2_selector]);
		player2.tickAnimator.set_player_Widths(engine2d::choose_correct_width(player2.get_character_name()));
		player2.tickAnimator.set_player_frame_count(engine2d::choose_correct_frame_count(player2.get_character_name()));
		player2.tickAnimator.set_player_frame_delays(engine2d::choose_correct_delay_count(player2.get_character_name()));
		int frameDelay = player2.tickAnimator.get_player_frame_delays()["Idle"];

		if (++frame_count2 >= frameDelay) {
			player2.tickAnim.sprite.frameWidth = player2.tickAnimator.get_player_Widths()["Idle"];
			player2.tickAnim.start_position_x = 0;
			player2.tickAnim.sprite.set_sprite_sheet(("../resources/CharacterSheets/" + player2.get_character_name() + "/" + player2.get_character_name() + "_idle.png").c_str());
			int maxFrame = player2.tickAnimator.get_player_frame_count()["Idle"];

			if (++frame_p2 >= maxFrame)
				frame_p2 = 0;

			frame_count2 = 0;
		}

		al_draw_tinted_scaled_rotated_bitmap_region(player2.tickAnim.sprite.sprite_sheet, player2.tickAnim.start_position_x + (frame_p2 * player2.tickAnim.sprite.frameWidth), 0, player2.tickAnim.sprite.frameWidth, player2.tickAnim.sprite.frameHeight, al_map_rgb(255, 255, 255), 0, 0, 800 - 3 * player2.tickAnim.sprite.frameWidth, 350, 2.0, 2.0, 0, ALLEGRO_FLIP_HORIZONTAL);
		al_draw_text(font_name, al_map_rgb(255, 0, 0), 720, 340, ALLEGRO_ALIGN_CENTER, character_names[p2_selector]);
	}
}

void Menu::characterSelectScreen(game_state & gamestate, ALLEGRO_EVENT_QUEUE * event_queue, mk::Fighter & player1, mk::Fighter & player2, ALLEGRO_DISPLAY * disp)
{
	ALLEGRO_BITMAP * character_menu = al_load_bitmap("../resources/menus/Choose_your_fighter.png");
	ALLEGRO_SAMPLE * ui_sample = al_load_sample("../resources/sounds/ui/mk1-00163.wav");
	ALLEGRO_EVENT ev;
	ALLEGRO_FONT * font = al_load_ttf_font("../resources/fonts/mk2.ttf", 30, 0);
	ALLEGRO_FONT * font_name = al_load_ttf_font("../resources/fonts/mk1.ttf", 35, 0);

	int menu_width = al_get_bitmap_width(character_menu);
	int menu_height = al_get_bitmap_height(character_menu);

	character_selector player1_selector = Kano;
	character_selector player2_selector = SubZero;
	
	engine2d::rectangle p1_rectangle(portrait_xpos[player1_selector], portrait_ypos[player1_selector], 125, 180, al_map_rgb(0, 0, 255), "P1");
	engine2d::rectangle p2_rectangle(portrait_xpos[player2_selector], portrait_ypos[player2_selector], 125, 180, al_map_rgb(255, 0, 0), "         P2");

	//Fade in
	for (float alpha = 250; alpha > 0; alpha -= 0.5) {
		al_draw_scaled_bitmap(character_menu, 0, 0, menu_width, menu_height, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 0);
		al_draw_filled_rectangle(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, al_map_rgba(0, 0, 0, alpha));
		al_flip_display();
	}
	ALLEGRO_SAMPLE_ID bgm_id;
	ALLEGRO_SAMPLE *bgm;
	ALLEGRO_CONFIG* cfg = al_load_config_file("../resources/config.cfg");
	if (atoi(al_get_config_value(cfg, "Music", "Music")) == 1) {
		 bgm = al_load_sample(("../resources/sounds/arenas/Character_Selection.wav"));
		al_play_sample(bgm, 0.4, 0.0, 1.0, ALLEGRO_PLAYMODE_LOOP, &bgm_id);
	}
	else {
		 bgm = al_load_sample("../resources/sounds/arenas/Silence.wav");
		al_play_sample(bgm, 0.4, 0.0, 1.0, ALLEGRO_PLAYMODE_LOOP, &bgm_id);
	}
	al_destroy_config(cfg);

	while (gamestate == CHARACTER_SELECT) {

		al_flip_display();
		al_draw_scaled_bitmap(character_menu, 0, 0, menu_width, menu_height, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 0);
		engine2d::draw_my_rect(p1_rectangle, font, al_map_rgb(0, 0, 255));
		engine2d::draw_my_rect(p2_rectangle, font, al_map_rgb(255, 0, 0));
		
		al_wait_for_event(event_queue, &ev);

		if (ev.type == ALLEGRO_EVENT_KEY_DOWN) {

			al_play_sample(ui_sample, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);
			
			switch (ev.keyboard.keycode) {
			case ALLEGRO_KEY_BACKSPACE:

				//Fade out
				for (float alpha = 0.0; alpha < 250; alpha += 0.5) {
					al_draw_scaled_bitmap(character_menu, 0, 0, menu_width, menu_height, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 0);
					al_draw_filled_rectangle(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, al_map_rgba(0, 0, 0, alpha));
					al_flip_display();
				}

				gamestate = MENU;
				break;

			case ALLEGRO_KEY_ESCAPE:

				gamestate = EXIT_GAME;
				break;

			case ALLEGRO_KEY_LEFT:

				changeSelection(LEFT, player1_selector, p1_rectangle);
				break;

			case ALLEGRO_KEY_RIGHT:

				changeSelection(RIGHT, player1_selector, p1_rectangle);
				break;

			case ALLEGRO_KEY_UP:

				changeSelection(UP, player1_selector, p1_rectangle);
				break;

			case ALLEGRO_KEY_DOWN:
				changeSelection(DOWN, player1_selector, p1_rectangle);
				break;

			case ALLEGRO_KEY_A:

				changeSelection(LEFT, player2_selector, p2_rectangle);
				break;

			case ALLEGRO_KEY_D:

				changeSelection(RIGHT, player2_selector, p2_rectangle);
				break;

			case ALLEGRO_KEY_W:

				changeSelection(UP, player2_selector, p2_rectangle);
				break;

			case ALLEGRO_KEY_S:

				changeSelection(DOWN, player2_selector, p2_rectangle);
				break;

			case ALLEGRO_KEY_SPACE:
				if (character_names[player1_selector] == "None" || character_names[player2_selector] == "None" || character_names[player1_selector] == "Hidden" || character_names[player2_selector] == "Hidden") {
					break;
				}
				/*
				else if ((character_names[player1_selector] == "Kano" || character_names[player2_selector] == "Kano" || character_names[player1_selector] == "LiuKang" || character_names[player2_selector] == "LuKang" || character_names[player1_selector] == "Raiden" || character_names[player2_selector] == "Raiden" || character_names[player1_selector] == "Scorpion" || character_names[player2_selector] == "Scorpion" || character_names[player1_selector] == "JohnnyCage" || character_names[player2_selector] == "JohnnyCage")){
					al_show_native_message_box(
						disp,
						"Warning",
						"The following Characters are not available in Beta :",
						"Scorpion,Raiden,Kano,LiuKang,JohnnyCage. \n "
						"Missing sprites cause crashes. \n"
						"Should all work when the sprites are stitched and put in appropriate folders. \n"
						"Please try SubZero for our full moveset as for now. \n"
						"Sonya also works to some degree but also crashes (due to missing sprites) "
						"so use with caution \n"
						"if you want to select all characters remove this warning message from "
						"Menu.cpp line 377",
						NULL,
						ALLEGRO_MESSAGEBOX_WARN
					);
					al_flush_event_queue(event_queue);
					break;

			    }
				*/
				player1.select_fighter(character_names[player1_selector]);
				player2.select_fighter(character_names[player2_selector]);

				//fade out
				for (float alpha = 0.0; alpha < 250; alpha += 0.5) {
					al_draw_scaled_bitmap(character_menu, 0, 0, 395, 254, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 0);
					al_draw_filled_rectangle(0, 0, 800, 600, al_map_rgba(0, 0, 0, alpha));
					al_flip_display();
				}

				gamestate = STAGE_SELECT;
				break;
				
			}

			std::cout << "\nPlayer 1: " << character_names[player1_selector] << "\nPlayer 2: " << character_names[player2_selector] << std::endl;
		} else if (ev.type == ALLEGRO_EVENT_TIMER) {
			playIdleAnimations(player1, player2, player1_selector, player2_selector, character_menu, p1_rectangle, p2_rectangle, font_name);
		}
	}

	al_destroy_sample(bgm);
	al_destroy_font(font);
	al_destroy_font(font_name);
}


void Menu::StageSelectScreen(game_state & gamestate, ALLEGRO_EVENT_QUEUE * event_queue, engine2d::background & arena, ALLEGRO_DISPLAY * disp)
{
	ALLEGRO_BITMAP * character_menu = al_load_bitmap("../resources/menus/StageSelect.png");
	ALLEGRO_SAMPLE * ui_sample = al_load_sample("../resources/sounds/ui/mk1-00163.wav");
	ALLEGRO_EVENT ev;
	ALLEGRO_FONT * font = al_load_ttf_font("../resources/fonts/mk2.ttf", 30, 0);
	ALLEGRO_FONT * font_name = al_load_ttf_font("../resources/fonts/mk1.ttf", 35, 0);

	int menu_width = al_get_bitmap_width(character_menu);
	int menu_height = al_get_bitmap_height(character_menu);

	stage_selector player1_selector = Courtyard;
	

	engine2d::rectangle p1_rectangle(portrait2_xpos[player1_selector], portrait2_ypos[player1_selector], 173, 90, al_map_rgb(255, 255, 0), "");
	

	//Fade in
	for (float alpha = 250; alpha > 0; alpha -= 0.5) {
		al_draw_scaled_bitmap(character_menu, 0, 0, menu_width, menu_height, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 0);
		al_draw_filled_rectangle(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, al_map_rgba(0, 0, 0, alpha));
		al_flip_display();
	}
	ALLEGRO_SAMPLE_ID bgm_id;
	ALLEGRO_SAMPLE *bgm;
	ALLEGRO_CONFIG* cfg = al_load_config_file("../resources/config.cfg");
	if (atoi(al_get_config_value(cfg, "Music", "Music")) == 1) {
		bgm = al_load_sample(("../resources/sounds/arenas/Character_Selection.wav"));
		al_play_sample(bgm, 0.4, 0.0, 1.0, ALLEGRO_PLAYMODE_LOOP, &bgm_id);
	}
	else {
		bgm = al_load_sample("../resources/sounds/arenas/Silence.wav");
		al_play_sample(bgm, 0.4, 0.0, 1.0, ALLEGRO_PLAYMODE_LOOP, &bgm_id);
	}
	al_destroy_config(cfg);

	while (gamestate == STAGE_SELECT) {

		al_flip_display();
		
		al_draw_scaled_bitmap(character_menu, 0, 0, menu_width, menu_height, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 0);
		engine2d::draw_my_rect(p1_rectangle, font, al_map_rgb(255, 255, 0));
		al_draw_text(font_name, al_map_rgb(255, 255, 0), 400, 145, ALLEGRO_ALIGN_CENTER, stage_names[player1_selector]);
		

		al_wait_for_event(event_queue, &ev);

		if (ev.type == ALLEGRO_EVENT_KEY_DOWN) {

			al_play_sample(ui_sample, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, NULL);

			switch (ev.keyboard.keycode) {
			case ALLEGRO_KEY_BACKSPACE:

				//Fade out
				for (float alpha = 0.0; alpha < 250; alpha += 0.2) {
					al_draw_scaled_bitmap(character_menu, 0, 0, menu_width, menu_height, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 0);
					al_draw_filled_rectangle(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, al_map_rgba(0, 0, 0, alpha));
					al_flip_display();
				}

				gamestate = CHARACTER_SELECT;
				break;

			case ALLEGRO_KEY_ESCAPE:

				gamestate = EXIT_GAME;
				break;

			case ALLEGRO_KEY_LEFT:

				changeSelectionstage(LEFT, player1_selector, p1_rectangle);
				break;

			case ALLEGRO_KEY_RIGHT:

				changeSelectionstage(RIGHT, player1_selector, p1_rectangle);
				break;

			case ALLEGRO_KEY_UP:

				changeSelectionstage(UP, player1_selector, p1_rectangle);
				break;

			case ALLEGRO_KEY_DOWN:
				changeSelectionstage(DOWN, player1_selector, p1_rectangle);
				break;

			case ALLEGRO_KEY_A:

				changeSelectionstage(LEFT, player1_selector, p1_rectangle);
				break;

			case ALLEGRO_KEY_D:

				changeSelectionstage(RIGHT, player1_selector, p1_rectangle);
				break;

			case ALLEGRO_KEY_W:

				changeSelectionstage(UP, player1_selector, p1_rectangle);
				break;

			case ALLEGRO_KEY_S:

				changeSelectionstage(DOWN, player1_selector, p1_rectangle);
				break;

			case ALLEGRO_KEY_SPACE:
				
				
				
				//player1.select_fighter(character_names[player1_selector]);
				//player2.select_fighter(character_names[player2_selector]);
				arena.change_arena(stage_names[player1_selector]);
				engine2d::set_correct_background_attributes(arena);
				//fade out
				for (float alpha = 0.0; alpha < 250; alpha += 0.5) {
					al_draw_scaled_bitmap(character_menu, 0, 0, al_get_bitmap_width(character_menu), al_get_bitmap_height(character_menu), 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 0);
					al_draw_filled_rectangle(0, 0, 800, 600, al_map_rgba(0, 0, 0, alpha));
					al_flip_display();
				}

				gamestate = FIGHT;
				break;

			}

			std::cout << "\nArena 1: " << stage_names[player1_selector] << std::endl;
		}
		else if (ev.type == ALLEGRO_EVENT_TIMER) {
			//playIdleAnimations(player1, player2, player1_selector, player2_selector, character_menu, p1_rectangle, p2_rectangle, font_name);
		}
	}

	al_destroy_sample(bgm);
	al_destroy_font(font);
	al_destroy_font(font_name);
}
