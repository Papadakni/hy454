# HY454
# BETA VERSION

The following is the project of  hy454 Computer Science Department University Of Crete Creation Of Intelligent Interfaces and Games
Creation of mortal kombat original edition using c++

## Some info

### General
Our implementation of **mortal kombat** 1 is based on the snes version because the spritesheets we found 
come from there. **Everything is made according to what we learned in class, e.g config file to get/set values and options,tick animator,sprites,fighter class etc**
The code is still a bit messy and should have been cleaned by the final turnin of the presentation.
So far we have working menus,character select and fight albeit not all moves are yet made.
Finish him is not yet made so fight can go on for more than 3 rounds.
In this beta version it is advised for both players to play as SubZero or sonya,because of missing current sprites for other characters
All levels are currently working, and for the beta , stage selector is working.
All moves made except special (freeze ball) and flying kick.
Options Screen Working with dynamic controls sound.
Everything is used dynamically new chars are just more inputs in config file and correct file names.
Due to time limits resolution is locked to 800x600 and is not advised to be changed in code.
In presentation we will run the game in Fullscreen

### Branches
Master Branch is our init turnin branch

Nick Branch is our experimental and current working branch

### Questions

for any questions email one of the following

csd3469@csd.uoc.gr,
csd3431@csd.uoc.gr,
csd3267@csd.uoc.gr,

## Getting Started

### Controls
Player 1 and 2 Controls are set in Resources/Config.ini file

Numbers next to moves correspond to allegro keyboard codes

To change the controls just change from the options menu or allegro config file.

Buttons to navigate the menus are space and backspace

Player_1:

Up/jump: Up arrow key 

Left: Left arrow key 

Right: Right arrow key 

Down/crouch: Down arrow key 

High Punch = . key

Low Punch = / key

High Kick = l key

Low Kick = ; key

Block = , key

Player_2:

Up/jump: Up arrow key 

Left: Left arrow key 

Right: Right arrow key 

Down/crouch: Down arrow key 

High Punch = g key

Low Punch = undefined

High Kick = undefined

Low Kick = undefined

Block = h key


## Prerequisites

Allegro 5 api with all addons enabled.

Working Keyboard.

30 mb of RAM minimum.

## Installing
Pulling and running the latest master branch should work


## Built With

Microsoft Visual Studio 2017
Allegro 5 API


## Authors

Nick Papadakis Csd 3469 - maintainer

Stavros Mavrikos Csd 3431 - maintainer

Panagiotis Zervakis Csd 3267 - maintainer

